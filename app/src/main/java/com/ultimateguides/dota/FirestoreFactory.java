package com.ultimateguides.dota;

import com.google.firebase.firestore.CollectionReference;

public class FirestoreFactory {

    private static CollectionReference getPath(String path) {
        CollectionReference localizedPath = GuideApplication.getFirestore()
                .collection(path + "_" + LocaleHelper.getLanguageDisplay());
        return localizedPath;
    }

    public static CollectionReference getAlliances() {
        return getPath("alliances");
    }

    public static CollectionReference getItems() {
        return getPath("items");
    }

    public static CollectionReference getHeroes() {
        return getPath("heroes");
    }

    public static CollectionReference getTeamsEditors() {
        return GuideApplication.getFirestore().collection("editors_teams");
    }
}
