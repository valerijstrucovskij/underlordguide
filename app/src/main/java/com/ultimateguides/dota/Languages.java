package com.ultimateguides.dota;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public abstract class Languages {
    @Retention(SOURCE)
    @IntDef({ENGLISH, RUSSIAN})
    public @interface NavigationMode {
    }

    public static final int ENGLISH = 0;
    public static final int RUSSIAN = 1;

    public abstract void setLang(@NavigationMode int mode);

    @NavigationMode
    public abstract int getLang();
}
