package com.ultimateguides.dota;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class HeroTypeDescriptor {


    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            Abaddon,
            Alchemist,
            AntiMage,
            ArcWarden,
            Axe,
            Batrider,
            Beastmaster,
            Bloodseeker,
            BountyHunter,
            ChaosKnight,
            Clockwerk,
            CrystalMaiden,
            Disruptor,
            Doom,
            DragonKnight,
            DrowRanger,
            Enchantress,
            Enigma,
            Gryocopter,
            Juggernaut,
            KeeperoftheLight,
            Kunkka,
            Lich,
            Lina,
            LoneDruid,
            Luna,
            Lycan,
            Medusa,
            Mirana,
            Morphling,
            NaturesProphet,
            Necrophos,
            OgreMagi,
            Omniknight,
            PhantomAssassin,
            Puck,
            Pudge,
            QueenofPain,
            Razor,
            SandKing,
            Shadowfiend,
            ShadowShaman,
            Slardar,
            Slark,
            Sniper,
            Techies,
            TemplarAssassin,
            Terrorblade,
            Tidehunter,
            Timbersaw,
            Tinker,
            Tiny,
            TreantProtector,
            TrollWarlord,
            Tusk,
            Venomancer,
            Viper,
            Warlock,
            Windranger,
            WitchDoctor,

            Dazzle,
            ShadowDemon,
            Sven,
            Weaver,
            WispIo,
            Bristleback,
            Broodmother,
            Void,
            LegionCommander,
            LifeStealer,
            Magnus,
            NyxAssassin
    })
    public @interface HeroType {

    }

    public static final String Abaddon = "abaddon";
    public static final String Alchemist = "alchemist";
    public static final String AntiMage = "antimage";
    public static final String ArcWarden = "arc_warden";
    public static final String Axe = "axe";
    public static final String Batrider = "bat_rider";
    public static final String Beastmaster = "beastmaster";
    public static final String Bloodseeker = "bloodseeker";
    public static final String BountyHunter = "bounty_hunter";
    public static final String ChaosKnight = "chaos_knight";
    public static final String Clockwerk = "clockwerk";
    public static final String CrystalMaiden = "crystal_maiden";
    public static final String Disruptor = "disruptor";
    public static final String Doom = "doom";
    public static final String DragonKnight = "dragon_knight";
    public static final String DrowRanger = "drow_ranger";
    public static final String Enchantress = "enchantress";
    public static final String Enigma = "enigma";
    public static final String Gryocopter = "gyrocopter";
    public static final String Juggernaut = "juggernaut";
    public static final String KeeperoftheLight = "keeper_of_the_light";
    public static final String Kunkka = "kunkka";
    public static final String Lich = "lich";
    public static final String Lina = "lina";
    public static final String LoneDruid = "lone_druid";
    public static final String Luna = "luna";
    public static final String Lycan = "lycan";
    public static final String Medusa = "medusa";
    public static final String Mirana = "mirana";
    public static final String Morphling = "morphling";
    public static final String NaturesProphet = "furion";
    public static final String Necrophos = "necrophos";
    public static final String OgreMagi = "ogre_magi";
    public static final String Omniknight = "omniknight";
    public static final String PhantomAssassin = "phantom_assassin";
    public static final String Puck = "puck";
    public static final String Pudge = "pudge";
    public static final String QueenofPain = "queen_of_pain";
    public static final String Razor = "razor";
    public static final String SandKing = "sand_king";
    public static final String Shadowfiend = "shadow_fiend";
    public static final String ShadowShaman = "shadow_shaman";
    public static final String Slardar = "slardar";
    public static final String Slark = "slark";
    public static final String Sniper = "sniper";
    public static final String Techies = "techies";
    public static final String TemplarAssassin = "templar_assassin";
    public static final String Terrorblade = "terrorblade";
    public static final String Tidehunter = "tidehunter";
    public static final String Timbersaw = "timbersaw";
    public static final String Tinker = "tinker";
    public static final String Tiny = "tiny";
    public static final String TreantProtector = "treant_protector";
    public static final String TrollWarlord = "troll_warlord";
    public static final String Tusk = "tusk";
    public static final String Venomancer = "venomancer";
    public static final String Viper = "viper";
    public static final String Warlock = "warlock";
    public static final String Windranger = "wind_ranger";
    public static final String WitchDoctor = "witch_doctor";
    //BIG UPDATE
    public static final String Dazzle = "dazzle";
    public static final String ShadowDemon = "shadow_demon";
    public static final String Sven = "sven";
    public static final String Weaver = "weaver";
    public static final String WispIo = "wisp";
    public static final String Bristleback = "bristleback";
    public static final String Broodmother = "broodmother";
    public static final String Void = "faceless_void";
    public static final String LegionCommander = "legion_commander";
    public static final String LifeStealer = "life_stealer";
    public static final String Magnus = "magnus";
    public static final String NyxAssassin = "nyx_assassin";
    public static final String SpiritEmber = "ember_spirit";
    public static final String SpiritEarth = "earth_spirit";
    public static final String SpiritStorm = "storm_spirit";
    public static final String SpiritVoid = "void_spirit";
    public static final String Snapfire = "snapfire";
}