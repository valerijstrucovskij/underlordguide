package com.ultimateguides.dota.retrofit;

import com.ultimateguides.dota.model.steamApi.NewsSteamModel;
import com.ultimateguides.dota.model.steamApi.VanityProfileModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SteamAPI {

    @GET("/ISteamNews/GetNewsForApp/v0002")
    Call<NewsSteamModel> GetNews(@Query("appid") String appid,
                             @Query("count") int count,
                             @Query("maxlength") int maxlength,
                             @Query("format") String format);

    /**
     * Uses to convert username to steamId64.
     * Username can be entered by user in steam settings, custom url
     *
     * @param vanityUrl
     * @return
     */
    @GET("/ISteamUser/ResolveVanityURL/v1")
    Call<VanityProfileModel> GetUserId(@Query("vanityurl") String vanityUrl,
                                       @Query("key") String webApiKey);

}
