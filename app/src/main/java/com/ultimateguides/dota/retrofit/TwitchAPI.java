package com.ultimateguides.dota.retrofit;

import com.ultimateguides.dota.model.steamApi.NewsSteamModel;
import com.ultimateguides.dota.model.twitch.TwitchDataModel;
import com.ultimateguides.dota.model.twitch.TwitchVideoModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface TwitchAPI {

    @Headers("Client-ID: h92d5tn4ca2bdkgjiru7m34kgq2ub3")
    @GET("/helix/streams")
    Call<TwitchDataModel<TwitchVideoModel>> GetCurrentStreams(@Query("game_id") int gameId); //512693
}
