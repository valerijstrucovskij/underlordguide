package com.ultimateguides.dota.screens.main.guides;

public class ContraptionModel {

    public String name;
    public final int icon;
    public String description;

    public ContraptionModel(String name, int icon, String description) {
        this.name = name;
        this.icon = icon;
        this.description = description;
    }
}
