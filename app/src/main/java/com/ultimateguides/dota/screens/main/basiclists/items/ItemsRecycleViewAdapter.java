package com.ultimateguides.dota.screens.main.basiclists.items;

import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.ItemsImages;
import com.ultimateguides.dota.model.ItemModel;

import java.util.ArrayList;
import java.util.List;

public class ItemsRecycleViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements Filterable {

    private List<ItemModel> data;
    private List<ItemModel> filteredData;
    private ItemsImages images = new ItemsImages();

    public ItemsRecycleViewAdapter(List<ItemModel> data) {
        this.data = data;
        this.filteredData = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ViewType.ITEM) {
            ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_items, parent, false);
            return new ItemsRecycleViewAdapter.ViewHolderItem(v);
        } else {
            ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_items_header, parent, false);
            return new ItemsRecycleViewAdapter.ViewHolderHeader(v);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemModel dataItem = getItem(position);
        if (holder instanceof ViewHolderHeader) {
            ViewHolderHeader itemView = (ViewHolderHeader) holder;
            itemView.titleView.setText(String.valueOf(dataItem.name));

            int color = ContextCompat.getColor(GuideApplication.getApp(), dataItem.headerColor);
            itemView.titleView.setTextColor(color);
            itemView.iconView.setBackgroundColor(color);
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
            layoutParams.setFullSpan(true);
        } else {
            ViewHolderItem itemView = (ViewHolderItem) holder;
            itemView.nameView.setText(String.valueOf(dataItem.name));

            Spanned description = HtmlCompat.fromHtml(String.valueOf(dataItem.description), HtmlCompat.FROM_HTML_OPTION_USE_CSS_COLORS);
            itemView.descriptionView.setText(description);
            Integer icon = images.get(dataItem.icon);
            if (icon != null) {
                itemView.iconView.setImageResource(icon);
            } else {
                itemView.iconView.setImageResource(R.color.transparent);
            }
        }
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    private ItemModel getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return filteredData.get(position).isHeader ? ViewType.HEADER : ViewType.ITEM;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredData = data;
                } else {
                    List<ItemModel> filteredList = new ArrayList<>();
                    for (ItemModel row : data) {
                        if (row.name.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredData = (ArrayList<ItemModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        ImageView iconView;
        TextView nameView;
        TextView descriptionView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            nameView = itemView.findViewById(R.id.tv_name);
            iconView = itemView.findViewById(R.id.iv_icon);
            descriptionView = itemView.findViewById(R.id.tv_description);
        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        TextView titleView;
        View iconView;

        public ViewHolderHeader(@NonNull View itemView) {
            super(itemView);
            titleView = itemView.findViewById(R.id.tv_title);
            iconView = itemView.findViewById(R.id.iv_icon);
        }
    }

    private interface ViewType {
        int HEADER = 1;
        int ITEM = 0;
    }
}
