package com.ultimateguides.dota.screens.main.home;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.text.HtmlCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.AppodealNetworks;
import com.bugfender.sdk.Bugfender;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.pixplicity.generate.Rate;
import com.ultimateguides.dota.BuildConfig;
import com.ultimateguides.dota.Config;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.LocaleHelper;
import com.ultimateguides.dota.PreferenceField;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.Utils;
import com.ultimateguides.dota.model.steamApi.NewsSteamModel;
import com.ultimateguides.dota.model.twitch.TwitchDataModel;
import com.ultimateguides.dota.model.twitch.TwitchVideoModel;
import com.ultimateguides.dota.retrofit.TwitchAPI;
import com.ultimateguides.dota.screens.tinderlords.TinderlordsActivity;
import com.ultimateguides.dota.screens.builder.TeamBuilderActivity;
import com.ultimateguides.dota.screens.donation.DonationActivity;
import com.ultimateguides.dota.screens.main.basiclists.alliances.AlliancesFragment;
import com.ultimateguides.dota.screens.main.basiclists.heroes.HeroesFragment;
import com.ultimateguides.dota.screens.main.basiclists.items.ItemsFragment;
import com.ultimateguides.dota.screens.main.ranks.RanksFragment;
import com.ultimateguides.dota.screens.main.teams.TeamsFragment;
import com.ultimateguides.dota.screens.news.ActivityNews;
import com.ultimateguides.dota.screens.synergy.SynergyActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ultimateguides.dota.GuideApplication.getPrefs;

public class HomeFragment extends Fragment
        implements NavigationView.OnNavigationItemSelectedListener {

    private ViewGroup rootView;

    private List<NewsSteamModel.AppnewsBean.NewsitemsBean> newsList = new ArrayList<>();
    private List<TwitchVideoModel> streamsList = new ArrayList<>();
    private NewsRecycleViewAdapter newsAdapter = new NewsRecycleViewAdapter(newsList);
    private StreamsRecycleViewAdapter streamsAdapter = new StreamsRecycleViewAdapter(streamsList);
    private View iconActivated;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_home, container, false);
        ViewGroup parentLayout = rootView.findViewById(R.id.container);
        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        String honorableTitle = getResources().getString(R.string.app_name) + " ☕";
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(honorableTitle);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

        //left side navigation
        DrawerLayout drawer = rootView.findViewById(R.id.drawer_layout);
        NavigationView navigationView = rootView.findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        //After 5 starts of the app ask user for feedback
        Rate rate = new Rate.Builder(getActivity())
                .setTriggerCount(5)
                .setMinimumInstallTime((int) TimeUnit.DAYS.toMillis(3))
                .setFeedbackAction(() -> onClickFeedback(null))
                .setPositiveButton("Sure!")
                .setCancelButton("Maybe later")
                .setNegativeButton("Leave feedback")
                .setSnackBarParent(getActivity().findViewById(android.R.id.content))
                .setSwipeToDismissVisible(true)
                .build();
        rate.count();
        rate.showRequest();

        iconActivated = rootView.findViewById(R.id.iv_activated);
        //assign listeners
        rootView.findViewById(R.id.btn_watch_video).setOnClickListener(this::onClickWatchVideo);
        rootView.findViewById(R.id.join_discord).setOnClickListener(this::joinDiscord);
        rootView.findViewById(R.id.donation).setOnClickListener(this::onClickDonation);
        rootView.findViewById(R.id.btn_feedback).setOnClickListener(this::onClickFeedback);
        rootView.findViewById(R.id.updates_available).setVisibility(View.GONE);
        rootView.findViewById(R.id.updates_available).setOnClickListener(v -> Utils.openAppMarket(getActivity()));
//        rootView.findViewById(R.id.btn_info).setOnClickListener(this::openFAQInfo);
        rootView.findViewById(R.id.btn_news_update).setOnClickListener(v -> requestNews());
        rootView.findViewById(R.id.btn_streams_update).setOnClickListener(v -> requestStreams());
        rootView.findViewById(R.id.btn_app_news).setOnClickListener(this::onClickPatchNotes);
        rootView.findViewById(R.id.tv_lang).setOnClickListener(this::onClickLanguage);
        rootView.findViewById(R.id.duo_looking).setOnClickListener(this::onClickTinderlords);
        iconActivated.setOnClickListener(this::onClickDonateActivated);
        updateLanguageLabel();
        //LOAD NEWS
        RecyclerView recycleView = rootView.findViewById(R.id.rv_news);
        recycleView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recycleView.setAdapter(newsAdapter);
        newsAdapter.setOnItemClick(this::showUrlPageDialog);
        //LOAD STREAMS
        RecyclerView recycleViewStreams = rootView.findViewById(R.id.rv_streams);
        recycleViewStreams.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recycleViewStreams.setAdapter(streamsAdapter);
        streamsAdapter.setOnItemClick(this::showUrlPageDialog);


        rootView.findViewById(R.id.btn_watch_video).setVisibility(View.INVISIBLE);

        //Load video advertizing
        Appodeal.disableNetwork(getActivity(), AppodealNetworks.INMOBI);
        Appodeal.setTesting(BuildConfig.DEBUG);
        Appodeal.initialize(getActivity(), Config.appodealId, Appodeal.REWARDED_VIDEO);
        Appodeal.setRewardedVideoCallbacks(new RewardedVideoCallback() {
            @Override
            public void onRewardedVideoShown() {
                super.onRewardedVideoShown();
                getPrefs()
                        .edit()
                        .putLong(PreferenceField.TIME_LAST_REWARDED_VIDEO, System.currentTimeMillis())
                        .apply();
                Appodeal.hide(getActivity(), Appodeal.BANNER_BOTTOM);
            }

            @Override
            public void onRewardedVideoLoaded(boolean b) {
                super.onRewardedVideoLoaded(b);
                if (!GuideApplication.isADSBlocked()) {
                    rootView.findViewById(R.id.btn_watch_video).setVisibility(View.VISIBLE);
                }
            }
        });


        return rootView;
    }


    @Subscribe
    public void onEvent(ShowNeedUpdateEvent e) {
        ((TextView) rootView.findViewById(R.id.tv_new_version)).setText(e.appVersionStore);
        rootView.findViewById(R.id.updates_available).setVisibility(View.VISIBLE);

        boolean hasNews = getPrefs().getBoolean(PreferenceField.HAS_APP_NEWS, false);
        if (hasNews) {
            AppCompatImageView btn = rootView.findViewById(R.id.btn_app_news);
            int colorInt = getResources().getColor(R.color.tier_5);
            ColorStateList csl = ColorStateList.valueOf(colorInt);
            btn.setSupportImageTintList(csl);
        }

    }

    private void requestStreams() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.twitch.tv")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        TwitchAPI twitchRep = retrofit.create(TwitchAPI.class);
        twitchRep.GetCurrentStreams(512693)
                .enqueue(new Callback<TwitchDataModel<TwitchVideoModel>>() {
                    @Override
                    public void onResponse(Call<TwitchDataModel<TwitchVideoModel>> call, Response<TwitchDataModel<TwitchVideoModel>> response) {
                        if (response.isSuccessful()) {
                            List<TwitchVideoModel> listStreams = response.body().data;
                            streamsList.clear();
                            streamsList.addAll(listStreams);
                            streamsAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(Call<TwitchDataModel<TwitchVideoModel>> call, Throwable throwable) {
                    }
                });

    }

    private void requestNews() {
        newsList.clear();
        newsAdapter.notifyDataSetChanged();
        GuideApplication.getApi().GetNews("1046930", 10, 300, "json")
                .enqueue(new Callback<NewsSteamModel>() {
                    @Override
                    public void onResponse(Call<NewsSteamModel> call, Response<NewsSteamModel> response) {
                        Log.d("HomeFragment", "onResponse news: " + response.isSuccessful());
                        if (response.isSuccessful()) {
                            Log.d("HomeFragment", "onResponse news: " + response.body().appnews.newsitems.size());

                            newsList.addAll(response.body().appnews.newsitems);
                            newsAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(Call<NewsSteamModel> call, Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void onClickDonation(View view) {
        Intent i = new Intent(getActivity(), DonationActivity.class);
        startActivity(i);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        LocaleHelper.onAttach(context);
    }

    public void onClickLanguage(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Change language");
        builder.setCancelable(true);
        String langSaved = LocaleHelper.getLanguage();
        int selectedLang = 0;
        switch (langSaved) {
            case "en":
                selectedLang = 0;
                break;
            case "ru":
                selectedLang = 1;
                break;
            case "th":
                selectedLang = 2;
                break;
        }
        builder.setSingleChoiceItems(new String[]{"English", "Русский", "ภาษาไทย"}, selectedLang, (dialogInterface, i) -> {
            GuideApplication.getPrefs()
                    .edit()
                    .remove(PreferenceField.FIRESTORE_DB_VERSION)
                    .apply();

            String lang = "en";
            switch (i) {
                case 0:
                    lang = "en";
                    break;
                case 1:
                    lang = "ru";
                    break;
                case 2:
                    lang = "th";
                    break;
            }
            LocaleHelper.setLocale(getActivity(), lang);
            Utils.triggerRebirth(getActivity());
            dialogInterface.dismiss();
        });
        builder.show();
    }

    public void openFAQInfo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("FAQ");
        builder.setCancelable(true);
        String content = getResources().getString(R.string.faq_home);
        builder.setMessage(HtmlCompat.fromHtml(content, HtmlCompat.FROM_HTML_MODE_LEGACY));
        builder.setNeutralButton(R.string.btn_hide, (dialogInterface, i) -> dialogInterface.dismiss());
        builder.show();
    }

    public void onClickPatchNotes(View v) {
        GuideApplication.getPrefs().edit().putBoolean(PreferenceField.HAS_APP_NEWS, false).apply();
        AppCompatImageView btn = rootView.findViewById(R.id.btn_app_news);
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_enabled}
        };

        int[] colors = new int[]{
                Color.WHITE
        };

        ColorStateList myList = new ColorStateList(states, colors);

        btn.setSupportImageTintList(myList);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.title_release_notes);
        builder.setCancelable(true);
        String releaseNotes = getResources().getString(R.string.release_notes);
        builder.setMessage(HtmlCompat.fromHtml(releaseNotes, HtmlCompat.FROM_HTML_MODE_LEGACY));

        builder.setNeutralButton(R.string.btn_hide, (dialogInterface, i) -> dialogInterface.dismiss());
        builder.show();
    }

    public void onClickDonateActivated(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Thank You");
        builder.setCancelable(true);
        String content = getResources().getString(R.string.faq_home);
//        builder.setMessage(HtmlCompat.fromHtml(content, HtmlCompat.FROM_HTML_MODE_LEGACY));
        builder.setMessage("You have made donation");
        builder.setNeutralButton(R.string.btn_hide, (dialogInterface, i) -> dialogInterface.dismiss());
        builder.show();
    }

    private void updateLanguageLabel() {
        String lang = LocaleHelper.getLanguage();
        TextView labelView = rootView.findViewById(R.id.tv_lang);
        labelView.setText(lang.toUpperCase());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        GuideApplication.getAnalytics().setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());
        if (!GuideApplication.isADSBlocked()) {
            rootView.findViewById(R.id.btn_watch_video).setVisibility(View.VISIBLE);
        }
        if (GuideApplication.isDonated()) {
            iconActivated.setVisibility(View.VISIBLE);
        } else {
            iconActivated.setVisibility(View.GONE);
        }
        requestNews();
        requestStreams();
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void showUrlPageDialog(String url) {
        BottomSheetDialog builder = new BottomSheetDialog(getActivity());
        ViewGroup root = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_webview, null);
        builder.setContentView(root);
        ViewGroup placeholder = root.findViewById(R.id.placeholder_screenshot_share);
        ProgressBar progressBar = root.findViewById(R.id.progressBar);
        WebView webView = root.findViewById(R.id.webView);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                progressBar.setProgress(newProgress);
                if (newProgress >= 100) {
                    progressBar.setVisibility(View.GONE);
                    placeholder.setVisibility(View.GONE);
                }
            }
        });
        webView.loadUrl(url);
        builder.show();
    }

    private void onClickWatchVideo(View v) {
        boolean isSawDialog = getPrefs().getBoolean(PreferenceField.IS_SAW_ONCE_REWARDED_VIDEO, false);
        if (isSawDialog) {
            Appodeal.show(getActivity(), Appodeal.REWARDED_VIDEO);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Watch video")
                    .setMessage("Removes all ads banners for 1 hour and show short non skipable video?")
                    .setPositiveButton("Yeap", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getPrefs().edit()
                                    .putBoolean(PreferenceField.IS_SAW_ONCE_REWARDED_VIDEO, true)
                                    .apply();
                            Appodeal.show(getActivity(), Appodeal.REWARDED_VIDEO);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .show();
        }
    }

    public void onClickFeedback(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Leaving feedback");
        View rootView = getLayoutInflater().inflate(R.layout.dialog_feedback, null, false);
        builder.setView(rootView);
        builder.setCancelable(false);
        builder.setPositiveButton("Send", (dialogInterface, i) -> {
            Map<String, Object> user = new HashMap<>();

            TextInputEditText nameView = rootView.findViewById(R.id.et_name);
            TextInputEditText messageView = rootView.findViewById(R.id.et_message);
            SeekBar experienceView = rootView.findViewById(R.id.sb_experience);

            StringBuilder message = new StringBuilder();
            message.append("Rating: ").append(experienceView.getProgress()).append("\n");
            message.append("Message: " + messageView.getText().toString() + "\n");

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference table = database.getReference("feedbacks");
            HashMap<String, String> result = new HashMap<>();
            result.put("username", nameView.getText().toString());
            result.put("rating", "" + experienceView.getProgress());
            result.put("message", messageView.getText().toString());
            //String keyFromPush = table.push().getKey();
            Bugfender.sendUserFeedbackReturningUrl(nameView.getText().toString(), message.toString());
            table.push().setValue(result);
        });
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());
        builder.setIcon(R.drawable.ic_suggest);
        builder.show();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_trademarks) {
            openTrademarks();
        } else if (id == R.id.nav_legaluse) {
            openLegalUse();
        } else if (id == R.id.nav_private_policy) {
            openPrivatePolicy();
        } else if (id == R.id.nav_faq) {
            openFAQInfo();
        }
        DrawerLayout drawer = rootView.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openStreams() {
        String url = "https://www.twitch.tv/directory/game/Dota%20Underlords";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void openPrivatePolicy() {
        String url = Config.PRIVATE_POLICY;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void openTrademarks() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Trademarks");
        builder.setMessage(R.string.trademarks);
        builder.show();
    }

    private void openLegalUse() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Legal use");
        builder.setMessage(R.string.legal_use);
        builder.show();
    }

    public void onClickTinderlords(View v) {
        Intent i = new Intent(getActivity(), TinderlordsActivity.class);
        startActivity(i);
    }

    public void onClickNews(View v) {
        Intent i = new Intent(getActivity(), ActivityNews.class);
        startActivity(i);
    }

    public void onClickAlliances(View v) {
        Intent i = new Intent(getActivity(), AlliancesFragment.class);
        startActivity(i);
    }

    public void onClickSynergy(View v) {
        Intent i = new Intent(getActivity(), SynergyActivity.class);
        startActivity(i);
    }

    public void onClickTeams(View v) {
        Intent i = new Intent(getActivity(), TeamsFragment.class);
        startActivity(i);
    }

    public void onClickBuilder(View v) {
        Intent i = new Intent(getActivity(), TeamBuilderActivity.class);
        startActivity(i);
    }

    public void onClickHeroes(View v) {
        Intent i = new Intent(getActivity(), HeroesFragment.class);
        startActivity(i);
    }

    public void onClickItems(View v) {
        Intent i = new Intent(getActivity(), ItemsFragment.class);
        startActivity(i);
    }

    public void onClickRanking(View v) {
        Intent i = new Intent(getActivity(), RanksFragment.class);
        startActivity(i);
    }

    private void joinDiscord(View v) {
        String url = "https://discord.gg/hZejHXE";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}