package com.ultimateguides.dota.screens.main.teams.editorsteams;

import androidx.annotation.NonNull;

import com.bugfender.sdk.Bugfender;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.ultimateguides.dota.Config;
import com.ultimateguides.dota.database.AppDatabase;
import com.ultimateguides.dota.database.TeamEntity;
import com.ultimateguides.dota.database.TeamsDao;
import com.ultimateguides.dota.model.firebase.CommunityTeamData;
import com.ultimateguides.dota.model.firebase.TeamModel;
import com.ultimateguides.dota.model.firebase.TeamRatingData;
import com.ultimateguides.dota.model.spreadsheet.ResponseAddionsModel;
import com.ultimateguides.dota.others.Action1;
import com.ultimateguides.dota.screens.ParentPresenter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class EditorsTeamsPresenter extends ParentPresenter<TeamModel> {

    public EditorsTeamsPresenter(String tableRange) {
        super(tableRange);
    }


//    public Single requestFull() {
//        return Single.fromCallable(() -> requestRating())
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread());
//
//    }

    public void requestRating() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference table = database.getReference("predefined_teams_user_rating");
        table.keepSynced(true);
        table.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                DataSnapshot child = dataSnapshot.child("hunter - warrior");
//                TeamRatingData teamRating1 = child.getValue(TeamRatingData.class);

                GenericTypeIndicator<Map<String, TeamRatingData>> value = new GenericTypeIndicator<Map<String, TeamRatingData>>() {
                };
                Map<String, TeamRatingData> teamRating2 = dataSnapshot.getValue(value);
                //onComplete.call(teamRating2);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

    public Completable addToLocalDatabase(TeamModel model) {
        TeamEntity entity = new TeamEntity(model);
        TeamsDao dao = AppDatabase.getInstance().teamsDao();
        return Completable.fromCallable(() -> dao.addTeam(entity))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void requestRating(Action1<Map<String, TeamRatingData>> onComplete) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference table = database.getReference("predefined_teams_user_rating");
        table.keepSynced(true);
        table.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                DataSnapshot child = dataSnapshot.child("hunter - warrior");
//                TeamRatingData teamRating1 = child.getValue(TeamRatingData.class);

                GenericTypeIndicator<Map<String, TeamRatingData>> value = new GenericTypeIndicator<Map<String, TeamRatingData>>() {
                };
                Map<String, TeamRatingData> teamRating2 = dataSnapshot.getValue(value);
                onComplete.call(teamRating2);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

//    public ArrayList<TeamModel> requestData(@NotNull Map<String, TeamRatingData> teamRating) {
//        try {
//            ValueRange result = sheetsService.spreadsheets().values()
//                    .get(Config.spreadsheet_additions_id, TABLE_RANGE)
//                    .setKey(Config.google_api_key)
//                    .execute();
//
//            String factory = result.toPrettyString();
//            ResponseAddionsModel response = new Gson().fromJson(factory, ResponseAddionsModel.class);
//
//            ArrayList<TeamModel> list = new ArrayList<>();
//            for (int i = 0; i < response.values.size(); i++) {
//                List<String> raw = response.values.get(i);
//                try {
//                    TeamModel model = new TeamModel(raw);
//                    model.userRating = teamRating.get(model.name).userRating;
//                    list.add(model);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            return list;
//        } catch (IOException e) {
//            Bugfender.sendIssueReturningUrl("HeroOLD parsing error", e.getMessage());
//            e.printStackTrace();
//        }
//        return null;
//    }

//    public Single<ArrayList<TeamModel>> requestDataAsync(Map<String, TeamRatingData> teamRating) {
//        return Single.fromCallable(() -> requestData(teamRating))
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread());
//    }

//    public void requestDataNew() {
//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference table = database.getReference("predefined_teams");
//        table.keepSynced(true);
//        table.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                DataSnapshot child = dataSnapshot.child("hunter - warrior");
//                TeamData data2 = child.getValue(TeamData.class);
//
//                if (data2.heroes.get(0).getType().equals(HeroOLD.IHeroType.Abaddon)) {
//
//                }
//
//
////                String type1 = data2.heroes.get(0);
//                System.out.println();
////                List<TeamModel> listRes = new ArrayList<>();
////                for (DataSnapshot dataValues : dataSnapshot.getChildren()) {
////                    HeroMainData model = dataValues.getValue(HeroMainData.class);
////                    System.out.println();
////                    listRes.add(model);
////                }
////                Log.d("Tag", "onDataChange: " + listRes.size());
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//
//        });
//    }

    public ArrayList<TeamModel> requestData() {
        try {
            ValueRange result = sheetsService.spreadsheets().values()
                    .get(Config.spreadsheet_additions_id, TABLE_RANGE)
                    .setKey(Config.google_api_key)
                    .execute();

            String factory = result.toPrettyString();
            ResponseAddionsModel response = new Gson().fromJson(factory, ResponseAddionsModel.class);

            ArrayList<TeamModel> list = new ArrayList<>();
            for (int i = 0; i < response.values.size(); i++) {
                List<String> raw = response.values.get(i);
                try {
//                    TeamModel model = new TeamModel(raw);
//                    list.add(model);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return list;
        } catch (IOException e) {
            Bugfender.sendIssueReturningUrl("HeroOLD parsing error", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }


}
