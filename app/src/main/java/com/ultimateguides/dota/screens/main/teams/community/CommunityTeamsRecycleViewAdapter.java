package com.ultimateguides.dota.screens.main.teams.community;


import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.HeroImages;
import com.ultimateguides.dota.model.firebase.CommunityTeamData;
import com.ultimateguides.dota.model.firebase.TeamModel;
import com.ultimateguides.dota.others.Action1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CommunityTeamsRecycleViewAdapter extends RecyclerView.Adapter<CommunityTeamsRecycleViewAdapter.ViewHolderItem> {
    List<CommunityTeamData> data;
    private Action1<Integer> onItemClick;
    private Action1<Integer> onRatingClick;
    private Action1<Integer> onBookmarkClick;
    HeroImages images = new HeroImages();
    private int lastPosition;

    public CommunityTeamsRecycleViewAdapter(List<CommunityTeamData> data) {
        this.data = data;
    }

    @Override
    public CommunityTeamsRecycleViewAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_team_community, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(CommunityTeamsRecycleViewAdapter.ViewHolderItem holder, int position) {
        CommunityTeamData dataItem = getItem(position);
        holder.name.setText(dataItem.name);
        holder.badge.setText(dataItem.username);
        holder.rating.setText("" + dataItem.rating);

        if (dataItem.heroes.size() >= 1) {
            holder.hero1.setImageResource(images.get(dataItem.heroes.get(0)));
        }

        for (int i = 0; i < holder.heroPortraits.size(); i++) {
            AppCompatImageView portrait = holder.heroPortraits.get(i);
            if (dataItem.heroes.size() - 1 >= i) {
                portrait.setImageResource(images.get(dataItem.heroes.get(i)));
            } else {
                portrait.setImageResource(R.color.transparent);
            }
        }

        String lordSaved = dataItem.lordId;
        if (!TextUtils.isEmpty(lordSaved)) {
            int lordIcon = R.drawable.placeholder_hero;
            switch (lordSaved) {
                case "annesix":
                    lordIcon = R.drawable.lord_annesix;
                    break;
                case "hobgen":
                    lordIcon = R.drawable.lord_hobgen;
                    break;
            }
            holder.lordView.setImageResource(lordIcon);
        }
        holder.root.setOnClickListener(view -> onItemClick.call(position));
        holder.ratingUp.setOnClickListener(view -> {
            //dataItem.userRating += 1;
            onRatingClick.call(position);
        });

        holder.bookmark.setOnClickListener(view -> {
            onBookmarkClick.call(position);
        });

        if (!TextUtils.isEmpty(dataItem.dateModified)) {
            holder.modified.setText(dataItem.dateModified);
        }
        setAnimation(holder.root, position);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(1f, 1f, 0f, 1.0f,
                    Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF, 0f);
//        anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            anim.setDuration(300);//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    private CommunityTeamData getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<Integer> onItemClick) {
        this.onItemClick = onItemClick;
    }

    public void setOnRatingClick(Action1<Integer> onItemClick) {
        this.onRatingClick = onItemClick;
    }

    public void setOnBookmarkClick(Action1<Integer> onBookmarkClick) {
        this.onBookmarkClick = onBookmarkClick;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView name;
        TextView badge;
        TextView modified;
        TextView rating;
        AppCompatImageView hero1;
        AppCompatImageView hero2;
        AppCompatImageView hero3;
        AppCompatImageView hero4;
        AppCompatImageView hero5;
        AppCompatImageView hero6;
        AppCompatImageView hero7;
        AppCompatImageView hero8;
        AppCompatImageView hero9;
        AppCompatImageView hero10;
        List<AppCompatImageView> heroPortraits;
        RoundedImageView lordView;
        ImageView ratingUp;
        AppCompatImageView bookmark;

        public ViewHolderItem(View itemView) {
            super(itemView);
            heroPortraits = new ArrayList<>();
            root = (ViewGroup) itemView;
            name = itemView.findViewById(R.id.tv_name);
            badge = itemView.findViewById(R.id.tv_badge);
            rating = itemView.findViewById(R.id.tv_rating);
            hero1 = itemView.findViewById(R.id.iv_hero_1);
            hero2 = itemView.findViewById(R.id.iv_hero_2);
            hero3 = itemView.findViewById(R.id.iv_hero_3);
            hero4 = itemView.findViewById(R.id.iv_hero_4);
            hero5 = itemView.findViewById(R.id.iv_hero_5);
            hero6 = itemView.findViewById(R.id.iv_hero_6);
            hero7 = itemView.findViewById(R.id.iv_hero_7);
            hero8 = itemView.findViewById(R.id.iv_hero_8);
            hero9 = itemView.findViewById(R.id.iv_hero_9);
            hero10 = itemView.findViewById(R.id.iv_hero_10);
            lordView = itemView.findViewById(R.id.iv_lord);
            heroPortraits.add(hero1);
            heroPortraits.add(hero2);
            heroPortraits.add(hero3);
            heroPortraits.add(hero4);
            heroPortraits.add(hero5);
            heroPortraits.add(hero6);
            heroPortraits.add(hero7);
            heroPortraits.add(hero8);
            heroPortraits.add(hero9);
            heroPortraits.add(hero10);

            ratingUp = itemView.findViewById(R.id.iv_rating);
            bookmark = itemView.findViewById(R.id.iv_bookmark);
            modified = itemView.findViewById(R.id.tv_modified);
        }
    }
}
