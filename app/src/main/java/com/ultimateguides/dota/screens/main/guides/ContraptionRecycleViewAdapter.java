package com.ultimateguides.dota.screens.main.guides;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimateguides.dota.R;
import com.ultimateguides.dota.others.Action1;

import java.util.List;

public class ContraptionRecycleViewAdapter extends RecyclerView.Adapter<ContraptionRecycleViewAdapter.ViewHolderItem> {

    private List<ContraptionModel> data;
    private Action1<Integer> onItemClick;

    public ContraptionRecycleViewAdapter(List<ContraptionModel> data) {
        this.data = data;
    }

    @Override
    public ContraptionRecycleViewAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_guide_corraption, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(ContraptionRecycleViewAdapter.ViewHolderItem holder, int position) {
        ContraptionModel dataItem = getItem(position);
        holder.nameView.setText(dataItem.name);
        holder.descriptionView.setText(dataItem.description);
        holder.icon.setImageResource(dataItem.icon);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private ContraptionModel getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<Integer> onItemClick) {
        this.onItemClick = onItemClick;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        ImageView icon;
        TextView nameView;
        TextView descriptionView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            icon = itemView.findViewById(R.id.icon);
            nameView = itemView.findViewById(R.id.tv_name);
            descriptionView = itemView.findViewById(R.id.tv_description);
        }
    }
}
