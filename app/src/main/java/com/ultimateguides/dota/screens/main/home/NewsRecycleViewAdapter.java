package com.ultimateguides.dota.screens.main.home;


import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimateguides.dota.R;
import com.ultimateguides.dota.others.Action1;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.ultimateguides.dota.model.steamApi.NewsSteamModel.AppnewsBean.NewsitemsBean;

public class NewsRecycleViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<NewsitemsBean> data;
    private Action1<String> onItemClick;

    public NewsRecycleViewAdapter(List<NewsitemsBean> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_news, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NewsitemsBean dataItem = getItem(position);
        ((ViewHolderItem) holder).root.setOnClickListener(view -> onItemClick.call(dataItem.url));
        ((ViewHolderItem) holder).titleView.setText("" + dataItem.title);

        Date d = new Date(dataItem.date * 1000L);
        String pattern = "E, dd MMMM";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.US);
        String date = simpleDateFormat.format(d);

        ((ViewHolderItem) holder).dateView.setText("" + date);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private NewsitemsBean getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<String> onItemClick) {
        this.onItemClick = onItemClick;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView titleView;
        TextView dateView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            titleView = itemView.findViewById(R.id.tv_title);
            dateView = itemView.findViewById(R.id.tv_date);
        }
    }
}
