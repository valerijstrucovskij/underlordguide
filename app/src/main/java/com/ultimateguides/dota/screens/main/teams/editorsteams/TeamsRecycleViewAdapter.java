package com.ultimateguides.dota.screens.main.teams.editorsteams;


import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.HeroImages;
import com.ultimateguides.dota.model.firebase.TeamModel;
import com.ultimateguides.dota.others.Action1;

import java.util.ArrayList;
import java.util.List;

public class TeamsRecycleViewAdapter extends RecyclerView.Adapter<TeamsRecycleViewAdapter.ViewHolderItem> {
    List<TeamModel> data;
    private Action1<Integer> onItemClick;
    private Action1<Integer> onRatingClick;
    private Action1<Integer> onBookmarkClick;
    HeroImages images = new HeroImages();

    public TeamsRecycleViewAdapter(List<TeamModel> data) {
        this.data = data;
    }

    @Override
    public TeamsRecycleViewAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_team_preset, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(TeamsRecycleViewAdapter.ViewHolderItem holder, int position) {
        TeamModel dataItem = getItem(position);
//        Palette.r
        holder.name.setText(dataItem.name);
        holder.badge.setText(dataItem.badge);
        holder.rating.setText("" + dataItem.userRating);

        if (dataItem.heroes.size() >= 1) {
            holder.hero1.setImageResource(images.get(dataItem.heroes.get(0)));
        }

        for (int i = 0; i < holder.heroPortraits.size(); i++) {
            AppCompatImageView portrait = holder.heroPortraits.get(i);
            if (dataItem.heroes.size() - 1 >= i) {
                portrait.setImageResource(images.get(dataItem.heroes.get(i)));
            } else {
                portrait.setImageResource(R.color.transparent);
            }
        }

        String lordSaved = dataItem.lordId;
        if (!TextUtils.isEmpty(lordSaved)) {
            int lordIcon = R.drawable.placeholder_hero;
            switch (lordSaved) {
                case "annesix":
                    lordIcon = R.drawable.lord_annesix;
                    break;
                case "hobgen":
                    lordIcon = R.drawable.lord_hobgen;
                    break;
            }
            holder.lordView.setImageResource(lordIcon);
        }
        holder.root.setOnClickListener(view -> onItemClick.call(position));
        holder.ratingUp.setOnClickListener(view -> {
            dataItem.userRating += 1;
            onRatingClick.call(position);
        });

        holder.bookmark.setOnClickListener(view -> {
            onBookmarkClick.call(position);
        });

        if (!TextUtils.isEmpty(dataItem.modified)) {
            holder.modified.setText(dataItem.modified);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private TeamModel getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<Integer> onItemClick) {
        this.onItemClick = onItemClick;
    }

    public void setOnRatingClick(Action1<Integer> onItemClick) {
        this.onRatingClick = onItemClick;
    }

    public void setOnBookmarkClick(Action1<Integer> onBookmarkClick) {
        this.onBookmarkClick = onBookmarkClick;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView name;
        TextView badge;
        TextView modified;
        TextView rating;
        AppCompatImageView hero1;
        AppCompatImageView hero2;
        AppCompatImageView hero3;
        AppCompatImageView hero4;
        AppCompatImageView hero5;
        AppCompatImageView hero6;
        AppCompatImageView hero7;
        AppCompatImageView hero8;
        AppCompatImageView hero9;
        AppCompatImageView hero10;
        List<AppCompatImageView> heroPortraits;
        RoundedImageView lordView;
        ImageView ratingUp;
        AppCompatImageView bookmark;

        public ViewHolderItem(View itemView) {
            super(itemView);
            heroPortraits = new ArrayList<>();
            root = (ViewGroup) itemView;
            name = itemView.findViewById(R.id.tv_name);
            badge = itemView.findViewById(R.id.tv_badge);
            hero1 = itemView.findViewById(R.id.iv_hero_1);
            hero2 = itemView.findViewById(R.id.iv_hero_2);
            hero3 = itemView.findViewById(R.id.iv_hero_3);
            hero4 = itemView.findViewById(R.id.iv_hero_4);
            hero5 = itemView.findViewById(R.id.iv_hero_5);
            hero6 = itemView.findViewById(R.id.iv_hero_6);
            hero7 = itemView.findViewById(R.id.iv_hero_7);
            hero8 = itemView.findViewById(R.id.iv_hero_8);
            hero9 = itemView.findViewById(R.id.iv_hero_9);
            hero10 = itemView.findViewById(R.id.iv_hero_10);
            lordView = itemView.findViewById(R.id.iv_lord);
            heroPortraits.add(hero1);
            heroPortraits.add(hero2);
            heroPortraits.add(hero3);
            heroPortraits.add(hero4);
            heroPortraits.add(hero5);
            heroPortraits.add(hero6);
            heroPortraits.add(hero7);
            heroPortraits.add(hero8);
            heroPortraits.add(hero9);
            heroPortraits.add(hero10);

            rating = itemView.findViewById(R.id.tv_rating);
            ratingUp = itemView.findViewById(R.id.iv_rating);
            bookmark = itemView.findViewById(R.id.iv_bookmark);
            modified = itemView.findViewById(R.id.tv_modified);
        }
    }
}
