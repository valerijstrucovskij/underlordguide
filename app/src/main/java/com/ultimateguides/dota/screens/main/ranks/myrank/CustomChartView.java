package com.ultimateguides.dota.screens.main.ranks.myrank;

import android.content.Context;
import android.util.AttributeSet;

import com.github.mikephil.charting.charts.LineChart; 
import com.github.mikephil.charting.components.Legend;

public class CustomChartView extends LineChart {
    public CustomChartView(Context context) {
        super(context);
        setupChart(this);
    }

    public CustomChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupChart(this);
    }

    public CustomChartView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setupChart(this);
    }

    private void setupChart(LineChart chart) {
        //((LineDataSet) data.getDataSetByIndex(0)).setCircleHoleColor(color);
        chart.getDescription().setEnabled(false);
        // chart.setDrawHorizontalGrid(false);
        // enable / disable grid background
        chart.setDrawGridBackground(false);
//        chart.getRenderer().getGridPaint().setGridColor(Color.WHITE & 0x70FFFFFF);
        // enable touch gestures
        chart.setTouchEnabled(true);
        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(false);
        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false);
//        chart.setBackgroundColor(color);
        //TODO set custom chart offsets (automatic offset calculation is hereby disabled)
        chart.setViewPortOffsets(50, 0, 50, 0);
        // add data
        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setEnabled(false);
        chart.getAxisLeft().setEnabled(false);
        chart.getAxisLeft().setSpaceTop(40);
        chart.getAxisLeft().setSpaceBottom(40);
        chart.getAxisLeft().setInverted(true);
        chart.getAxisRight().setEnabled(false);
        chart.getXAxis().setEnabled(false);
        // animate calls invalidate()...
        chart.animateX(1500);
    }
}
