package com.ultimateguides.dota.screens.main.teams.community;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.firebase.CommunityTeamData;
import com.ultimateguides.dota.model.firebase.TeamModel;
import com.ultimateguides.dota.screens.teamdetails.TeamDetailsActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.ultimateguides.dota.FirebaseAnalyticsParams.FAVORITE_TEAM;

public class CommunityTeamsFragment extends Fragment {

    private CommunityTeamsPresenter presenter = new CommunityTeamsPresenter();
    private List<CommunityTeamData> dataList = new ArrayList<>();
    private CommunityTeamsRecycleViewAdapter adapter = new CommunityTeamsRecycleViewAdapter(dataList);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_teams_community, container, false);

        LinearLayout waiting = rootView.findViewById(R.id.waiting);
        RecyclerView recyclerView = rootView.findViewById(R.id.rv_teams);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClick(param -> {
            Intent i = new Intent(getContext(), TeamDetailsActivity.class);
            CommunityTeamData team = dataList.get(param);
            TeamModel modelTeam = new TeamModel();
            modelTeam.name = team.username;
            modelTeam.heroes = team.heroes;
            modelTeam.lordId = team.lordId;
            i.putExtra("model", modelTeam);
            startActivity(i);
        });

        adapter.setOnRatingClick(param -> {
            presenter.increaseRating(dataList.get(param));
        });

        adapter.setOnBookmarkClick(param -> {
            CommunityTeamData team = dataList.get(param);
            TeamModel modelTeam = new TeamModel();
            modelTeam.name = team.name;
            modelTeam.heroes = team.heroes;
            modelTeam.lordId = team.lordId;

            Bundle bundle = new Bundle();
            bundle.putString(FAVORITE_TEAM, modelTeam.heroes.toString());
            GuideApplication.getAnalytics().logEvent(FAVORITE_TEAM, bundle);

            presenter.addToLocalDatabase(modelTeam)
                    .subscribe(
                            () -> {
                                FancyToast.makeText(getActivity(), "Team " + team.name + " added to you bookmarks 'My Teams'",
                                        Toast.LENGTH_LONG,
                                        FancyToast.SUCCESS,
                                        false
                                ).show();
                                Log.d("RoomWithRx", "onCreateView: ");
                            },
                            throwable -> throwable.printStackTrace()
                    );
        });

        //-------------------------------------------------------
        waiting.setVisibility(View.VISIBLE);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference table = database.getReference("community_builds");
        table.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                waiting.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                dataList.clear();

                for (DataSnapshot itemSnapshot : dataSnapshot.getChildren()) {
                    String key = itemSnapshot.getKey();
                    CommunityTeamData model = itemSnapshot.getValue(CommunityTeamData.class);
                    model.key = key;
                    dataList.add(model);
                }

                Collections.sort(dataList, (t1, t2) -> {
//                            Date date1 = extractDate(t1.dateModified);
//                            Date date2 = extractDate(t2.dateModified);
//                            Date dateToday = new Date();
//                            int diff = Math.abs(dateToday.getDay() - date1.getDay());
//                            int diff2 = Math.abs(dateToday.getDay() - date2.getDay());
//                            if (diff < 3) {
//                                return 1;
//                            } else if (diff2 < 3) {
//                                return -1;
//                            } else {
//                                return (extractInt(t2.rating) - extractInt(t1.rating));
//                            }
                            return (extractInt(t2.rating) - extractInt(t1.rating));
                        }
                );
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
//        GuideApplication.getFirestore()
//                .collection("editors_teams")
//                .orderBy("editor_rate", Query.Direction.DESCENDING)
//                .get(Source.CACHE)
//                .addOnCompleteListener(task -> {
//                    waiting.setVisibility(View.GONE);
//                    recyclerView.setVisibility(View.VISIBLE);
//                    if (task.isSuccessful()) {
//                        dataList.clear();
//                        for (QueryDocumentSnapshot document : task.getResult()) {
//                            TeamModel model = document.toObject(TeamModel.class);
//                            dataList.add(model);
//                        }
//                        adapter.notifyDataSetChanged();
//                    } else {
//                        waiting.setVisibility(View.GONE);
//                        recyclerView.setVisibility(View.VISIBLE);
//
//                        Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
//                    }
//                });
        return rootView;
    }

    int extractInt(String s) {
        String num = s.replaceAll("\\D", "");
        // return 0 if no digits found
        return num.isEmpty() ? 0 : Integer.parseInt(num);
    }

    Date extractDate(String s) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return formatter.parse(s);
        } catch (ParseException e) {
            return null;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        GuideApplication.getAnalytics().setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());
    }
}