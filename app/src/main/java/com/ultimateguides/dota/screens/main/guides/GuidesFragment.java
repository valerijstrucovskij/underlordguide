package com.ultimateguides.dota.screens.main.guides;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.shawnlin.numberpicker.NumberPicker;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.TierModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GuidesFragment extends Fragment {

    private ArrayList<GuideModel> titles = new ArrayList<>();
    private ViewGroup rootView;

    public GuidesFragment() {

    }

    public static GuidesFragment newInstance() {
        GuidesFragment fragment = new GuidesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_guides_list, container, false);

//        titles.add(new GuideHeaderModel("Must to know"));
        titles.add(new GuideModel("must to know", "Shared pool and Heroes"));
        titles.add(new GuideModel("must to know", "Hotkeys/shortcuts"));
//        titles.add(new GuideHeaderModel("In game info"));
        titles.add(new GuideModel("basic", "Neutral waves"));
        titles.add(new GuideModel("basic", "Seasonal rank system"));
        titles.add(new GuideModel("basic", "Contraptions"));
        titles.add(new GuideModel("tools", "Level up calculator"));

//        titles.add(new GuideHeaderModel("Coming soon"));
        titles.add(new GuideModel("coming soon", "Underlord characters"));

        RecyclerView recycleView = rootView.findViewById(R.id.recycleView);
        GuidesRecycleViewAdapter adapter = new GuidesRecycleViewAdapter(titles);
        final GridLayoutManager layoutManager = (GridLayoutManager) (recycleView.getLayoutManager());
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (titles.get(position).isHeader()) ? layoutManager.getSpanCount() : 1;
            }
        });

        recycleView.setAdapter(adapter);
        adapter.setOnItemClick(pos -> {
            switch (pos) {
                case 2:
                    showDialog(getGuideLootWaves());
                    break;
                case 0:
                    showDialog(getGuideSharedPool());
                    break;
                case 3:
                    showDialog(getGuideRanks());
                    break;
                case 1:
                    showDialog(getGuideKeyboard());
                    break;
                case 4:
                    showDialog(getGuideContraption());
                    break;
                case 5:
                    showDialog(getLevelupCalculator());
                    break;
                case 6:
                    Toast.makeText(getActivity(), "Will be added soon", Toast.LENGTH_SHORT).show();
                    break;
            }
        });
        return rootView;
    }

    private void showDialog(View view) {
        BottomSheetDialog builder = new BottomSheetDialog(getActivity());
        builder.setContentView(view);
        builder.setCancelable(true);
        builder.show();
    }

    //GUIDES-------------------------

    private ViewGroup getGuideLootWaves() {
        ViewGroup view = (ViewGroup) getLayoutInflater().inflate(R.layout.layout_guide_loot_levels, null);
        RecyclerView tierRecycleView = view.findViewById(R.id.rv_loot_levels);
        List<LootLevelModel> tierList = new ArrayList<>();

        tierList.add(new LootLevelModel("1", "Warmup",
                "2 lane creeps", "1", "Tier 1: 100%", R.drawable.image_wave_1));
        tierList.add(new LootLevelModel("2", "Sparring match",
                "1 mega creep, 2 lane", "1/2", "T1: 100%, T2: 20%", R.drawable.image_wave_2));
        tierList.add(new LootLevelModel("3", "Gear up",
                "2 mega creep, 4 lane", "1/2", "T1: 70%, T2: 30%", R.drawable.image_wave_3));
        tierList.add(new LootLevelModel("10", "First intermission",
                "3 rock golems", "1/2", "T1: 20%, T2: 80%", R.drawable.image_wave_10));
        tierList.add(new LootLevelModel("15", "Dog and pony show",
                "5 jumping dogs", "2/3", "T2: 70%, T3: 30%", R.drawable.image_wave_15));
        tierList.add(new LootLevelModel("20", "Bear trap!",
                "2 hellbears", "2/3", "T2: 30%, T3: 70%", R.drawable.image_wave_20));
        tierList.add(new LootLevelModel("25", "Survival of the fittest",
                "2 angry birds", "3/4", "T3: 70%, T4: 30%", R.drawable.image_wave_25));
        tierList.add(new LootLevelModel("30", "Big dinos, big rewards",
                "3 big dinos", "3/4", "T3: 50%, T4: 50%", R.drawable.image_wave_30));
        tierList.add(new LootLevelModel("35", "Things are heating up",
                "1 massive dragon", "3/4", "T3: 20%, T4: 80%", R.drawable.image_wave_35));
        tierList.add(new LootLevelModel("40", "You're about to be trolled",
                "3 mystic trolls", "4/5", "T4: 70%, T5: 30%", R.drawable.image_wave_40));
        tierList.add(new LootLevelModel("45", "We've reached the endgame",
                "The Year Beast", "4/5", "T4: 50%, T5: 50%", R.drawable.image_wave_45));
        tierList.add(new LootLevelModel("50", "Correction, now we've reached the endgame",
                "Roshan", "4/5", "T4: 40%, T5: 60%", R.drawable.image_wave_50));
        LootLevelsRecycleViewAdapter tierAdapter = new LootLevelsRecycleViewAdapter(tierList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        tierRecycleView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_white));
        tierRecycleView.addItemDecoration(dividerItemDecoration);
        tierRecycleView.setAdapter(tierAdapter);
        return view;
    }

    private ViewGroup getGuideRanks() {
        ViewGroup view = (ViewGroup) getLayoutInflater().inflate(R.layout.layout_guide_ranks, null);
        return view;
    }

    private HashMap<Integer, Integer> levelExp = new HashMap<>();
    int level = 1;
    int xp = 0;
    int gold = 0;

    private ViewGroup getLevelupCalculator() {
        ViewGroup view = (ViewGroup) getLayoutInflater().inflate(R.layout.layout_levelup_calculator, null);
        NumberPicker levelView = view.findViewById(R.id.np_level);
        NumberPicker xpView = view.findViewById(R.id.np_xp);
        NumberPicker goldView = view.findViewById(R.id.np_gold);

        levelExp.put(1, 1);
        levelExp.put(2, 1);
        levelExp.put(3, 2);
        levelExp.put(4, 4);
        levelExp.put(5, 8);
        levelExp.put(6, 16);
        levelExp.put(7, 24);
        levelExp.put(8, 32);
        levelExp.put(9, 40);

        levelView.setOnValueChangedListener((picker, oldVal, newVal) -> {
            level = newVal;
            calculateLevelup(view, level, xp, gold);
        });
        xpView.setOnValueChangedListener((picker, oldVal, newVal) -> {
            xp = newVal;
            calculateLevelup(view, level, xp, gold);
        });
        goldView.setOnValueChangedListener((picker, oldVal, newVal) -> {
            gold = newVal;
            calculateLevelup(view, level, xp, gold);
        });
        return view;
    }


    private void calculateLevelup(View root, int level, int xp, int gold) {
        int xpToNext = levelExp.get(level);
        xpToNext -= xp;
        if (xpToNext < 0) {
            root.setBackgroundResource(R.color.tier_2);
            return;
        }
        int buyCount = gold / 5;
        if (xp + buyCount * 4 >= levelExp.get(level)) {
            root.setBackgroundResource(R.color.tier_2);
        } else {
            root.setBackgroundResource(R.color.tier_5);
        }
    }

    private ViewGroup getGuideContraption() {
        ViewGroup view = (ViewGroup) getLayoutInflater().inflate(R.layout.layout_guide_keyboard, null);
        RecyclerView recycleView = view.findViewById(R.id.rv_keyboard);
        List<ContraptionModel> list = new ArrayList<>();
        list.add(new ContraptionModel("Barricade", R.drawable.contraption_barricade,
                "- Tier 2 Item\n" +
                        "- Places 2 Barricades on Board\n" +
                        "- 400 Health, 20 Armor, Immune to Spells\n" +
                        "- Blocks opposing ranged attacks until they break"
        ));
        list.add(new ContraptionModel("Target Buddy", R.drawable.contraption_target_buddy,
                "- Tier 2 Item\n" +
                        "- 1000 HP, 10 Armor\n" +
                        "- Taunts enemies in melee range for 3 seconds, 10 second CD\n" +
                        "- Can equip Items"
        ));
        list.add(new ContraptionModel("Healing Ward", R.drawable.contraption_healing_ward,
                "- Tier 3 Item\n" +
                        "- 200 Health, 0 armor\n" +
                        "- Heals adjacent allies for 20 health per second"
        ));
        list.add(new ContraptionModel("Tombstone", R.drawable.contraption_tombstone,
                "- Tier 4 Item\n" +
                        "- 2000 Health, 20 Armor\n" +
                        "- When any hero dies within 2 squares, summon a zombie\n" +
                        "- Zombies scales with the star level of the hero killed"
        ));
        ContraptionRecycleViewAdapter adapter = new ContraptionRecycleViewAdapter(list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        recycleView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_white));
        recycleView.addItemDecoration(dividerItemDecoration);
        recycleView.setAdapter(adapter);
        return view;
    }

    private ViewGroup getGuideKeyboard() {
        ViewGroup view = (ViewGroup) getLayoutInflater().inflate(R.layout.layout_guide_keyboard, null);
        RecyclerView tierRecycleView = view.findViewById(R.id.rv_keyboard);
        List<GuideModel> list = new ArrayList<>();
        list.add(new GuideModel("Tab", "Show next player in order shown on scoreboard to the left (will change as people lose health)"));
        list.add(new GuideModel("F1", "Show your board"));
        list.add(new GuideModel("Numpad", "Show a player (the assigned number doesn't change throughout the match)"));
        list.add(new GuideModel("Q*", "Lock units for purchase (*Works while shop is closed)"));
        list.add(new GuideModel("W", "Move selected unit to the first available slot on bench (whether it's on the board or already on the bench)"));
        list.add(new GuideModel("E", "Sell selected unit"));
        list.add(new GuideModel("R*", "Refresh units in shop"));
        list.add(new GuideModel("T*", "Purchase exp"));
        list.add(new GuideModel("Y", "Chat wheel"));
        list.add(new GuideModel("U", "Open items panel with cursor (yellow dot on first item). You can then use arrow keys to select item, Enter to enter equip mode, arrow keys to select the unit, then Enter again to equip the item. Esc to cancel and go back one mode. "));
        list.add(new GuideModel("I", "Items panel"));
        list.add(new GuideModel("O", "Damage panel"));
        list.add(new GuideModel("P", "Synergy panel"));
        list.add(new GuideModel("Mousewheel ", "Change panel"));
        list.add(new GuideModel("A", "During combat, view player board being attacked by you"));
        list.add(new GuideModel("S", "During combat, view player board attacking you"));
        list.add(new GuideModel("Space bar", "Open/close shop"));
        list.add(new GuideModel("1-5", "Buy unit in that slot"));
        list.add(new GuideModel("1-3", "Select loot in that slot if loot available (1-4 with the Embarrassment of Riches global item)"));
        list.add(new GuideModel("Arrow keys", "Select units around the board. Enter to pick up, arrow keys to move, and Enter again to place"));
        list.add(new GuideModel("RMB (right mouse button)", "Move selected unit to cursor position"));
        KeyboardRecycleViewAdapter adapter = new KeyboardRecycleViewAdapter(list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        tierRecycleView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_white));
        tierRecycleView.addItemDecoration(dividerItemDecoration);
        tierRecycleView.setAdapter(adapter);
        return view;
    }

    private ViewGroup getGuideSharedPool() {
        ViewGroup view = (ViewGroup) getLayoutInflater().inflate(R.layout.layout_guide_shared_pool, null);
        RecyclerView tierRecycleView = view.findViewById(R.id.rv_tier_draw);
        List<TierModel> tierList = new ArrayList<>();
        tierList.add(new TierModel("Level", "Tier 1", "Tier 2", "Tier 3", "Tier 4", "Tier 5"));
        tierList.add(new TierModel("1", "100%", "0%", "0%", "0%", "0%"));
        tierList.add(new TierModel("2", "70%", "30%", "0%", "0%", "0%"));
        tierList.add(new TierModel("3", "60%", "35%", "5%", "0%", "0%"));
        tierList.add(new TierModel("4", "50%", "35%", "15%", "0%", "0%"));
        tierList.add(new TierModel("5", "40%", "35%", "25%", "0%", "0%"));
        tierList.add(new TierModel("6", "30%", "30%", "30%", "10%", "0%"));
        tierList.add(new TierModel("7", "25%", "30%", "30%", "15%", "0%"));
        tierList.add(new TierModel("8", "22%", "27%", "30%", "20%", "1%"));
        tierList.add(new TierModel("9", "20%", "23%", "29%", "25%", "3%"));
        tierList.add(new TierModel("10", "15%", "21%", "28%", "30%", "6%"));
        tierList.add(new TierModel("10+", "15%", "20%", "25%", "30%", "10%"));
        tierList.add(new TierModel("Count", "45", "30", "25", "15", "10"));
        TiersRecycleViewAdapter tierAdapter = new TiersRecycleViewAdapter(tierList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        tierRecycleView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_white));
        tierRecycleView.addItemDecoration(dividerItemDecoration);
        tierRecycleView.setAdapter(tierAdapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        GuideApplication.getAnalytics().setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());
    }
}
