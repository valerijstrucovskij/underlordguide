package com.ultimateguides.dota.screens.main.guides;

public class KeyboardModel {

    public String key;
    public String description;

    public KeyboardModel(String key, String description) {
        this.key = key;
        this.description = description;
    }
}
