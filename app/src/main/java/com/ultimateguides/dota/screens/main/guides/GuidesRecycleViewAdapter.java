package com.ultimateguides.dota.screens.main.guides;


import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ComplexColorCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.resources.TextAppearance;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.others.Action1;

import java.util.List;

public class GuidesRecycleViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int SECTION_TYPE = 0;
    private static final int NORMAL_TYPE = 1;
    List<GuideModel> data;
    private Action1<Integer> onItemClick;

    public GuidesRecycleViewAdapter(List<GuideModel> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;
        if (viewType == SECTION_TYPE) {
            v = new TextView(parent.getContext());
            return new ViewHolderHeaderItem(v);
        } else {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_guide, parent, false);
            return new ViewHolderItem(v);
        }


    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).isHeader()
                ? SECTION_TYPE
                : NORMAL_TYPE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (data.get(position).isHeader()) {
            GuideHeaderModel model = (GuideHeaderModel) getItem(position);
            ((ViewHolderHeaderItem) holder).nameView.setText(model.name);
        } else {
            GuideModel dataItem = getItem(position);
            GuidesRecycleViewAdapter.ViewHolderItem holderLoc = (ViewHolderItem) holder;
            holderLoc.nameView.setText(dataItem.name);
            holderLoc.categoryView.setText(dataItem.category);
            holderLoc.root.setOnClickListener(view -> onItemClick.call(position));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private GuideModel getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<Integer> onItemClick) {
        this.onItemClick = onItemClick;
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView categoryView;
        TextView nameView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            categoryView = itemView.findViewById(R.id.tv_category);
            nameView = itemView.findViewById(R.id.tv_name);
        }
    }

    class ViewHolderHeaderItem extends RecyclerView.ViewHolder {
        TextView nameView;

        public ViewHolderHeaderItem(View v) {
            super(v);

            nameView = (TextView) v;
            nameView.setTextAppearance(v.getContext(), android.R.style.TextAppearance_Large);
            int padding = v.getContext().getResources().getDimensionPixelSize(R.dimen.default_padding);
            nameView.setPadding(padding, padding * 2, 0, padding/2);
            nameView.setGravity(Gravity.CENTER_HORIZONTAL);
            nameView.setTextColor(ContextCompat.getColor(v.getContext(), R.color.gray_text_disabled));
        }
    }
}
