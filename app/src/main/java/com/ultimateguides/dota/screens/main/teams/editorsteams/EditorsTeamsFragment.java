package com.ultimateguides.dota.screens.main.teams.editorsteams;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.Source;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.firebase.TeamModel;
import com.ultimateguides.dota.screens.teamdetails.TeamDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import static com.ultimateguides.dota.FirebaseAnalyticsParams.FAVORITE_TEAM;

public class EditorsTeamsFragment extends Fragment {

    private EditorsTeamsPresenter presenter = new EditorsTeamsPresenter("PredefinedTeams!A2:N2");
    private List<TeamModel> dataList = new ArrayList<>();
    private TeamsRecycleViewAdapter adapter = new TeamsRecycleViewAdapter(dataList);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_teams_prebuild, container, false);

        LinearLayout waiting = rootView.findViewById(R.id.waiting);
        RecyclerView recyclerView = rootView.findViewById(R.id.rv_teams);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClick(param -> {
            Intent i = new Intent(getContext(), TeamDetailsActivity.class);
            i.putExtra("model", dataList.get(param));
            startActivity(i);
        });

        adapter.setOnRatingClick(param -> {
            //presenter.increaseRating(dataList.get(param));
        });

        adapter.setOnBookmarkClick(param -> {
            TeamModel team = dataList.get(param);

            Bundle bundle = new Bundle();
            bundle.putString(FAVORITE_TEAM, team.name);
            GuideApplication.getAnalytics().logEvent(FAVORITE_TEAM, bundle);

            presenter.addToLocalDatabase(team)
                    .subscribe(
                            () -> {
                                FancyToast.makeText(getActivity(),
                                        "Team " + team.name + " added to you bookmarks 'My Teams'",
                                        Toast.LENGTH_LONG,
                                        FancyToast.SUCCESS,
                                        false)
                                        .show();
                                Log.d("RoomWithRx", "onCreateView: ");
                            },
                            throwable -> throwable.printStackTrace()
                    );
        });

        //-------------------------------------------------------
        waiting.setVisibility(View.VISIBLE);

        GuideApplication.getFirestore()
                .collection("editors_teams")
                .orderBy("editor_rate", Query.Direction.DESCENDING)
                .get(Source.CACHE)
                .addOnCompleteListener(task -> {
                    waiting.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    if (task.isSuccessful()) {
                        dataList.clear();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            TeamModel model = document.toObject(TeamModel.class);
                            dataList.add(model);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        waiting.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        FancyToast.makeText(getContext(),
                                "Error",
                                Toast.LENGTH_LONG, FancyToast.ERROR, false
                        ).show();
                    }
                });


//            presenter.requestDataAsync(param)
//                    .subscribe(result -> {
//                        waiting.setVisibility(View.GONE);
//                        recyclerView.setVisibility(View.VISIBLE);
//
//                        dataList.clear();
//                        dataList.addAll(result);
//                        Collections.sort(dataList, (t1, t2) -> {
////                            int modCompare = t2.modified.compareTo(t1.modified);
////                            if (modCompare != 0) {
////                                return modCompare;
////                            }
//                            return t2.userRating - t1.userRating;
//                        });
//                        adapter.notifyDataSetChanged();
//                    }, error -> {
//
//                        //TODO load from last
//                    });
//        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        GuideApplication.getAnalytics().setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());
    }
}