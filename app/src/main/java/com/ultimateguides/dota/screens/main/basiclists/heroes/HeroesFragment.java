package com.ultimateguides.dota.screens.main.basiclists.heroes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Source;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.ultimateguides.dota.FirestoreFactory;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.HeroImages;
import com.ultimateguides.dota.customviews.GridSpacingItemDecoration;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.model.firebase.HeroData;
import com.ultimateguides.dota.model.firebase.HeroMainData;
import com.ultimateguides.dota.model.firebase.HeroSkillsData;
import com.ultimateguides.dota.screens.herodetails.HeroDetailsActivity;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HeroesFragment extends Fragment {

    public static int COLUMN = 4;
    private List<HeroModel> list = new ArrayList<>();
    private HeroesRecycleViewAdapter adapter = new HeroesRecycleViewAdapter(list);
    private ExpandableLayout expendableTier;
    private ViewGroup rootView;

    private ImageView hero1CompareView;
    private ImageView hero2CompareView;
    MaterialButton btnCompare;
    private HeroModel hero1Compare;
    private HeroModel hero2Compare;

    public HeroesFragment() {
    }

    public static HeroesFragment newInstance() {
        HeroesFragment fragment = new HeroesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_heroes, container, false);
        LinearLayout waiting = rootView.findViewById(R.id.waiting);
//        expendableTier = rootView.findViewById(R.id.expandable_tier);
        rootView.findViewById(R.id.btn_show_tier).setOnClickListener(this::onClickExpandable);
        RecyclerView recyclerView = rootView.findViewById(R.id.rv_heroes);
        GridLayoutManager grid = new GridLayoutManager(getActivity(), COLUMN);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(COLUMN, 10, false));
        recyclerView.setLayoutManager(grid);

        //-------------
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClick(param -> {
            Intent i = new Intent(getActivity(), HeroDetailsActivity.class);
            i.putExtra("model", param);
            startActivity(i);
//            HeroDetailsDialog dialog = new HeroDetailsDialog(this, param);
//            dialog.show();
        });

        adapter.setOnLongItemClick(param -> {
            if (hero1Compare == null) {
                hero1Compare = param;
            } else if (hero2Compare == null) {
                hero2Compare = param;
            } else {
                FancyToast.makeText(getActivity(),
                        "Only 2 heroes allowed",
                        Toast.LENGTH_SHORT, FancyToast.INFO, false
                ).show();
            }
            updateComparatorView();
        });

        //-------------
        SearchView searchView = rootView.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String text = newText;
                adapter.getFilter().filter(text);
                return false;
            }
        });

        MaterialButtonToggleGroup sortBy = rootView.findViewById(R.id.group);
        sortBy.addOnButtonCheckedListener((group, checkedId, isChecked) -> {
            if (isChecked) {
                sortBy(checkedId);
                adapter.notifyDataSetChanged();
            } else {
                if (-1 == checkedId) group.check(R.id.btn_name);
            }
        });
        sortBy.check(R.id.btn_name);

        //-------------------------------------------------------
        waiting.setVisibility(View.VISIBLE);

        Task<QuerySnapshot> heroesMainTask = FirestoreFactory.getHeroes().get(Source.CACHE);
        Tasks.whenAllComplete(heroesMainTask)
                .addOnCompleteListener(task -> {
                    waiting.setVisibility(View.GONE);

                    HeroImages avatars = new HeroImages();
                    QuerySnapshot heroesMain = heroesMainTask.getResult();
                    list.clear();
                    for (QueryDocumentSnapshot document : heroesMain) {
                        HeroData data = document.toObject(HeroData.class);
                        if(data.type.equals("slark")){
                            System.out.println();
                        }
                        HeroModel model = new HeroModel(data.type, avatars.get(data.type));
                        model.update(data);
                        list.add(model);
                    }
                    adapter.notifyDataSetChanged();
                });

        //--------------------
        hero1CompareView = rootView.findViewById(R.id.iv_hero1_compare);
        hero2CompareView = rootView.findViewById(R.id.iv_hero2_compare);
        btnCompare = rootView.findViewById(R.id.btn_compare);
        btnCompare.setOnClickListener(view -> {
            if (list.size() < 2) {
                FancyToast.makeText(getActivity(), "Please add more heroes",
                        Toast.LENGTH_SHORT,
                        FancyToast.INFO,
                        false

                ).show();
            }
        });
        hero1CompareView.setOnClickListener(view -> {
            hero1Compare = null;
            updateComparatorView();
        });

        hero2CompareView.setOnClickListener(view -> {
            hero2Compare = null;
            updateComparatorView();
        });
        btnCompare.setOnClickListener(view -> {
            showCompareDialog();
        });


        return rootView;
    }

    private void updateComparatorView() {
        if (hero1Compare == null) {
            hero1CompareView.setImageResource(R.drawable.placeholder_hero);
        } else {
            hero1CompareView.setImageResource(hero1Compare.avatarRes);
        }
        hero1CompareView.invalidate();

        if (hero2Compare == null) {
            hero2CompareView.setImageResource(R.drawable.placeholder_hero);
        } else {
            hero2CompareView.setImageResource(hero2Compare.avatarRes);
        }
        hero2CompareView.invalidate();

        if (hero1Compare == null || hero2Compare == null) {
            btnCompare.setEnabled(false);
        } else {
            btnCompare.setEnabled(true);
        }
    }

    public void showCompareDialog() {
        HeroCompareDialog dialog = new HeroCompareDialog(getActivity());
        dialog.setModels(hero1Compare, hero2Compare);
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        GuideApplication.getAnalytics().setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());
    }

    private void onClickExpandable(View v) {
        expendableTier.toggle();
    }

    private void sortBy(int btnId) {
        switch (btnId) {
            case R.id.btn_name:
                Collections.sort(adapter.getList(), (m1, m2) -> m1.type.compareTo(m2.type));
                break;
            case R.id.btn_tier:
                Collections.sort(adapter.getList(), (m1, m2) -> {
                    if (m1.goldCost == m2.goldCost) {
                        return m1.type.compareTo(m2.type);
                    }
                    return m1.goldCost - m2.goldCost;
                });
                break;
            case R.id.btn_dps:
                Collections.sort(adapter.getList(), (m1, m2) -> m2.dps - m1.dps);
                break;
        }
    }
}
