package com.ultimateguides.dota.screens.main.guides;

public class GuideHeaderModel extends GuideModel {

    public GuideHeaderModel(String title) {
        super("", title);
    }

    @Override
    public boolean isHeader() {
        return true;
    }
}
