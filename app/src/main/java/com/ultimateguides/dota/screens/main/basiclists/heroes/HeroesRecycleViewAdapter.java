package com.ultimateguides.dota.screens.main.basiclists.heroes;


import android.content.Context;
import android.content.res.ColorStateList;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.others.Action1;

import java.util.ArrayList;
import java.util.List;

public class HeroesRecycleViewAdapter extends RecyclerView.Adapter<HeroesRecycleViewAdapter.ViewHolderItem>
        implements Filterable {

    List<HeroModel> data;
    private Action1<HeroModel> onItemClick;
    private Action1<HeroModel> onLongItemClick;
    private List<HeroModel> filteredData;

    public HeroesRecycleViewAdapter(List<HeroModel> data) {
        this.data = data;
        this.filteredData = data;
    }

    @Override
    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        FrameLayout v = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_hero_with_gold, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderItem holder, int position) {
        HeroModel dataItem = getItem(position);
        Log.d("HeroName", dataItem.name);
        holder.imageView.setImageResource(dataItem.avatarRes);
        Context c = holder.root.getContext();
        ColorStateList backColor = null;
        if (dataItem.draftTier == 5) {
            holder.root.setPadding(3, 3, 3, 3);
            backColor = ContextCompat.getColorStateList(c, R.color.tier_4);
        } else {
            backColor = ContextCompat.getColorStateList(c, R.color.palette5);
            holder.root.setPadding(0, 0, 0, 0);
        }
        holder.root.setBackgroundTintList(backColor);

        holder.tierView.setText("" + dataItem.goldCost);
        holder.nameView.setText("" + dataItem.name);
        holder.root.setOnClickListener(view -> onItemClick.call(dataItem));
        holder.root.setOnLongClickListener(view -> {
            onLongItemClick.call(dataItem);
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    private HeroModel getItem(int position) {
        return filteredData.get(position);
    }

    public void setOnItemClick(Action1<HeroModel> onItemClick) {
        this.onItemClick = onItemClick;
    }

    public void setOnLongItemClick(Action1<HeroModel> onLongItemClick) {
        this.onLongItemClick = onLongItemClick;
    }

    public List<HeroModel> getList() {
        return filteredData;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredData = data;
                } else {
                    List<HeroModel> filteredList = new ArrayList<>();
                    for (HeroModel row : data) {
                        if (row.name.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredData = (ArrayList<HeroModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        RoundedImageView imageView;
        TextView tierView;
        TextView nameView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            imageView = itemView.findViewById(R.id.imageView);
            tierView = itemView.findViewById(R.id.tier);
            nameView = itemView.findViewById(R.id.tv_name);
        }
    }
}
