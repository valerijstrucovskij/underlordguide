package com.ultimateguides.dota.screens.main.guides;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimateguides.dota.R;
import com.ultimateguides.dota.others.Action1;

import java.util.List;

public class LootLevelsRecycleViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<LootLevelModel> data;
    private Action1<Integer> onItemClick;
    private Resources resources;

    public LootLevelsRecycleViewAdapter(List<LootLevelModel> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_guide_loot_round, parent, false);
        resources = v.getContext().getResources();
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LootLevelModel dataItem = getItem(position);
        ViewHolderItem viewItem = ((ViewHolderItem) holder);
        viewItem.roundView.setText(dataItem.level);
        viewItem.nameView.setText(dataItem.name);
        viewItem.creepsView.setText(dataItem.creeps);
        viewItem.itemsTierView.setText(dataItem.tierDropRate);
        viewItem.image.setImageResource(dataItem.imageWave);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private LootLevelModel getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<Integer> onItemClick) {
        this.onItemClick = onItemClick;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView roundView;
        TextView nameView;
        TextView creepsView;
        TextView itemsTierView;
        ImageView image;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            roundView = itemView.findViewById(R.id.tv_round);
            nameView = itemView.findViewById(R.id.tv_name);
            creepsView = itemView.findViewById(R.id.tv_creeps);
            itemsTierView = itemView.findViewById(R.id.tv_item_tiers);
            image = itemView.findViewById(R.id.icon);
        }
    }
}
