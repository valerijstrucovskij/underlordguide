package com.ultimateguides.dota.screens.synergy.heroes;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Source;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.HeroImages;
import com.ultimateguides.dota.customviews.GridSpacingItemDecoration;
import com.ultimateguides.dota.customviews.LoadingView;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.model.HeroModelOLD;
import com.ultimateguides.dota.model.firebase.HeroData;
import com.ultimateguides.dota.model.firebase.HeroMainData;
import com.ultimateguides.dota.model.firebase.HeroSkillsData;
import com.ultimateguides.dota.screens.herodetails.HeroDetailsActivity;
import com.ultimateguides.dota.screens.synergy.events.SynergyClearEvent;
import com.ultimateguides.dota.screens.synergy.events.SynergySelectedEvent;
import com.ultimateguides.dota.screens.synergy.selector.SynergyPresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class SynergyHeroesFragment extends Fragment {
    SwitchCompat switchPerfect;
    public static final int COLUMN = 4;
    private final HashSet<String> filter;
    private LinearLayout nothingToShow;
    private List<HeroModel> list = new ArrayList<>();
    private List<HeroModel> filteredModelList = new ArrayList<>();
    private SynergyRecycleViewAdapter adapter = new SynergyRecycleViewAdapter(filteredModelList);
    private SynergyPresenter presenter = new SynergyPresenter();

    public SynergyHeroesFragment(HashSet<String> filter) {
        this.filter = filter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_synergy_heroes, container, false);
        nothingToShow = rootView.findViewById(R.id.nothing_to_show);

        switchPerfect = rootView.findViewById(R.id.switch_is_perfect);
        switchPerfect.setOnCheckedChangeListener((compoundButton, b) -> showFiltered(b));

        LoadingView waiting = rootView.findViewById(R.id.waiting);
        RecyclerView recyclerView = rootView.findViewById(R.id.rv_heroes);
        GridLayoutManager grid = new GridLayoutManager(getContext(), COLUMN);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(COLUMN, 10, false));
        recyclerView.setLayoutManager(grid);

        recyclerView.setAdapter(adapter);
        adapter.setOnItemClick(param -> {
            Intent i = new Intent(getContext(), HeroDetailsActivity.class);
            i.putExtra("model", filteredModelList.get(param));
            startActivity(i);
        });

        waiting.setVisibility(View.VISIBLE);


        Task<QuerySnapshot> heroesMainTask = GuideApplication.getFirestore()
                .collection("heroes_main")
                .get(Source.CACHE);

        Task<QuerySnapshot> heroesStatsTask = GuideApplication.getFirestore()
                .collection("heroes_stats")
                .get(Source.CACHE);

        Tasks.whenAllComplete(heroesMainTask, heroesStatsTask).addOnCompleteListener(task -> {
            HeroImages avatars = new HeroImages();
            waiting.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
//                    expandableAlliances.expand();

            QuerySnapshot heroesMain = heroesMainTask.getResult();
            QuerySnapshot heroesStats = heroesStatsTask.getResult();
            list.clear();
            for (QueryDocumentSnapshot document : heroesMain) {
                HeroData data = document.toObject(HeroData.class);
                HeroModel model = new HeroModel(data.type, 0);
                model.update(data);
//                model.avatarRes = avatars.get(data.type);
//                for (QueryDocumentSnapshot docStats : heroesStats) {
//                    if (docStats.get("type").equals(data.type)) {
//                        HeroSkillsData stats = docStats.toObject(HeroSkillsData.class);
//                        model.updateStats(stats);
//                    }
//                }
                list.add(model);
            }
            adapter.notifyDataSetChanged();
        });
//        presenter.requestHeroAsync()
//                .subscribe(result -> {
//                    waiting.setVisibility(View.GONE);
//
//
//                    list.clear();
//                    list.addAll(result);
//                    adapter.notifyDataSetChanged();
//                }, error -> {
//                    waiting.setVisibility(View.GONE);
//                    recyclerView.setVisibility(View.VISIBLE);
//
//                    error.printStackTrace();
//                    Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
//                    //TODO load from last
//                });
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SynergyClearEvent event) {
        onClickClearAll(event.v);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(SynergySelectedEvent event) {
        showFiltered(switchPerfect.isChecked());
    }

    private void onClickClearAll(View v) {
        filteredModelList.clear();
        adapter.notifyDataSetChanged();
    }

    private void showFiltered(boolean isPerfect) {
        filteredModelList.clear();
        if (isPerfect) {
            showFilteredPerfect();
        } else {
            showFilteredAll();
        }

        if (filteredModelList.size() == 0) {
            nothingToShow.setVisibility(View.VISIBLE);
        } else {
            nothingToShow.setVisibility(View.GONE);
        }
        adapter.notifyDataSetChanged();
    }

    private void showFilteredAll() {
        for (String item : filter) {
            item = unificate(item);
            for (HeroModel model : list) {
                if (unificate(model.alliance1.name()).equals(item)
                        || unificate(model.alliance2.name()).equals(item) |
                        unificate(model.alliance3.name()).equals(item)) {
                    if (!filteredModelList.contains(model))
                        filteredModelList.add(model);
                }
            }
        }
    }

    private void showFilteredPerfect() {
        for (HeroModel model : list) {
            int hasAlliances = 2;
            for (String item : filter) {
                item = unificate(item);

                if (unificate(model.alliance1.name()).equals(item)
                        || unificate(model.alliance2.name()).equals(item) |
                        unificate(model.alliance3.name()).equals(item)) {
                    hasAlliances--;
                }
            }
            if (hasAlliances <= 0) {
                filteredModelList.add(model);
            }
        }
    }

    private String unificate(String value) {
        return value.replace(" ", "").toLowerCase();
    }
}