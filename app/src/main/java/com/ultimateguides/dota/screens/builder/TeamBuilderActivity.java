package com.ultimateguides.dota.screens.builder;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.crashlytics.android.Crashlytics;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Source;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.FirestoreFactory;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.HeroTypeDescriptor;
import com.ultimateguides.dota.PreferenceField;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.Utils;
import com.ultimateguides.dota.binding.AlliancesImages;
import com.ultimateguides.dota.binding.HeroImages;
import com.ultimateguides.dota.customviews.AllianceView;
import com.ultimateguides.dota.database.AppDatabase;
import com.ultimateguides.dota.database.TeamEntity;
import com.ultimateguides.dota.database.TeamsDao;
import com.ultimateguides.dota.model.AllianceModel;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.model.HeroModelOLD;
import com.ultimateguides.dota.model.ItemModel;
import com.ultimateguides.dota.model.firebase.AllianceData;
import com.ultimateguides.dota.model.firebase.HeroData;
import com.ultimateguides.dota.model.firebase.ItemsData;
import com.ultimateguides.dota.screens.herodetails.HeroDetailsActivity;
import com.ultimateguides.dota.screens.main.basiclists.items.ItemsRecycleViewAdapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static androidx.recyclerview.widget.StaggeredGridLayoutManager.VERTICAL;

public class TeamBuilderActivity extends AppCompatActivity {

    private View waiting;
    private TeamEntity teamModel;
    private HeroImages imagesHeroes = new HeroImages();
    private AlliancesImages imagesAlliances = new AlliancesImages();
    private boolean isEdit = false;

    private List<HeroModel> heroList = new ArrayList<>();
    private List<HeroModel> selectedHeroList = new ArrayList<>();
    private List<AllianceModel> allianceList = new ArrayList<>(10);
    private HeroesToTeamsRecycleViewAdapter adapter = new HeroesToTeamsRecycleViewAdapter(selectedHeroList);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initExtras();
        updateLord();
        getSupportActionBar().setTitle("0/10");

        waiting = findViewById(R.id.waiting);
        waiting.setVisibility(View.VISIBLE);

        RecyclerView recycleViewHeroes = findViewById(R.id.rv_heroes);
        recycleViewHeroes.setAdapter(adapter);
        adapter.setOnItemClick(param -> {
            if (isEdit) {
                selectedHeroList.remove(param);
                teamModel.heroes.remove(param.type);
                adapter.notifyDataSetChanged();
                onTeamChanged();
            } else {
                Intent i = new Intent(this, HeroDetailsActivity.class);
                i.putExtra("model", param);
                startActivity(i);
            }

        });
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recycleViewHeroes);

        Task<QuerySnapshot> heroesMainTask = FirestoreFactory.getHeroes().get(Source.CACHE);
        Task<QuerySnapshot> alliancesTask = FirestoreFactory.getAlliances()
                .orderBy("type", Query.Direction.ASCENDING)
                .get(Source.CACHE);
        Tasks.whenAllComplete(heroesMainTask, alliancesTask)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        allianceList.clear();
                        for (QueryDocumentSnapshot document : alliancesTask.getResult()) {
                            AllianceData data = document.toObject(AllianceData.class);
                            AllianceModel model = new AllianceModel(data);
                            allianceList.add(model);
                        }

                        heroList.clear();
                        for (QueryDocumentSnapshot document : heroesMainTask.getResult()) {
                            HeroData data = document.toObject(HeroData.class);
                            HeroModel model = new HeroModel(data.type, 0);
                            model.update(data);
                            model.avatarRes = imagesHeroes.get(data.type);
                            heroList.add(model);
                        }

                        //------
                        for (String heroType : teamModel.heroes) {
                            selectedHeroList.add(findHeroOfType(heroType));
                            adapter.notifyDataSetChanged();
                            onTeamChanged();
                        }
                    } else {
                        Log.d("Firestore", "Update fails");
                    }
                    waiting.setVisibility(View.GONE);
                    GuideApplication.addADS(this, R.id.ads_banner);
                });

        //----------------------------------------------------------------------------
        findViewById(R.id.btn_edit).setOnClickListener(this::editTeam);
        findViewById(R.id.btn_save).setOnClickListener(this::saveTeam);
        findViewById(R.id.btn_add_hero).setOnClickListener(this::showAddNewHeroDialog);
        findViewById(R.id.btn_reccomend_hero).setOnClickListener(this::showAddNewHeroRecomendedDialog);
        findViewById(R.id.btn_share).setOnClickListener(this::onClickShare);
        findViewById(R.id.btn_upload).setOnClickListener(this::onClickUpload);
        findViewById(R.id.iv_lord).setOnClickListener(this::onClickSelectLord);
    }

    private void updateRoasterCount() {
        getSupportActionBar().setTitle(teamModel.heroes.size() + "/10");
    }

    private void setEditState(boolean isEdit) {
        this.isEdit = isEdit;
        if (isEdit) {
            findViewById(R.id.btn_edit).setVisibility(View.GONE);
            findViewById(R.id.btn_save).setVisibility(View.VISIBLE);
            findViewById(R.id.placeholder_buttons_edit).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.btn_edit).setVisibility(View.VISIBLE);
            findViewById(R.id.btn_save).setVisibility(View.GONE);
            findViewById(R.id.placeholder_buttons_edit).setVisibility(View.GONE);
        }
    }

    private void editTeam(View v) {
        setEditState(true);
    }

    @SuppressLint("CheckResult")
    private void saveTeam(View v) {
        View root = getLayoutInflater().inflate(R.layout.dialog_save_team, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        TextInputEditText name = root.findViewById(R.id.et_name);
        builder.setView(root);
        if (!TextUtils.isEmpty(teamModel.name)) {
            name.setText("" + teamModel.name);
        }
        builder.setNegativeButton("Cancel", null);

        builder.setPositiveButton("OK", (dialogInterface, i) -> {
            setEditState(false);
            teamModel.name = name.getText().toString();
            teamModel.modified = System.currentTimeMillis();
            TeamsDao dao = AppDatabase.getInstance().teamsDao();
            if (teamModel.id == 0) {
                Completable.fromCallable(() -> dao.addTeam(teamModel))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                () -> {
                                    FancyToast.makeText(TeamBuilderActivity.this, "Team " + teamModel.name + " added to you bookmarks 'My Teams'",
                                            Toast.LENGTH_LONG,
                                            FancyToast.SUCCESS,
                                            false
                                    ).show();
                                    Log.d("RoomWithRx", "onCreateView: ");
                                },
                                throwable -> throwable.printStackTrace()
                        );
            } else {
                Completable.fromCallable(() -> dao.updateTeam(teamModel))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                () -> {
                                    FancyToast.makeText(TeamBuilderActivity.this, "Team " + teamModel.name + " updated'",
                                            Toast.LENGTH_LONG,
                                            FancyToast.INFO,
                                            false
                                    ).show();
                                    Log.d("RoomWithRx", "onCreateView: ");
                                },
                                throwable -> throwable.printStackTrace()
                        );
            }
        });
        builder.show();
    }

    public String getDate() {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
        String dateString = format.format(new Date());
        return dateString;
    }

    private void showAddNewHeroDialog(View v) {
        BottomSheetDialog builder = new BottomSheetDialog(this);
        ViewGroup root = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_select_hero, null);
        HeroesToTeamsRecycleViewAdapter adapterDialog = new HeroesToTeamsRecycleViewAdapter(heroList);
        SearchView searchView = root.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapterDialog.getFilter().filter(newText);
                return false;
            }
        });
        RecyclerView heroesView = root.findViewById(R.id.rv_heroes);
        heroesView.setAdapter(adapterDialog);
        builder.setContentView(root);
        builder.show();
        adapterDialog.setOnItemClick(param -> {
            builder.dismiss();
            teamModel.heroes.add(param.type);
            selectedHeroList.add(param);
            adapter.notifyDataSetChanged();
            onTeamChanged();
        });
    }

    public void onClickShare(View v) {
        LinearLayout placeholder = findViewById(R.id.placeholder_screenshot_share);

        // save bitmap to cache directory
        try {

            File cachePath = new File(GuideApplication.getApp().getCacheDir(), "images");
            cachePath.mkdirs(); // don't forget to make the directory
            FileOutputStream stream = new FileOutputStream(cachePath + "/team_build.png"); // overwrites this image every time
            Utils.getBitmapFromView(placeholder).compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File imagePath = new File(GuideApplication.getApp().getCacheDir(), "images");
        File newFile = new File(imagePath, "team_build.png");
        Uri contentUri = FileProvider.getUriForFile(GuideApplication.getApp(), "com.ultimateguides.dota.fileprovider", newFile);

        if (contentUri != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, teamModel.name);
            shareIntent.putExtra(Intent.EXTRA_TEXT, teamModel.name + "\n*Made with Underlords Guide [Android]");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.setDataAndType(contentUri, getContentResolver().getType(contentUri));
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
            startActivity(Intent.createChooser(shareIntent, "Choose an app"));
        }
    }

    public void onClickUpload(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Share build with community");
        View rootView = getLayoutInflater().inflate(R.layout.dialog_share_team_with_editor, null, false);
        builder.setView(rootView);
        builder.setCancelable(false);
        builder.setPositiveButton("Send", (dialogInterface, i) -> {
            Map<String, Object> user = new HashMap<>();

            TextInputEditText usernameView = rootView.findViewById(R.id.et_username);
            TextInputEditText teamnameView = rootView.findViewById(R.id.et_team_name);
            TextInputEditText messageView = rootView.findViewById(R.id.et_message);
            Spinner langView = rootView.findViewById(R.id.spinner_lang);

            StringBuilder message = new StringBuilder();
            message.append("Message: " + messageView.getText().toString() + "\n");

            FirebaseDatabase database = FirebaseDatabase.getInstance();

            HashMap<String, Object> result = new HashMap<>();
            StringBuilder listString = new StringBuilder();
            for (String s : teamModel.heroes) {
                listString.append(s).append(";");
            }
            String username = usernameView.getText().toString();
            if (TextUtils.isEmpty(username)) {
                username = "anonymous";
            }
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String strDate = formatter.format(date);

            String name = teamnameView.getText().toString();
            String lang = langView.getSelectedItem().toString();
            result.put("username", username);
            result.put("name", name);
            result.put("heroes", listString.toString());
            result.put("notes", messageView.getText().toString());
            result.put("date", strDate);
            result.put("lord", teamModel.lordName);
            result.put("rating", "0");
            result.put("lang", lang);

            DatabaseReference table = database.getReference("community_builds");
            table.push().setValue(result);
            FancyToast.makeText(TeamBuilderActivity.this,
                    "Success! Thank you",
                    Toast.LENGTH_SHORT,
                    FancyToast.SUCCESS,
                    false
            ).show();
        });
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());
        builder.setIcon(R.drawable.ic_suggest);
        builder.show();
    }


    private void showAddNewHeroRecomendedDialog(View v) {
        if (teamModel.heroes.size() == 0) {
            FancyToast.makeText(this,
                    "Can't recommend anything to empty team",
                    Toast.LENGTH_SHORT,
                    FancyToast.WARNING,
                    false
            ).show();
            return;
        }

        ArrayList<HeroModel> recommendedList = new ArrayList<>();
        for (HeroModel model : heroList) {
            if (allianceBonuses.containsKey(model.alliance1) ||
                    allianceBonuses.containsKey(model.alliance2) ||
                    allianceBonuses.containsKey(model.alliance3)) {
                if (!teamModel.heroes.contains(model.type)) {
                    recommendedList.add(model);
                }
            }
        }

        Collections.sort(recommendedList, (m1, m2) -> {
            int count1 = 0;
            count1 += isHeroCompleteAlliance(m1.alliance1) ? 1 : 0;
            count1 += isHeroCompleteAlliance(m1.alliance2) ? 1 : 0;
            count1 += isHeroCompleteAlliance(m1.alliance3) ? 1 : 0;

            int count2 = 0;
            count2 += isHeroCompleteAlliance(m2.alliance1) ? 1 : 0;
            count2 += isHeroCompleteAlliance(m2.alliance2) ? 1 : 0;
            count2 += isHeroCompleteAlliance(m2.alliance3) ? 1 : 0;

            Log.d("BUILDER", "showAddNewHeroRecomendedDialog: "
                    + m1.name + " : " + count1 + "| "
                    + m2.name + " : " + count2
            );
            return count2 - count1;
        });

        BottomSheetDialog builder = new BottomSheetDialog(this);
        ViewGroup root = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_select_hero_recommend, null);
        HeroesRecommendRecycleViewAdapter adapterDialog = new HeroesRecommendRecycleViewAdapter(recommendedList, allianceList, allianceBonuses);
        RecyclerView heroesView = root.findViewById(R.id.rv_heroes);
        MaterialButton btnClose = root.findViewById(R.id.btn_close);
        heroesView.setAdapter(adapterDialog);
        builder.setContentView(root);
        builder.show();

        btnClose.setOnClickListener(view -> builder.dismiss());
        adapterDialog.setOnItemClick(param -> {
            builder.dismiss();
            teamModel.heroes.add(param.type);
            selectedHeroList.add(param);
            adapter.notifyDataSetChanged();
            onTeamChanged();
        });
    }

    public boolean isHeroCompleteAlliance(AllianceType allianceType) {
        if (allianceBonuses.containsKey(allianceType)) {
            AllianceModel allModel1 = findAllianceOfType(allianceType);
            int activeCount = allianceBonuses.get(allianceType) + 1;
            return activeCount % allModel1.unitsPerStage == 0 && activeCount != 0;
        }
        return false;
    }


    public void onTeamChanged() {
        int visiblePlaceholder = selectedHeroList.size() == 0 ? View.VISIBLE : View.GONE;
        findViewById(R.id.tv_empty_team).setVisibility(visiblePlaceholder);
        calculateBonuses();
        calculateBackgroundColor();
        calculateRecommendItems();
        updateRoasterCount();
    }

    private void initExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            teamModel = (TeamEntity) extras.getSerializable("teamEntity");
        }
        if (teamModel == null) {
            teamModel = new TeamEntity(null);
        }

        if (teamModel.heroes.size() == 0) {
            setEditState(true);
        }
    }

    private void calculateBonuses() {
        //delete duplicates
        Set<String> set = new HashSet<>(teamModel.heroes);
        ArrayList<String> heroes = new ArrayList<>(set);
        //----
        allianceBonuses.clear();
        for (AllianceModel alliance : allianceList) {
            AllianceType allianceType = alliance.type;
            for (String heroType : heroes) {
                HeroModel hero = findHeroOfType(heroType);
                if (hero.alliance1 == allianceType
                        || hero.alliance2 == allianceType
                        || hero.alliance3 == allianceType) {
                    if (allianceBonuses.containsKey(allianceType)) {
                        int count = allianceBonuses.get(alliance.type);
                        allianceBonuses.put(allianceType, count + 1);
                    } else {
                        allianceBonuses.put(allianceType, 1);
                    }
                }
            }
        }

//        trim with zero effect
//        for (AllianceModel alliance : allianceList) {
//            AllianceType allianceType = alliance.type;
//            int step = alliance.unitsPerStage;
//            if (allianceBonuses.containsKey(allianceType)) {
//                Integer bonusCount = allianceBonuses.get(allianceType);
//                if (bonusCount < step) {
//                    allianceBonuses.remove(allianceType);
//                }
//            }
//        }

        LinearLayout placeholderBonuses = findViewById(R.id.placeholder_bonuses);
        FlexboxLayout placeholderBonusesInactive = findViewById(R.id.placeholder_bonuses_inactive);
        placeholderBonuses.removeAllViews();
        placeholderBonusesInactive.removeAllViews();
        for (AllianceType type : allianceBonuses.keySet()) {
            AllianceModel allianceItem = findAllianceOfType(type);
            int maxUnits = allianceItem.unitsPerStage * allianceItem.maxStages;
            Integer countUnits = allianceBonuses.get(type) > maxUnits ? maxUnits : allianceBonuses.get(type);
            int unitsPerStage = allianceItem.unitsPerStage;
            int completedStages = countUnits / unitsPerStage;

            AllianceView view = new AllianceView(TeamBuilderActivity.this);
            view.setAllianceIcon(imagesAlliances.get(type));
            view.setStages(allianceItem.maxStages);
            view.setUnitsPerStage(allianceItem.unitsPerStage);
            view.setActiveCount(countUnits);
            view.setDescribe(completedStages == 0 ? "" : allianceItem.stages.get(completedStages - 1));
            view.updateView();
            if (completedStages > 0) {
                placeholderBonuses.addView(view);
            } else {
                placeholderBonusesInactive.addView(view);
            }
        }

        //------------------------
        int priceMin = 0;
        int priceMax = 0;
        for (String heroType : teamModel.heroes) {
            long firestoreVersion = GuideApplication.getPrefs().getLong(PreferenceField.FIRESTORE_DB_VERSION, 0);
            Crashlytics.setInt("teambuilder_heroes_size", heroList.size());
            Crashlytics.setString("teambuilder_hero", heroType);
            Crashlytics.setLong("firestore_version", firestoreVersion);
            HeroModel hero = findHeroOfType(heroType);
//            Crashlytics.getInstance().crash();
//            priceMin += hero.goldCost;
//            priceMax += (hero.goldCost * 3);
        }

//        TextView priceMinView = findViewById(R.id.tv_price_min);
//        TextView priceMaxView = findViewById(R.id.tv_price_max);
//        priceMinView.setText(String.valueOf(priceMin));
//        priceMaxView.setText(String.valueOf(priceMax));
    }

    public void calculateBackgroundColor() {
        AllianceType type = null;
        int maxUnitsAlliance = 0;
        for (AllianceType item : allianceBonuses.keySet()) {
            int count = allianceBonuses.get(item);
            if (count > maxUnitsAlliance) {
                maxUnitsAlliance = count;
                type = item;
            }
        }
        try {
            Bitmap icon = BitmapFactory.decodeResource(this.getResources(), imagesAlliances.get(type));
            teamModel.backgroundColor = Palette.from(icon).generate().getDominantColor(getResources().getColor(R.color.listitem_background));
        } catch (Exception ignored) {

        }
    }

    HashMap<AllianceType, Integer> allianceBonuses = new HashMap<>();

    private void openHeroDetails(HeroModelOLD model) {
        Intent i = new Intent(this, HeroDetailsActivity.class);
        i.putExtra("teamModel", model);
        startActivity(i);
    }

    private HeroModel findHeroOfType(@HeroTypeDescriptor.HeroType String type) {
        if (heroList.size() > 0) {
            for (HeroModel model : heroList) {
                if (model.type.equals(type)) {
                    return model;
                }
            }
        }
        return null;
    }

    private AllianceModel findAllianceOfType(AllianceType type) {
        if (allianceList.size() > 0) {
            for (AllianceModel model : allianceList) {
                if (model.type == type) {
                    return model;
                }
            }
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickSelectLord(View v) {
        if (!isEdit) {
            FancyToast.makeText(this, "Please enter edit mode",
                    Toast.LENGTH_SHORT,
                    FancyToast.INFO,
                    false
            ).show();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Pick a lord which best fits the build");
        String lordSaved = teamModel.lordName;
        int selected = -1;
        if (!TextUtils.isEmpty(lordSaved)) {
            switch (lordSaved) {
                case "annesix":
                    selected = 0;
                    break;
                case "hobgen":
                    selected = 1;
                    break;
            }
        }
        builder.setSingleChoiceItems(new String[]{"Annesix", "Hobgen"}, selected, (dialogInterface, i) -> {
            switch (i) {
                case 0:
                    teamModel.lordName = "annesix";
                    break;
                case 1:
                    teamModel.lordName = "hobgen";
                    break;
            }
            updateLord();
            dialogInterface.dismiss();
        });
        builder.show();
    }

    private void updateLord() {
        String lordId = teamModel.lordName;
        if (TextUtils.isEmpty(lordId)) return;

        String lordName = "";
        int lordIcon = R.drawable.placeholder_hero;
        TextView lordNameView = findViewById(R.id.tv_lord_name);
        ImageView lordIconView = findViewById(R.id.iv_lord);
        switch (lordId) {
            case "annesix":
                lordName = "Annesix";
                lordIcon = R.drawable.lord_annesix;
                break;
            case "hobgen":
                lordName = "Hobgen";
                lordIcon = R.drawable.lord_hobgen;
                break;
        }

        lordNameView.setText(lordName);
        lordIconView.setImageResource(lordIcon);
    }

    private List<ItemModel> list = new ArrayList<>();
    private ItemsRecycleViewAdapter adapterItems = new ItemsRecycleViewAdapter(list);

    private void calculateRecommendItems() {
        RecyclerView recycleView = findViewById(R.id.recycleView);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1, VERTICAL);
        recycleView.setLayoutManager(layoutManager);
        recycleView.setAdapter(adapterItems);

        FirestoreFactory.getItems()
                .orderBy("tier", Query.Direction.ASCENDING)
                .get(Source.CACHE)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        list.clear();
                        List<ItemModel> fullList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            ItemsData data = document.toObject(ItemsData.class);
                            if (data.id == 10563 //barricade
                                    || data.id == 10558 //TODO next update; assasins
                                    || data.id == 10548 //TODO next update: beasts
                                    || data.id == 10525 //extra damage
                                    || data.id == 10526 //gold on defeat
                                    || data.id == 10527 //free first roll
                                    || data.id == 10565 //target buddy
                                    || data.id == 10562 //healing ward
                                    || data.id == 10560 //recruiter
                                    || data.id == 10564 //tombstone
                                    || data.id == 10008 //outdated
                                    || data.id == 10531 //TODO next update; bonus_damage_low_life
                                    || data.id == 10548) continue;
//                            if (data.tier != tier) {
//                                list.add(new ItemModel(data.tier));
//                                tier = data.tier;
//
//                            }
                            ItemModel model = new ItemModel(data);
                            fullList.add(model);
                        }

                        for (String heroId : teamModel.heroes) {
                            HeroModel model = findHeroOfType(heroId);
                            if (model == null) continue;

                            if (model.items != null) {
                                for (String itemId : model.items) {
                                    for (ItemModel itemModel : fullList) {
                                        if (itemId.equals(itemModel.icon)) {
                                            if (!list.contains(itemModel)) {
                                                list.add(itemModel);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        adapterItems.notifyDataSetChanged();
                    } else {
                        Log.d("Firestore", "Update fails");
                    }
                });
    }
}
