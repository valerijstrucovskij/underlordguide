package com.ultimateguides.dota.screens.synergy.selector;


import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.gson.Gson;
import com.ultimateguides.dota.Config;
import com.ultimateguides.dota.HeroTypeOLD;
import com.ultimateguides.dota.model.HeroModelOLD;
import com.ultimateguides.dota.model.spreadsheet.ResponseAddionsModel;
import com.ultimateguides.dota.model.spreadsheet.ResponseHeroesModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SynergyPresenter {

    private final String spreadsheetId;
    private final Sheets sheetsService;

    public SynergyPresenter() {
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory factory = JacksonFactory.getDefaultInstance();
        sheetsService = new Sheets.Builder(transport, factory, null)
                .setApplicationName("UnderlordsGuide")
                .build();
        spreadsheetId = Config.spreadsheet_id;
    }


    public Single<ArrayList<HeroModelOLD>> requestHeroAsync() {
        return Single.fromCallable(() -> requestHeroes())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
//                .filter(list -> list != null);
    }

    public ArrayList<HeroModelOLD> requestHeroes() {
        ValueRange resultStats = null;
        ValueRange readResultAddtions = null;
        try {
            resultStats = sheetsService.spreadsheets().values()
                    .get(Config.spreadsheet_additions_id, "HeroesStats!A5:AB66")
                    .setKey(Config.google_api_key)
                    .execute();


            readResultAddtions = sheetsService.spreadsheets().values()
                    .get(Config.spreadsheet_additions_id, "HeroesSynergySkills!A2:G61")
                    .setKey(Config.google_api_key)
                    .execute();

            String factory = resultStats.toPrettyString();
            String factory2 = readResultAddtions.toPrettyString();
            ResponseHeroesModel response = new Gson().fromJson(factory, ResponseHeroesModel.class);
            ResponseAddionsModel response2 = new Gson().fromJson(factory2, ResponseAddionsModel.class);

            //AvatarImages avatars = new AvatarImages();
            ArrayList<HeroModelOLD> heroes = new ArrayList<>();

            List<List<String>> heroesToRemove = new ArrayList<>();
            for (int i = 0; i < response.stats.size(); i++) {
                if (response.stats.get(i).get(0).contains("..")) {
                    List<String> rawHero = response.stats.get(i);
                    heroesToRemove.add(rawHero);
                }
            }
            response.stats.removeAll(heroesToRemove);

            for (int i = 0; i < response.stats.size(); i++) {
                List<String> raw = response.stats.get(i);
                String name = raw.get(0).replaceAll(" ", "")
                        .replaceAll("'", "");

                try {
                    HeroTypeOLD type = HeroTypeOLD.valueOf(name);
                    //HeroModelOLD model = new HeroModelOLD(type, avatars.get(type));
//                    model.stats = new HeroStats(raw);
//                    model.addAddions(response2.values.get(i));
//                    heroes.add(model);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return heroes;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
