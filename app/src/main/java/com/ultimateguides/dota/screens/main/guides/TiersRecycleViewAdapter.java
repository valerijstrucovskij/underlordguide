package com.ultimateguides.dota.screens.main.guides;


import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.TierModel;
import com.ultimateguides.dota.others.Action1;

import java.util.List;

public class TiersRecycleViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<TierModel> data;
    private Action1<Integer> onItemClick;
    private Resources resources;

    public TiersRecycleViewAdapter(List<TierModel> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_draw_tier, parent, false);
        resources = v.getContext().getResources();
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TierModel dataItem = getItem(position);
        ViewHolderItem viewItem = ((ViewHolderItem) holder);

        viewItem.levelView.setText(dataItem.level);
        viewItem.tier1View.setText(dataItem.tier1);
        viewItem.tier1View.setTextColor(resources.getColor(R.color.tier_1));
        viewItem.tier2View.setText(dataItem.tier2);
        viewItem.tier2View.setTextColor(resources.getColor(R.color.tier_2));
        viewItem.tier3View.setText(dataItem.tier3);
        viewItem.tier3View.setTextColor(resources.getColor(R.color.tier_3));
        viewItem.tier4View.setText(dataItem.tier4);
        viewItem.tier4View.setTextColor(resources.getColor(R.color.tier_4));
        viewItem.tier5View.setText(dataItem.tier5);
        viewItem.tier5View.setTextColor(resources.getColor(R.color.tier_5));
        viewItem.setBolt(position == 0);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private TierModel getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<Integer> onItemClick) {
        this.onItemClick = onItemClick;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView levelView;
        TextView tier1View;
        TextView tier2View;
        TextView tier3View;
        TextView tier4View;
        TextView tier5View;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            levelView = itemView.findViewById(R.id.tv_level);
            tier1View = itemView.findViewById(R.id.tv_tier_1);
            tier2View = itemView.findViewById(R.id.tv_tier_2);
            tier3View = itemView.findViewById(R.id.tv_tier_3);
            tier4View = itemView.findViewById(R.id.tv_tier_4);
            tier5View = itemView.findViewById(R.id.tv_tier_5);
        }

        public void setBolt(boolean isBolt) {
            if (isBolt) {
                levelView.setTypeface(null, Typeface.BOLD);
                tier1View.setTypeface(null, Typeface.BOLD);
                tier2View.setTypeface(null, Typeface.BOLD);
                tier3View.setTypeface(null, Typeface.BOLD);
                tier4View.setTypeface(null, Typeface.BOLD);
                tier5View.setTypeface(null, Typeface.BOLD);
            } else {
                levelView.setTypeface(null, Typeface.NORMAL);
                tier1View.setTypeface(null, Typeface.NORMAL);
                tier2View.setTypeface(null, Typeface.NORMAL);
                tier3View.setTypeface(null, Typeface.NORMAL);
                tier4View.setTypeface(null, Typeface.NORMAL);
                tier5View.setTypeface(null, Typeface.NORMAL);
            }
        }
    }
}
