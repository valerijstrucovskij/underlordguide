package com.ultimateguides.dota.screens.main.basiclists.alliances;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Source;
import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.FirestoreFactory;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.AlliancesImages;
import com.ultimateguides.dota.binding.HeroImages;
import com.ultimateguides.dota.model.AllianceModel;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.model.firebase.AllianceData;
import com.ultimateguides.dota.model.firebase.HeroData;
import com.ultimateguides.dota.model.firebase.HeroMainData;
import com.ultimateguides.dota.model.firebase.HeroSkillsData;
import com.ultimateguides.dota.screens.builder.HeroesToTeamsRecycleViewAdapter;
import com.ultimateguides.dota.screens.herodetails.HeroDetailsActivity;

import java.util.ArrayList;
import java.util.List;

public class AlliancesFragment extends Fragment {

    //    AlliancesListPresenter presenter = new AlliancesListPresenter("Classes!A2:F24");
    List<AllianceModel> allianceList = new ArrayList<>();
    AlliancesRecycleViewAdapter adapter = new AlliancesRecycleViewAdapter(allianceList);
    private ViewGroup rootView;
    private List<HeroModel> heroList = new ArrayList<>();
    private HeroImages imagesHeroes = new HeroImages();
    private AlliancesImages imagesAlliances = new AlliancesImages();

    public AlliancesFragment() {
    }

    public static AlliancesFragment newInstance() {
        AlliancesFragment fragment = new AlliancesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.activity_alliances_list, container, false);
        LinearLayout waiting = rootView.findViewById(R.id.waiting);
        RecyclerView recycleView = rootView.findViewById(R.id.rv_races);
        recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycleView.setAdapter(adapter);
        adapter.setOnItemClick(this::showAddNewHeroDialog);

        SearchView searchView = rootView.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String text = newText;
                adapter.getFilter().filter(text);
                return false;
            }
        });

        waiting.setVisibility(View.VISIBLE);
        FirestoreFactory.getAlliances()
                .orderBy("type", Query.Direction.ASCENDING)
                .get(Source.CACHE)
                .addOnCompleteListener(task -> {
                    waiting.setVisibility(View.GONE);
                    if (task.isSuccessful()) {
                        allianceList.clear();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            AllianceData data = document.toObject(AllianceData.class);
                            AllianceModel model = new AllianceModel(data);
                            allianceList.add(model);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        Log.d("Firestore", "Update fails");
                    }
                    waiting.setVisibility(View.GONE);
                });

        Task<QuerySnapshot> heroesMainTask = FirestoreFactory.getHeroes().get(Source.CACHE);
        Task<QuerySnapshot> alliancesTask = FirestoreFactory.getAlliances()
                .orderBy("type", Query.Direction.ASCENDING)
                .get(Source.CACHE);
        Tasks.whenAllComplete(heroesMainTask, alliancesTask)
                .addOnCompleteListener(task -> {
                    if (alliancesTask.isSuccessful()) {
                        allianceList.clear();
                        for (QueryDocumentSnapshot document : alliancesTask.getResult()) {
                            AllianceData data = document.toObject(AllianceData.class);
                            AllianceModel model = new AllianceModel(data);
                            allianceList.add(model);
                        }

                        QuerySnapshot heroes = heroesMainTask.getResult();
                        heroList.clear();
                        for (QueryDocumentSnapshot document : heroesMainTask.getResult()) {
                            HeroData data = document.toObject(HeroData.class);
                            HeroModel model = new HeroModel(data.type, 0);
                            model.update(data);
                            model.avatarRes = imagesHeroes.get(data.type);

//                            for (QueryDocumentSnapshot docStats : heroesStats) {
//                                if (docStats.get("type").equals(data.type)) {
//                                    HeroSkillsData stats = docStats.toObject(HeroSkillsData.class);
//                                    model.updateStats(stats);
//                                }
//                            }
                            heroList.add(model);
                        }
                    } else {
                        Log.d("Firestore", "Update fails");
                    }
                    waiting.setVisibility(View.GONE);
                });

        return rootView;
    }

    private void showAddNewHeroDialog(AllianceType value) {
        Log.d("Alliances", "showAddNewHeroDialog: " + value);
        BottomSheetDialog builder = new BottomSheetDialog(getActivity());

        List<HeroModel> listToShowinDialog = new ArrayList<>();
        for (HeroModel heroModel : heroList) {
            if (heroModel.alliance1.equals(value)
                    || heroModel.alliance2.equals(value)
                    || heroModel.alliance3.equals(value)) {
                listToShowinDialog.add(heroModel);
            }
        }

        ViewGroup root = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_select_hero, null);
        HeroesToTeamsRecycleViewAdapter adapterDialog = new HeroesToTeamsRecycleViewAdapter(listToShowinDialog);
        adapterDialog.setOnItemClick(model -> {
            Intent i = new Intent(getActivity(), HeroDetailsActivity.class);
            i.putExtra("model", model);
            startActivity(i);
        });
        SearchView searchView = root.findViewById(R.id.searchView);
        searchView.setVisibility(View.GONE);
        RecyclerView heroesView = root.findViewById(R.id.rv_heroes);
        heroesView.setAdapter(adapterDialog);
        builder.setContentView(root);
        builder.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        GuideApplication.getAnalytics().setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());
    }
}
