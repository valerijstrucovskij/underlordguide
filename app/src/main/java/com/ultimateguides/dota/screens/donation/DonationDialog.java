package com.ultimateguides.dota.screens.donation;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.google.android.material.button.MaterialButton;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.PreferenceField;
import com.ultimateguides.dota.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DonationDialog extends AlertDialog.Builder {
    private final Activity activity;
    private ViewGroup root;
    private AlertDialog dialog;
    private BillingClient billingClient;

    private Map<String, SkuDetails> mSkuDetailsMap = new HashMap<>();
    private String mSkuId = "small_coffee";
    private View placeholderButton;

    public DonationDialog(Activity activity) {
        super(activity, android.R.style.Theme_Material_Light_DialogWhenLarge_DarkActionBar);
        this.activity = activity;
        createView();
        createBilling();
    }

    @Override
    public AlertDialog show() {
        dialog = super.show();
        return dialog;
    }

    private void createView() {
        LayoutInflater li = LayoutInflater.from(getContext());
        root = (ViewGroup) li.inflate(R.layout.dialog_buy_coffee, null);
        MaterialButton btnClose = root.findViewById(R.id.btn_not_now);
        placeholderButton = root.findViewById(R.id.placeholder_price);
        placeholderButton.setVisibility(View.GONE);
        btnClose.setOnClickListener(view -> dialog.dismiss());

        root.findViewById(R.id.btn_buy_1).setOnClickListener(this::onClickPay);
        root.findViewById(R.id.btn_buy_2).setOnClickListener(this::onClickPay);
        root.findViewById(R.id.btn_buy_3).setOnClickListener(this::onClickPay);

        TextView usernameView = root.findViewById(R.id.tv_username);
        String username = GuideApplication.getPrefs().getString(PreferenceField.USERNAME, "");
        usernameView.setText("" + username);

        setView(root);
    }


    private void onClickPay(View v) {
        if (v.getId() == R.id.btn_buy_1) {
            launchBilling(mSkuId);
        }
//        if (v.getId() == R.id.btn_buy_2) {
//            getCoffeeSKU("medium_coffee");
//        }
//        if (v.getId() == R.id.btn_buy_3) {
//            getCoffeeSKU("big_coffee");
//        }
    }

    private void payComplete() {
        GuideApplication.setDonate(true);
        GuideApplication.getPrefs().edit()
                .putBoolean(PreferenceField.DID_DONATION, true)
                .apply();
        //TODO Save to prefs
        //                    for (SkuDetails skuDetails : skuDetailsList) {
//                        String sku = skuDetails.getSku();
//                        String price = skuDetails.getPrice();
//                        if ("premium_upgrade".equals(sku)) {
//                            premiumUpgradePrice = price;
//                        } else if ("gas".equals(sku)) {
//                            gasPrice = price;
//                        }
//                    }
    }


    private void createBilling() {
        billingClient = BillingClient.newBuilder(activity)
                .enablePendingPurchases()
                .setListener((billingResult, purchases) -> {
                    payComplete();
                })
                .build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    querySkuDetails();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });
    }

    private void querySkuDetails() {
        SkuDetailsParams.Builder skuDetailsParamsBuilder = SkuDetailsParams.newBuilder();
        List<String> skuList = new ArrayList<>();
        skuList.add(mSkuId);
        skuDetailsParamsBuilder.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        billingClient.querySkuDetailsAsync(skuDetailsParamsBuilder.build(), (billingResult, skuDetailsList) -> {
            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                for (SkuDetails skuDetails : skuDetailsList) {
                    mSkuDetailsMap.put(skuDetails.getSku(), skuDetails);

                }
                placeholderButton.setVisibility(View.VISIBLE);
            }
        });
    }

    public void launchBilling(String skuId) {
        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(mSkuDetailsMap.get(skuId))
                .build();
        billingClient.launchBillingFlow(activity, billingFlowParams);
    }
}
