package com.ultimateguides.dota.screens.synergy.events;

import android.view.View;

public class SynergySelectedEvent {

    public final View v;

    public SynergySelectedEvent(View v) {
        this.v = v;
    }
}
