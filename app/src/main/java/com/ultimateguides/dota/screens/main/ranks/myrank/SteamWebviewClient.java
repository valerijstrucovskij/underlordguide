package com.ultimateguides.dota.screens.main.ranks.myrank;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.PreferenceField;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.steamApi.VanityProfileModel;

import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SteamWebviewClient extends WebViewClient {

    private final String startUrl = "https://steamcommunity.com/profiles/PROFILE_ID/gcpd/1046930?category=Games&tab=Matches";
    private final ProgressBar progressBar;
    private final ViewGroup waiting;
    private StringBuilder stepsLog = new StringBuilder();
    private StringBuilder stepsLogRes = new StringBuilder();

    private Activity activity;

    public SteamWebviewClient(ProgressBar progressBar, ViewGroup waiting) {
        this.progressBar = progressBar;
        this.waiting = waiting;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if (url.toLowerCase().endsWith("/login")) {
            waiting.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
        } else {
            waiting.setVisibility(View.VISIBLE);
            view.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        stepsLog.append("page finished = " + url + "\n");
        Log.d("SteamWebviewClient", "onPageFinished: " + url);
        if (url.toLowerCase().endsWith("/login")) {
            progressBar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(1);
            if (!url.toLowerCase().contains("category")) {
                if (url.toLowerCase().contains("/id/")) {
                    String userId = url.split("/id/")[1];
                    Call<VanityProfileModel> call = GuideApplication.getApi().GetUserId(userId, "572B3C48605A2252A5DA1F2FFC19E7E0");
                    call.enqueue(new Callback<VanityProfileModel>() {
                        @Override
                        public void onResponse(Call<VanityProfileModel> call, Response<VanityProfileModel> response) {
//                            stepsLog.append("page finished = " + response.body().response.toString() + "\n");
                            if (response.code() == 200 && response.isSuccessful()) {
                                String steamId = response.body().response.steamid;
                                GuideApplication.getPrefs().edit().putString(PreferenceField.STEAM_ID, steamId).apply();
                                view.loadUrl(startUrl.replace("PROFILE_ID", steamId));
                            }
                        }

                        @Override
                        public void onFailure(Call<VanityProfileModel> call, Throwable throwable) {

                        }
                    });
                } else if (url.toLowerCase().contains("profiles") && !url.toLowerCase().contains("gcpd")) {
                    showDialog(url);
                    url = url.replace("/goto", "");
                    String userId = url.split("profiles/")[1];
                    Log.d("SteamWebviewClient", "User id: " + userId);
                    GuideApplication.getPrefs().edit().putString(PreferenceField.STEAM_ID, userId).apply();
                    String steamId = GuideApplication.getPrefs().getString(PreferenceField.STEAM_ID, "");
                    view.loadUrl(startUrl.replace("PROFILE_ID", steamId));
                }
            } else if (url.toLowerCase().contains("matches")) {
                progressBar.setProgress(2);
                executeJScript(view, loadResourceInject(R.raw.inject_matches));
            } else if (url.toLowerCase().endsWith("GameAccountClient".toLowerCase())) {
                progressBar.setProgress(3);
                executeJScript(view, loadResourceInject(R.raw.inject_mmr));
            } else if (url.toLowerCase().endsWith("EventClaim".toLowerCase())) {
                progressBar.setVisibility(View.GONE);
                executeJScript(view, loadResourceInject(R.raw.inject_seasonpass_items));
            }
        }
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    private void showDialog(String message) {
//        MaterialAlertDialogBuilder b = new MaterialAlertDialogBuilder(activity);
//        b.setMessage(message);
//        b.show();
    }

    @Override
    public void onLoadResource(WebView view, String url) {
        super.onLoadResource(view, url);
        stepsLogRes.append("Resources = " + url + "\n");
        if (url.equals("https://store.steampowered.com/login/transfer")) {
            Log.d("SteamWebviewClient", "Redirect");
            view.loadUrl("https://steamcommunity.com/my/goto");
        }
    }

    private String loadResourceInject(int resId) {
        String data = null;
        try {
            Resources res = GuideApplication.getApp().getResources();
            InputStream in_s = res.openRawResource(resId);
            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            data = new String(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    private void executeJScript(WebView webView, String script) {
        webView.evaluateJavascript(script, s -> Log.d("TAG", "onReceiveValue: " + s));
    }

    public void clearLogs() {
        stepsLog.setLength(0);
        stepsLogRes.setLength(0);
    }

    public CharSequence getLogs() {
        return stepsLog.toString();
    }
}
