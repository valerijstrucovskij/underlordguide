package com.ultimateguides.dota.screens.builder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.AlliancesImages;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.others.Action1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HeroesToTeamsRecycleViewAdapter extends RecyclerView.Adapter<HeroesToTeamsRecycleViewAdapter.ViewHolderItem>
        implements Filterable, ItemTouchHelperAdapter  {

    List<HeroModel> data;
    private Action1<HeroModel> onItemClick;
    private List<HeroModel> filteredData;
    AlliancesImages images = new AlliancesImages();

    public HeroesToTeamsRecycleViewAdapter(List<HeroModel> data) {
        this.data = data;
        this.filteredData = data;
    }

    @Override
    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        FrameLayout v = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_hero_full, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderItem holder, int position) {
        HeroModel dataItem = getItem(position);
        holder.imageView.setImageResource(dataItem.avatarRes);
        holder.tierView.setText("" + dataItem.goldCost);
//        ((ViewHolderItem) holder).tierView.setVisibility(View.INVISIBLE);
        holder.nameView.setText("" + dataItem.name);
        holder.alliance1.setImageResource(images.get(dataItem.alliance1));
        holder.alliance2.setImageResource(images.get(dataItem.alliance2));
        holder.alliance3.setImageResource(images.get(dataItem.alliance3));
        if (dataItem.draftTier == 5) {
            holder.nameView.setBackgroundResource(R.color.ace);
        } else {
            holder.nameView.setBackgroundResource(R.color.dialogBackground);
        }


        holder.root.setOnClickListener(view -> onItemClick.call(dataItem));
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    private HeroModel getItem(int position) {
        return filteredData.get(position);
    }

    public void setOnItemClick(Action1<HeroModel> onItemClick) {
        this.onItemClick = onItemClick;
    }

    public List<HeroModel> getList() {
        return filteredData;
    }

    @Override
    public void onItemDismiss(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(data, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(data, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
//        return true;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredData = data;
                } else {
                    List<HeroModel> filteredList = new ArrayList<>();
                    for (HeroModel row : data) {
                        if (row.name.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredData = (ArrayList<HeroModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        RoundedImageView imageView;
        ImageView alliance1;
        ImageView alliance2;
        ImageView alliance3;
        TextView tierView;
        TextView nameView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            imageView = itemView.findViewById(R.id.imageView);
            tierView = itemView.findViewById(R.id.tier);
            nameView = itemView.findViewById(R.id.tv_name);
            alliance1 = itemView.findViewById(R.id.iv_alliance_1);
            alliance2 = itemView.findViewById(R.id.iv_alliance_2);
            alliance3 = itemView.findViewById(R.id.iv_alliance_3);
        }
    }
}
