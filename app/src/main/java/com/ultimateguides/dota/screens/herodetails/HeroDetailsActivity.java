package com.ultimateguides.dota.screens.herodetails;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.makeramen.roundedimageview.RoundedImageView;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.AlliancesImages;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.model.HeroModelOLD;
import com.ultimateguides.dota.model.SkillStage;

import java.util.ArrayList;
import java.util.List;

import static com.ultimateguides.dota.FirebaseAnalyticsParams.FAVORITE_HERO;
import static com.ultimateguides.dota.FirebaseAnalyticsParams.FAVORITE_TEAM;

public class HeroDetailsActivity extends AppCompatActivity {

    private HeroModel model;
    private boolean isTextViewClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initExtras();
        initView(model);
    }

    private void initExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            model = (HeroModel) extras.getSerializable("model");
            Bundle bundle = new Bundle();
            bundle.putString("hero_name", model.name);
            GuideApplication.getAnalytics().logEvent(FAVORITE_HERO, bundle);
        }
    }

    private void initView(HeroModel model) {
        if (model == null) {
            finish();
            return;
        }
        RoundedImageView avatarView = findViewById(R.id.iv_avatar);
        ImageView race1View = findViewById(R.id.iv_race_1);
        ImageView race2View = findViewById(R.id.iv_race_2);
        ImageView race3View = findViewById(R.id.iv_race_3);
        TextView aceView = findViewById(R.id.tv_ace);
        TextView nameView = findViewById(R.id.tv_name);
        TextView priceView = findViewById(R.id.tv_price);

        nameView.setText(String.valueOf(model.name));
        priceView.setText(String.valueOf(model.goldCost));
        if (model.goldCost == 5) {
            aceView.setVisibility(View.VISIBLE);
        } else {
            aceView.setVisibility(View.GONE);
        }
        avatarView.setImageResource(model.avatarRes);
        AlliancesImages images = new AlliancesImages();
        race1View.setImageResource(images.get(model.alliance1));
        race2View.setImageResource(images.get(model.alliance2));
        race3View.setImageResource(images.get(model.alliance3));

        ViewPager viewPager = findViewById(R.id.viewPager);
        List<String> listDate = new ArrayList<>();
        listDate.add("Stats");
        listDate.add("Skills");
        listDate.add("Items");
        HeroDetailsViewPagerAdapter adapter = new HeroDetailsViewPagerAdapter(this, listDate, model);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
//        tabLayout.getTabAt(0).setIcon(R.drawable.ic_synergy);
//        tabLayout.getTabAt(1).setIcon(R.drawable.ic_synergy_2);
        GuideApplication.addADS(this, R.id.ads_banner);
    }

    private void toggleDescription(TextView t1, boolean newState) {
        if (isTextViewClicked) {
            t1.setMaxLines(7);
            isTextViewClicked = false;
        } else {
            t1.setMaxLines(Integer.MAX_VALUE);
            isTextViewClicked = true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
