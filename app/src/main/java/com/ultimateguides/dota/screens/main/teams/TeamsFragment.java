package com.ultimateguides.dota.screens.main.teams;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.screens.SimpleViewPagerAdapter;
import com.ultimateguides.dota.screens.main.teams.community.CommunityTeamsFragment;
import com.ultimateguides.dota.screens.main.teams.myteams.MyTeamsRecycleViewFragment;
import com.ultimateguides.dota.screens.main.teams.editorsteams.EditorsTeamsFragment;

import java.util.ArrayList;

public class TeamsFragment extends Fragment {

//    String[] titles = new String[]{"My", "Editors' Choice", "COMM"};
    int[] titles = new int[]{R.string.teams_my, R.string.teams_editors, R.string.teams_community};
    private ArrayList<Fragment> fragments = new ArrayList<>();

    private CommunityTeamsFragment communityFragment;
    private EditorsTeamsFragment premadeFragment;
    private MyTeamsRecycleViewFragment myTeamsFragment;
    private ViewGroup rootView;


    public static TeamsFragment newInstance() {
        TeamsFragment fragment = new TeamsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.activity_composition, container, false);

        ViewPager viewPager = rootView.findViewById(R.id.viewPager);
        myTeamsFragment = new MyTeamsRecycleViewFragment();
        premadeFragment = new EditorsTeamsFragment();
        communityFragment = new CommunityTeamsFragment();
        fragments.add(myTeamsFragment);
        fragments.add(premadeFragment);
        fragments.add(communityFragment);

        SimpleViewPagerAdapter pagerAdapter = new SimpleViewPagerAdapter(getChildFragmentManager(), getContext(), titles, fragments);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = rootView.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_my_team_2);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_team_premade);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_upload_2);

//        GuideApplication.addADS(this, R.id.ads_banner);
        return rootView;
    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home) {
//            finish();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}
