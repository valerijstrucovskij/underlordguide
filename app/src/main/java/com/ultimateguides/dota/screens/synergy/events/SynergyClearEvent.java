package com.ultimateguides.dota.screens.synergy.events;

import android.view.View;

public class SynergyClearEvent {

    public final View v;

    public SynergyClearEvent(View v) {
        this.v = v;
    }
}
