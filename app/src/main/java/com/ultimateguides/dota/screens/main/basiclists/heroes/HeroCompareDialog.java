package com.ultimateguides.dota.screens.main.basiclists.heroes;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.TypefaceCompat;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.makeramen.roundedimageview.RoundedImageView;
import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.AlliancesImages;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.model.SkillStage;

import java.time.format.TextStyle;

import static com.kofigyan.stateprogressbar.StateProgressBar.StateNumber;
import static com.kofigyan.stateprogressbar.StateProgressBar.StateNumber.ONE;
import static com.kofigyan.stateprogressbar.StateProgressBar.StateNumber.THREE;
import static com.kofigyan.stateprogressbar.StateProgressBar.StateNumber.TWO;

public class HeroCompareDialog extends BottomSheetDialog {

    AlliancesImages images = new AlliancesImages();
    private StateNumber[] stateNumbers = {ONE, TWO, THREE};
    private final ViewGroup rootDialog;

    public HeroCompareDialog(@NonNull Context context) {
        super(context);
        rootDialog = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_hero_comparison, null);
        setContentView(rootDialog);
    }

    public void setModels(HeroModel model1, HeroModel model2) {
        HeroModelStages modelStages1 = new HeroModelStages(model1);
        modelStages1.setCurrentStage(0);
        HeroModelStages modelStages2 = new HeroModelStages(model2);
        modelStages2.setCurrentStage(0);

        ViewGroup container1 = rootDialog.findViewById(R.id.container1);
        ViewGroup container2 = rootDialog.findViewById(R.id.container2);
        ViewHolder viewHolder1 = new ViewHolder(container1);
        ViewHolder viewHolder2 = new ViewHolder(container2);

        updateContainer(viewHolder1, modelStages1, modelStages2);
        updateContainer(viewHolder2, modelStages2, modelStages1);

        StateProgressBar levelView_1 = container1.findViewById(R.id.select_tier);
        levelView_1.setOnStateItemClickListener((stateProgressBar, stateItem, stateNumber, isCurrentState) -> {
            stateProgressBar.setCurrentStateNumber(stateNumbers[stateNumber - 1]);
            modelStages1.setCurrentStage(stateNumber - 1);
            updateContainer(viewHolder1, modelStages1, modelStages2);
            updateContainer(viewHolder2, modelStages2, modelStages1);
        });

        StateProgressBar levelView_2 = container2.findViewById(R.id.select_tier);
        levelView_2.enableAnimationToCurrentState(true);
        levelView_2.setOnStateItemClickListener((stateProgressBar, stateItem, stateNumber, isCurrentState) -> {
            stateProgressBar.setCurrentStateNumber(stateNumbers[stateNumber - 1]);
            modelStages2.setCurrentStage(stateNumber - 1);
            updateContainer(viewHolder2, modelStages2, modelStages1);
            updateContainer(viewHolder1, modelStages1, modelStages2);
        });
    }

    private void updateContainer(ViewHolder viewHolder, HeroModelStages model, HeroModelStages modelOther) {
        viewHolder.nameView.setText(String.valueOf(model.name));
        viewHolder.priceView.setText(String.valueOf(model.goldCost));
        viewHolder.avatarView.setImageResource(model.avatarRes);
        viewHolder.race1View.setImageResource(images.get(model.alliance1));
        viewHolder.race2View.setImageResource(images.get(model.alliance2));
        viewHolder.race3View.setImageResource(images.get(model.alliance3));

        viewHolder.setText(viewHolder.healthView, model.health, modelOther.health);
        viewHolder.setText(viewHolder.manaView, model.maxMana, modelOther.maxMana);
        viewHolder.setText(viewHolder.dpsView, model.dps, modelOther.dps);
        viewHolder.setText(viewHolder.damageView, model.damageMin + model.damageMax, modelOther.damageMin + modelOther.damageMax);
        viewHolder.setText(viewHolder.attackSpeedView, model.attackRate, modelOther.attackRate);
        viewHolder.setText(viewHolder.movementSpeedView, model.movespeed, modelOther.movespeed);
        viewHolder.setText(viewHolder.attackRangeView, model.attackRange, modelOther.attackRange);
        viewHolder.setText(viewHolder.magicResistView, model.magicResist, modelOther.magicResist);
        viewHolder.setText(viewHolder.armorView, model.armor, modelOther.armor);
    }

    private class HeroModelStages {
        public int currentStage = 0;
        public String name;
        public AllianceType alliance1;
        public AllianceType alliance2;
        public AllianceType alliance3;
        public int avatarRes;

        public int dps;
        public int armor;
        public int attackRange;
        public float attackRate;
        public int goldCost;
        public int goldCostBasic;
        public int id;
        public int magicResist;
        public int maxMana;
        public int movespeed;
        public int damageMin;
        public int damageMax;
        public int health;

        public SkillStage[] stages = new SkillStage[3];

        public HeroModelStages(HeroModel model) {
            stages[0] = new SkillStage();
            stages[1] = new SkillStage();
            stages[2] = new SkillStage();

            name = model.name;
            goldCost = model.goldCost;
            goldCostBasic = model.goldCost;
            alliance1 = model.alliance1;
            alliance2 = model.alliance2;
            alliance3 = model.alliance3;
            avatarRes = model.avatarRes;

            stages[0].health = model.health.get(0);
            stages[1].health = model.health.get(1);
            stages[2].health = model.health.get(2);

            if (isArray(model.armor)) {
                String[] armorArray = model.armor.split(",");
                stages[0].armor = Integer.parseInt(armorArray[0]);
                stages[1].armor = Integer.parseInt(armorArray[1]);
                stages[2].armor = Integer.parseInt(armorArray[2]);
            } else {
                stages[0].armor = Integer.parseInt(model.armor);
                stages[1].armor = Integer.parseInt(model.armor);
                stages[2].armor = Integer.parseInt(model.armor);
            }

            stages[0].mana = model.maxMana;
            stages[1].mana = model.maxMana;
            stages[2].mana = model.maxMana;

            stages[0].attackDamageMin = model.damageMin.get(0);
            stages[1].attackDamageMin = model.damageMin.get(1);
            stages[2].attackDamageMin = model.damageMin.get(2);

            stages[0].attackDamageMax = model.damageMax.get(0);
            stages[1].attackDamageMax = model.damageMax.get(1);
            stages[2].attackDamageMax = model.damageMax.get(2);

            if (isArray(model.attackRate)) {
                String[] array = model.attackRate.split(",");
                stages[0].attackSpeed = Float.parseFloat(array[0]);
                stages[1].attackSpeed = Float.parseFloat(array[1]);
                stages[2].attackSpeed = Float.parseFloat(array[2]);
            } else {
                stages[0].attackSpeed = Float.parseFloat(model.attackRate);
                stages[1].attackSpeed = Float.parseFloat(model.attackRate);
                stages[2].attackSpeed = Float.parseFloat(model.attackRate);
            }

            stages[0].moveSpeed = model.movespeed;
            stages[1].moveSpeed = model.movespeed;
            stages[2].moveSpeed = model.movespeed;

            stages[0].attackRange = model.attackRange;
            stages[1].attackRange = model.attackRange;
            stages[2].attackRange = model.attackRange;

            if (isArray(model.magicResist)) {
                String[] array = model.magicResist.split(",");
                stages[0].magicResist = Integer.parseInt(array[0]);
                stages[1].magicResist = Integer.parseInt(array[1]);
                stages[2].magicResist = Integer.parseInt(array[2]);
            } else {
                stages[0].magicResist = Integer.parseInt(model.magicResist);
                stages[1].magicResist = Integer.parseInt(model.magicResist);
                stages[2].magicResist = Integer.parseInt(model.magicResist);
            }

            stages[0].dps = (int) (stages[0].attackDamageMin * stages[0].attackSpeed);
            stages[1].dps = (int) (stages[1].attackDamageMin * stages[1].attackSpeed);
            stages[2].dps = (int) (stages[2].attackDamageMin * stages[2].attackSpeed);
        }


        public void setCurrentStage(int i) {
            currentStage = i;
            switch (i) {
                case 0:
                    goldCost = goldCostBasic;
                    break;
                case 1:
                    goldCost = goldCostBasic * 3;
                    break;

                case 2:
                    goldCost = goldCostBasic * 9;
                    break;
            }
            health = stages[currentStage].health;
            dps = stages[currentStage].dps;
            armor = stages[currentStage].armor;
            maxMana = stages[currentStage].mana;
            damageMin = stages[currentStage].attackDamageMin;
            damageMax = stages[currentStage].attackDamageMax;
            attackRate = stages[currentStage].attackSpeed;
            attackRange = stages[currentStage].attackRange;
            movespeed = stages[currentStage].moveSpeed;
            magicResist = stages[currentStage].magicResist;
        }

        private boolean isArray(String value) {
            if (value.contains(",")
                    || value.contains("/")) {
                return true;
            }
            return false;
        }
    }

    private class ViewHolder {
        RoundedImageView avatarView;
        ImageView race1View;
        ImageView race2View;
        ImageView race3View;
        TextView nameView;
        TextView priceView;
        TextView healthView;
        TextView manaView;
        TextView dpsView;
        TextView damageView;
        TextView attackSpeedView;
        TextView movementSpeedView;
        TextView attackRangeView;
        TextView magicResistView;
        TextView armorView;

        public ViewHolder(ViewGroup root) {
            avatarView = root.findViewById(R.id.iv_avatar);
            race1View = root.findViewById(R.id.iv_race_1);
            race2View = root.findViewById(R.id.iv_race_2);
            race3View = root.findViewById(R.id.iv_race_3);
            nameView = root.findViewById(R.id.tv_name);
            priceView = root.findViewById(R.id.tv_price);
            healthView = root.findViewById(R.id.tv_health);
            manaView = root.findViewById(R.id.tv_mana);
            dpsView = root.findViewById(R.id.tv_dps);
            damageView = root.findViewById(R.id.tv_damage);
            attackSpeedView = root.findViewById(R.id.tv_attack_speed);
            movementSpeedView = root.findViewById(R.id.tv_movement_speed);
            attackRangeView = root.findViewById(R.id.tv_attack_range);
            magicResistView = root.findViewById(R.id.tv_magic_resist);
            armorView = root.findViewById(R.id.tv_armor);
        }

        public void setText(TextView view, int value1, int value2) {
            view.setText(String.format("%s", value1));
            if (value1 > value2) {
                view.setTextColor(ContextCompat.getColor(GuideApplication.getApp(), R.color.tier_1));
            } else {
                view.setTextColor(ContextCompat.getColor(GuideApplication.getApp(), R.color.white));
            }
        }

        public void setText(TextView view, float value1, float value2) {
            view.setText(String.format("%s", value1));
            if (value1 > value2) {
                view.setTextColor(ContextCompat.getColor(GuideApplication.getApp(), R.color.tier_1));
            } else {
                view.setTextColor(ContextCompat.getColor(GuideApplication.getApp(), R.color.white));
            }
        }
    }
}
