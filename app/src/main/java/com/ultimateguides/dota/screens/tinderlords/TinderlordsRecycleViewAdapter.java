package com.ultimateguides.dota.screens.tinderlords;


import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.HeroImages;
import com.ultimateguides.dota.model.firebase.TinderlordsData;
import com.ultimateguides.dota.others.Action1;

import java.util.List;

public class TinderlordsRecycleViewAdapter extends RecyclerView.Adapter<TinderlordsRecycleViewAdapter.ViewHolderItem> {

    List<TinderlordsData> data;
    private final String userId;
    private Action1<String> onItemClick;

    public TinderlordsRecycleViewAdapter(List<TinderlordsData> data, String userId) {
        this.data = data;
        this.userId = userId;
    }

    @Override
    public TinderlordsRecycleViewAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_tinderlords_user, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(TinderlordsRecycleViewAdapter.ViewHolderItem holder, int position) {
        TinderlordsData dataItem = getItem(position);
        if (TextUtils.isEmpty(dataItem.message)) {
            holder.messageView.setText("no message");
        } else {
            holder.messageView.setText(dataItem.message);
        }
        if (TextUtils.isEmpty(dataItem.discordUser)) {
            holder.discordView.setText("no discord");
        } else {
            holder.discordView.setText(dataItem.discordUser);
        }
        if (TextUtils.isEmpty(dataItem.steamUser)) {
            holder.steamView.setText("no steam");
        } else {
            holder.steamView.setText(dataItem.steamUser);
        }
        holder.langView.setText("[" + dataItem.lang + "]");

        if (dataItem.ownerId.equals(userId)) {
            holder.btnClose.setVisibility(View.VISIBLE);
        } else {
            holder.btnClose.setVisibility(View.GONE);
        }

//        holder.root.setOnClickListener(view -> onItemClick.call(position));
//        holder.ratingUp.setOnClickListener(view -> {
//            //dataItem.userRating += 1;
//            onRatingClick.call(position);
//        });
//
        holder.btnClose.setOnClickListener(view -> {
            onItemClick.call(dataItem.key);
        });
//
//        if (!TextUtils.isEmpty(dataItem.dateModified)) {
//            holder.modified.setText(dataItem.dateModified);
//        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private TinderlordsData getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<String> onItemClick) {
        this.onItemClick = onItemClick;
    }
//
//    public void setOnRatingClick(Action1<Integer> onItemClick) {
//        this.onRatingClick = onItemClick;
//    }
//
//    public void setOnBookmarkClick(Action1<Integer> onBookmarkClick) {
//        this.onBookmarkClick = onBookmarkClick;
//    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView messageView;
        TextView langView;
        TextView steamView;
        TextView discordView;
        ImageView btnClose;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            messageView = itemView.findViewById(R.id.tv_message);
            discordView = itemView.findViewById(R.id.tv_discord_id);
            steamView = itemView.findViewById(R.id.tv_steam_id);
            langView = itemView.findViewById(R.id.tv_lang);
            btnClose = itemView.findViewById(R.id.iv_delete);
        }
    }
}
