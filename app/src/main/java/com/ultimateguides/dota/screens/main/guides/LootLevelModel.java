package com.ultimateguides.dota.screens.main.guides;

public class LootLevelModel {

    public String level;
    public String name;
    public String creeps;
    public String itemsTiers;
    public String tierDropRate;
    public final int imageWave;

    public LootLevelModel(String level, String name, String creeps, String itemsTiers, String tierDropRate, int imageWave) {
        this.level = level;
        this.name = name;
        this.creeps = creeps;
        this.itemsTiers = itemsTiers;
        this.tierDropRate = tierDropRate;
        this.imageWave = imageWave;
    }
}
