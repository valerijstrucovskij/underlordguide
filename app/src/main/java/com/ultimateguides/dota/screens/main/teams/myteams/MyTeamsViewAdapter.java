package com.ultimateguides.dota.screens.main.teams.myteams;


import android.content.res.ColorStateList;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.HeroImages;
import com.ultimateguides.dota.database.TeamEntity;
import com.ultimateguides.dota.others.Action1;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MyTeamsViewAdapter extends RecyclerView.Adapter<MyTeamsViewAdapter.ViewHolderItem> {
    List<TeamEntity> data;
    private Action1<Integer> onItemClick;
    private Action1<TeamEntity> onDeleteTeam;
    HeroImages images = new HeroImages();

    public MyTeamsViewAdapter(List<TeamEntity> data) {
        this.data = data;
    }

    @Override
    public MyTeamsViewAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_team_my, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(MyTeamsViewAdapter.ViewHolderItem holder, int position) {
        TeamEntity dataItem = getItem(position);
        holder.name.setText(dataItem.name);
        if (dataItem.heroes.size() >= 1) {
            holder.hero1.setImageResource(images.get(dataItem.heroes.get(0)));
        }

        for (int i = 0; i < holder.heroPortraits.size(); i++) {
            AppCompatImageView portrait = holder.heroPortraits.get(i);
            if (dataItem.heroes.size() - 1 >= i) {
                portrait.setImageResource(images.get(dataItem.heroes.get(i)));
            } else {
                portrait.setImageResource(R.color.transparent);
            }
        }
        holder.root.setOnClickListener(view -> onItemClick.call(position));
        holder.deleteTeam.setOnClickListener(view -> {
            onDeleteTeam.call(dataItem);
        });

        String lordSaved = dataItem.lordName;
        if (!TextUtils.isEmpty(lordSaved)) {
            int lordIcon = R.drawable.placeholder_hero;
            switch (lordSaved) {
                case "annesix":
                    lordIcon = R.drawable.lord_annesix;
                    break;
                case "hobgen":
                    lordIcon = R.drawable.lord_hobgen;
                    break;
            }
            holder.lordView.setImageResource(lordIcon);
        }

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
        String dateString = format.format(dataItem.modified);
        if (!TextUtils.isEmpty(dateString)) {
            holder.modified.setText("modified " + dateString);
        }

        if (dataItem.backgroundColor != 0) {
            holder.root.setBackgroundTintList(ColorStateList.valueOf(dataItem.backgroundColor));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private TeamEntity getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<Integer> onItemClick) {
        this.onItemClick = onItemClick;
    }

    public void setOnDeleteTeam(Action1<TeamEntity> onItemClick) {
        this.onDeleteTeam = onItemClick;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView name;
        TextView modified;
        TextView rating;
        AppCompatImageView hero1;
        AppCompatImageView hero2;
        AppCompatImageView hero3;
        AppCompatImageView hero4;
        AppCompatImageView hero5;
        AppCompatImageView hero6;
        AppCompatImageView hero7;
        AppCompatImageView hero8;
        AppCompatImageView hero9;
        AppCompatImageView hero10;
        RoundedImageView lordView;

        List<AppCompatImageView> heroPortraits;
        AppCompatImageView deleteTeam;

        public ViewHolderItem(View itemView) {
            super(itemView);
            heroPortraits = new ArrayList<>();
            root = (ViewGroup) itemView;
            name = itemView.findViewById(R.id.tv_name);
            lordView = itemView.findViewById(R.id.iv_lord);
//            rating = itemView.findViewById(R.id.tv_rating);
            hero1 = itemView.findViewById(R.id.iv_hero_1);
            hero2 = itemView.findViewById(R.id.iv_hero_2);
            hero3 = itemView.findViewById(R.id.iv_hero_3);
            hero4 = itemView.findViewById(R.id.iv_hero_4);
            hero5 = itemView.findViewById(R.id.iv_hero_5);
            hero6 = itemView.findViewById(R.id.iv_hero_6);
            hero7 = itemView.findViewById(R.id.iv_hero_7);
            hero8 = itemView.findViewById(R.id.iv_hero_8);
            hero9 = itemView.findViewById(R.id.iv_hero_9);
            hero10 = itemView.findViewById(R.id.iv_hero_10);
            heroPortraits.add(hero1);
            heroPortraits.add(hero2);
            heroPortraits.add(hero3);
            heroPortraits.add(hero4);
            heroPortraits.add(hero5);
            heroPortraits.add(hero6);
            heroPortraits.add(hero7);
            heroPortraits.add(hero8);
            heroPortraits.add(hero9);
            heroPortraits.add(hero10);

            deleteTeam = itemView.findViewById(R.id.iv_team_remove);
            modified = itemView.findViewById(R.id.tv_modified);
        }
    }
}
