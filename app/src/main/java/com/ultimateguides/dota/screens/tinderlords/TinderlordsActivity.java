package com.ultimateguides.dota.screens.tinderlords;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.PreferenceField;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.customviews.LoadingView;
import com.ultimateguides.dota.model.firebase.TinderlordsData;
import com.ultimateguides.dota.screens.ParentListActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class TinderlordsActivity extends ParentListActivity {

    private List<TinderlordsData> dataList = new ArrayList<>();
    private TinderlordsRecycleViewAdapter adapter;
    String uuid = GuideApplication.getPrefs().getString(PreferenceField.UUID, "");

    @Override
    public int getLayoutId() {
        return R.layout.activity_tinderlords;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findViewById(R.id.btn_send_request).setOnClickListener(this::onClickUpload);

        LoadingView waiting = findViewById(R.id.waiting);
        ViewGroup emptyPlaceholder = findViewById(R.id.empty_list);
        RecyclerView recyclerView = findViewById(R.id.rv_requests);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TinderlordsRecycleViewAdapter(dataList, uuid);
        recyclerView.setAdapter(adapter);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference table = database.getReference("tinderlords");
        waiting.setVisibility(View.VISIBLE);
        table.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataList.clear();

                for (DataSnapshot itemSnapshot : dataSnapshot.getChildren()) {
                    String key = itemSnapshot.getKey();
                    TinderlordsData model = itemSnapshot.getValue(TinderlordsData.class);
                    model.key = key;
                    dataList.add(model);
                }
//                Collections.sort(dataList, (t1, t2) -> t1.rating.compareTo(t2.rating));
                if (dataList.size() == 0) {
                    emptyPlaceholder.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    emptyPlaceholder.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
                adapter.notifyDataSetChanged();
                waiting.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        adapter.setOnItemClick(key -> {
            table.child(key).removeValue();
        });
    }

    public void onClickUpload(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Send request");
        View rootView = getLayoutInflater().inflate(R.layout.dialog_send_request_tinderlords, null, false);
        builder.setView(rootView);
        builder.setCancelable(false);
        builder.setPositiveButton("Send", null);

        builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());
        builder.setIcon(R.drawable.ic_suggest);
        AlertDialog alert = builder.show();

        Button btnOk = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        btnOk.setOnClickListener(view -> {
            TextInputEditText langView = rootView.findViewById(R.id.et_lang);
            TextInputEditText steamNameView = rootView.findViewById(R.id.et_steam_name);
            TextInputEditText discordNameView = rootView.findViewById(R.id.et_discord_name);
            TextInputEditText messageView = rootView.findViewById(R.id.et_message);

            String lang = langView.getText().toString();
            if (TextUtils.isEmpty(lang)) {
                lang = "english";
            }
            String steamUser = steamNameView.getText().toString();
            String discordUser = discordNameView.getText().toString();
            String message = messageView.getText().toString();


            if (!TextUtils.isEmpty(discordUser) && !discordUser.contains("#")) {
                FancyToast.makeText(TinderlordsActivity.this,
                        "Warning! Usually discord account has # symbol",
                        Toast.LENGTH_LONG,
                        FancyToast.WARNING,
                        false
                ).show();
                return;
            }

            if (TextUtils.isEmpty(steamUser) && TextUtils.isEmpty(discordUser)) {
                FancyToast.makeText(TinderlordsActivity.this,
                        "Sorry, but discord or steam account must be filled",
                        Toast.LENGTH_LONG,
                        FancyToast.ERROR,
                        false
                ).show();
                return;
            }

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            HashMap<String, String> result = new HashMap<>();
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String strDate = formatter.format(date);

            result.put("lang", lang);
            result.put("steamUser", steamUser);
            result.put("discordUser", discordUser);
            result.put("message", message);
            result.put("date", strDate);
            result.put("ownerId", uuid);

            DatabaseReference table = database.getReference("tinderlords");
            table.push().setValue(result);
            FancyToast.makeText(TinderlordsActivity.this,
                    "Success! Now all the players will see your request",
                    Toast.LENGTH_SHORT,
                    FancyToast.SUCCESS,
                    false
            ).show();
            alert.dismiss();
        });

    }
}
