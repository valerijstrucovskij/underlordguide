package com.ultimateguides.dota.screens.main.basiclists.alliances;

import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.gson.Gson;
import com.ultimateguides.dota.Config;
import com.ultimateguides.dota.model.AllianceModel;
import com.ultimateguides.dota.model.spreadsheet.ResponseAddionsModel;
import com.ultimateguides.dota.screens.ParentPresenter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AlliancesListPresenter extends ParentPresenter<AllianceModel> {

    public AlliancesListPresenter(String tableRange) {
        super(tableRange);
    }

    public ArrayList<AllianceModel> requestData() {
        try {
            ValueRange readResult = sheetsService.spreadsheets().values()
                    .get(Config.spreadsheet_additions_id, TABLE_RANGE)
                    .setKey(Config.google_api_key)
                    .execute();

            String factory = readResult.toPrettyString();
            ResponseAddionsModel response = new Gson().fromJson(factory, ResponseAddionsModel.class);

            ArrayList<AllianceModel> alliances = new ArrayList<>();
            for (int i = 0; i < response.values.size(); i++) {
                List<String> allianceString = response.values.get(i);
                try {
                    AllianceModel model = new AllianceModel(allianceString);
                    alliances.add(model);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return alliances;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
