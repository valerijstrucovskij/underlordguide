package com.ultimateguides.dota.screens.herodetails;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.makeramen.roundedimageview.RoundedImageView;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.AlliancesImages;
import com.ultimateguides.dota.model.HeroModel;

public class HeroDetailsDialog extends BottomSheetDialog {
    private HeroModel model;

    public HeroDetailsDialog(@NonNull Context context, HeroModel model) {
        super(context);
        this.model = model;
        initView(model);
    }
//
//    public HeroDetailsDialog(HeroModel model) {
//        super();
//        this.model = model;
//    }

//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return initView(model);
//    }

    private void initView(HeroModel model) {
        ViewGroup root = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_hero_details, null);
        setContentView(root);

        RoundedImageView avatarView = root.findViewById(R.id.iv_avatar);
        ImageView race1View = root.findViewById(R.id.iv_race_1);
        ImageView race2View = root.findViewById(R.id.iv_race_2);
        ImageView race3View = root.findViewById(R.id.iv_race_3);
        TextView nameView = root.findViewById(R.id.tv_name);
        TextView priceView = root.findViewById(R.id.tv_price);
        TextView healthView = root.findViewById(R.id.tv_health);
        TextView manaView = root.findViewById(R.id.tv_mana);
        TextView dpsView = root.findViewById(R.id.tv_dps);
        TextView damageView = root.findViewById(R.id.tv_damage);
        TextView attackSpeedView = root.findViewById(R.id.tv_attack_speed);
        TextView movementSpeedView = root.findViewById(R.id.tv_movement_speed);
        TextView attackRangeView = root.findViewById(R.id.tv_attack_range);
        TextView magicResistView = root.findViewById(R.id.tv_magic_resist);
        TextView armorView = root.findViewById(R.id.tv_armor);

//        SkillStage st1 = model.stats.stage1;
//        SkillStage st2 = model.stats.stage2;
//        SkillStage st3 = model.stats.stage3;

        nameView.setText(String.valueOf(model.name));
        priceView.setText(String.valueOf(model.goldCost));
        avatarView.setImageResource(model.avatarRes);
        AlliancesImages images = new AlliancesImages();
        race1View.setImageResource(images.get(model.alliance1));
        race2View.setImageResource(images.get(model.alliance2));
        race3View.setImageResource(images.get(model.alliance3));
//        healthView.setText(String.format("%s/%s/%s", model.health., st2.health, st3.health));
        healthView.setText(String.format("%s", model.health));
        manaView.setText(String.format("%s", model.maxMana));
//        dpsView.setText(String.format("%s/%s/%s", st1.dps, st2.dps, st3.dps));
        dpsView.setText(String.format("%s", model.dps));
        damageView.setText(String.format("%s-%s/%s-%s/%s-%s",
                model.damageMin.get(0), model.damageMax.get(0),
                model.damageMin.get(1), model.damageMax.get(1),
                model.damageMin.get(2), model.damageMax.get(2)));
//        attackSpeedView.setText(String.format("%s/%s/%s", st1.attackSpeed, st2.attackSpeed, st3.attackSpeed));
        attackSpeedView.setText(String.format("%s", model.attackRate));
        movementSpeedView.setText(String.format("%s", model.movespeed));
        attackRangeView.setText(String.format("%s", model.attackRange));
        magicResistView.setText(String.format("%s", model.magicResist));
        armorView.setText(String.format("%s", model.armor));

//        return root;
    }

    //    private void showAddNewHeroDialog(View v) {
//        BottomSheetDialog builder = new BottomSheetDialog(this);
//
//        ViewGroup root = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_select_hero, null);
//        HeroesToTeamsRecycleViewAdapter adapterDialog = new HeroesToTeamsRecycleViewAdapter(heroList);
//        SearchView searchView = root.findViewById(R.id.searchView);
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                adapterDialog.getFilter().filter(newText);
//                return false;
//            }
//        });
//        RecyclerView heroesView = root.findViewById(R.id.rv_heroes);
//        MaterialButton btnClose = root.findViewById(R.id.btn_close);
//        heroesView.setAdapter(adapterDialog);
//        builder.setContentView(root);
//        builder.show();
////        btnClose.setOnClickListener(view -> dialog.dismiss());
//        adapterDialog.setOnItemClick(param -> {
//            builder.dismiss();
//            teamModel.heroes.add(param.type);
//            selectedHeroList.add(param);
//            adapter.notifyDataSetChanged();
//            onTeamChanged();
//        });
//    }

}
