package com.ultimateguides.dota.screens.main.ranks.leaderboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.customviews.LoadingView;
import com.ultimateguides.dota.model.steamApi.LeaderboardModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class LeaderboardFragment extends Fragment {

    LoadingView loadingView;
    WebView webView;
    private List<LeaderboardModel> dataList = new ArrayList<>();
    private LeaderboardRecycleViewAdapter adapter = new LeaderboardRecycleViewAdapter(dataList);
    private ViewGroup rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_leaderboard, container, false);

        loadingView = rootView.findViewById(R.id.loading);
        webView = rootView.findViewById(R.id.webView);

        webView.setVisibility(View.GONE);
        loadingView.setVisibility(View.VISIBLE);
        webView.addJavascriptInterface(new JSCallback(), "Android");
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new LeaderboardWebviewClient());
        WebView.setWebContentsDebuggingEnabled(false);

        RecyclerView recyclerView = rootView.findViewById(R.id.recycleView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getContext().getResources().getDrawable(R.drawable.divider_white));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);

        loadingView.setVisibility(View.VISIBLE);
        webView.loadUrl("https://underlords.com/leaderboard");
        return rootView;
    }

    public class JSCallback {

        @JavascriptInterface
        public void onLeaderboard(String value) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Type listType = new TypeToken<ArrayList<LeaderboardModel>>() {
            }.getType();
            ArrayList<LeaderboardModel> array = gson.fromJson(value, listType);
            dataList.clear();
            dataList.addAll(array);
            getActivity().runOnUiThread(() -> {
                webView.setVisibility(View.GONE);
                loadingView.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
            });
        }
    }
}