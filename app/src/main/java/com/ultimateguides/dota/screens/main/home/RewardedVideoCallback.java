package com.ultimateguides.dota.screens.main.home;

import com.appodeal.ads.RewardedVideoCallbacks;

public class RewardedVideoCallback implements RewardedVideoCallbacks {
    @Override
    public void onRewardedVideoLoaded(boolean b) {

    }

    @Override
    public void onRewardedVideoFailedToLoad() {

    }

    @Override
    public void onRewardedVideoShown() {

    }


    @Override
    public void onRewardedVideoFinished(double v, String s) {

    }

    @Override
    public void onRewardedVideoClosed(boolean b) {

    }

    @Override
    public void onRewardedVideoExpired() {

    }

    @Override
    public void onRewardedVideoClicked() {

    }
}
