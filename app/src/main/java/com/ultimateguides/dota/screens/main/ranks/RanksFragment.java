package com.ultimateguides.dota.screens.main.ranks;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.screens.SimpleViewPagerAdapter;
import com.ultimateguides.dota.screens.main.ranks.leaderboard.LeaderboardFragment;
import com.ultimateguides.dota.screens.main.ranks.myrank.MyRankFragment;

import java.util.ArrayList;

public class RanksFragment extends Fragment {


    View btnShare;

    private ArrayList<Fragment> fragments = new ArrayList<>();
    private final LeaderboardFragment leaderboardFragment = new LeaderboardFragment();
    private final MyRankFragment myRankFragment = new MyRankFragment();
    private int[] titles = new int[]{R.string.ranks_my};
    private ViewGroup rootView;

    public RanksFragment() {
    }

    public static RanksFragment newInstance() {
        RanksFragment fragment = new RanksFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.activity_ranks, container, false);

        ViewPager viewPager = rootView.findViewById(R.id.viewPager);
        fragments.add(myRankFragment);
        // fragments.add(leaderboardFragment);
        SimpleViewPagerAdapter pagerAdapter = new SimpleViewPagerAdapter(getChildFragmentManager(), getContext(), titles, fragments);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = rootView.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_my_rank);
//        tabLayout.getTabAt(1).setIcon(R.drawable.ic_leaderboard);


//        btnShare = rootView.findViewById(R.id.btn_info);
//        btnShare.setOnClickListener(this::openFAQInfo);
//        chart.setBackgroundColor(getResources().getColor(R.color.light_gray));
//        GuideApplication.addADS(this, R.id.ads_banner);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        GuideApplication.getAnalytics().setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());
    }


    //TODO share button
//    public void onClickShare(View v) {
//        int overallGames = data.size();
//        int overallRate = 0;
//        for (RankModel model : data) {
//            overallRate += model.FinalRank;
//        }
//        float averageRate = overallRate / (overallGames * 1f);
//        String averageRateString = String.format("%.2f", averageRate);
//
//        String message = " Fun fuct! Played ";
//        message += overallGames + " games ";
//        message += "with average rank " + averageRateString;
//        message += ". Not so bad I guess!";
//        message += "#dotaunderlords";
//
//        String tweetUrl = "https://twitter.com/intent/tweet?text=" + message;
//        Uri uri = Uri.parse(tweetUrl);
//        startActivity(new Intent(Intent.ACTION_VIEW, uri));
//    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                finish();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
}
