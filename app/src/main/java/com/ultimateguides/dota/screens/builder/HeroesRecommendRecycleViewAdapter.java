package com.ultimateguides.dota.screens.builder;


import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.AlliancesImages;
import com.ultimateguides.dota.customviews.AllianceView;
import com.ultimateguides.dota.model.AllianceModel;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.others.Action1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HeroesRecommendRecycleViewAdapter extends RecyclerView.Adapter<HeroesRecommendRecycleViewAdapter.ViewHolderItem>
        implements Filterable {

    List<HeroModel> data;
    private Action1<HeroModel> onItemClick;
    private List<HeroModel> filteredData;
    private List<AllianceModel> allianceList;
    private HashMap<AllianceType, Integer> allianceBonuses;
    AlliancesImages images = new AlliancesImages();

    public HeroesRecommendRecycleViewAdapter(List<HeroModel> data, List<AllianceModel> allianceList, HashMap<AllianceType, Integer> allianceBonuses) {
        this.data = data;
        this.filteredData = data;
        this.allianceList = allianceList;
        this.allianceBonuses = allianceBonuses;
    }

    @Override
    public HeroesRecommendRecycleViewAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_hero_recommended, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(HeroesRecommendRecycleViewAdapter.ViewHolderItem holder, int position) {
        HeroModel dataItem = getItem(position);
        holder.imageView.setImageResource(dataItem.avatarRes);
//        ((ViewHolderItem) holder).tierView.setText("" + dataItem.draftTier);
//        holder.tierView.setVisibility(View.INVISIBLE);
        holder.nameView.setText("" + dataItem.name);
        holder.tierView.setText("" + dataItem.goldCost);
        if (dataItem.draftTier == 5) {
            holder.root.setBackgroundResource(R.color.ace);
        } else {
            holder.root.setBackgroundResource(R.color.dialogBackground);
        }

//        holder.allianceIcons1.setImageResource(images.get(dataItem.alliance1));
//        holder.allianceIcons2.setImageResource(images.get(dataItem.alliance2));
//        holder.allianceIcons3.setImageResource(images.get(dataItem.alliance3));

        AllianceType alliance1Type = dataItem.alliance1;
        holder.alliance1.setAllianceIcon(images.get(alliance1Type));
        holder.alliance1.setStages(findAllianceOfType(alliance1Type).maxStages);
        holder.alliance1.setUnitsPerStage(findAllianceOfType(alliance1Type).unitsPerStage);
        holder.alliance1.setHighlightLast(true);
        if(allianceBonuses.containsKey(alliance1Type)) {
            holder.alliance1.setActiveCount(allianceBonuses.get(alliance1Type)+1);
        }
        holder.alliance1.updateView();

        AllianceType alliance1Type2 = dataItem.alliance2;
        holder.alliance2.setAllianceIcon(images.get(alliance1Type2));
        holder.alliance2.setStages(findAllianceOfType(alliance1Type2).maxStages);
        holder.alliance2.setUnitsPerStage(findAllianceOfType(alliance1Type2).unitsPerStage);
        holder.alliance2.setHighlightLast(true);
        if(allianceBonuses.containsKey(alliance1Type2)) {
            holder.alliance2.setActiveCount(allianceBonuses.get(alliance1Type2)+1);
        }
        holder.alliance2.updateView();
        if (TextUtils.isEmpty(dataItem.alliance3.toString()) ||
                dataItem.alliance3.toString().equals("None")) {
            holder.alliance3.setVisibility(View.GONE);
        } else {
            holder.alliance3.setVisibility(View.VISIBLE);
            AllianceType alliance1Type3 = dataItem.alliance3;
            holder.alliance3.setAllianceIcon(images.get(alliance1Type3));
            holder.alliance3.setStages(findAllianceOfType(alliance1Type3).maxStages);
            holder.alliance3.setUnitsPerStage(findAllianceOfType(alliance1Type3).unitsPerStage);
            holder.alliance3.setHighlightLast(true);
            if(allianceBonuses.containsKey(alliance1Type3)) {
                holder.alliance3.setActiveCount(allianceBonuses.get(alliance1Type3)+1);
            }
            holder.alliance3.updateView();
        }

        holder.root.setOnClickListener(view -> onItemClick.call(dataItem));
    }

    private AllianceModel findAllianceOfType(AllianceType type) {
        if (allianceList.size() > 0) {
            for (AllianceModel model : allianceList) {
                if (model.type == type) {
                    return model;
                }
            }
        }
        return null;
    }
//    view.setAllianceIcon(imagesAlliances.get(type));
//            view.setStages(allianceItem.maxStages);
//            view.setUnitsPerStage(allianceItem.unitsPerStage);
//            view.setActiveCount(countUnits);
//            view.setDescribe(completedStages == 0 ? "" : allianceItem.stages.get(completedStages - 1));
//            view.updateView();

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    private HeroModel getItem(int position) {
        return filteredData.get(position);
    }

    public void setOnItemClick(Action1<HeroModel> onItemClick) {
        this.onItemClick = onItemClick;
    }

    public List<HeroModel> getList() {
        return filteredData;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredData = data;
                } else {
                    List<HeroModel> filteredList = new ArrayList<>();
                    for (HeroModel row : data) {
                        if (row.name.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredData = (ArrayList<HeroModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        RoundedImageView imageView;
//        ImageView allianceIcons1;
//        ImageView allianceIcons2;
//        ImageView allianceIcons3;
        AllianceView alliance1;
        AllianceView alliance2;
        AllianceView alliance3;
        TextView tierView;
        TextView nameView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            imageView = itemView.findViewById(R.id.imageView);
            tierView = itemView.findViewById(R.id.tier);
            nameView = itemView.findViewById(R.id.tv_name);
//            allianceIcons1 = itemView.findViewById(R.id.iv_alliance_1);
//            allianceIcons2 = itemView.findViewById(R.id.iv_alliance_2);
//            allianceIcons3 = itemView.findViewById(R.id.iv_alliance_3);

            alliance1 = itemView.findViewById(R.id.alliance1);
            alliance2 = itemView.findViewById(R.id.alliance2);
            alliance3 = itemView.findViewById(R.id.alliance3);
        }
    }
}
