package com.ultimateguides.dota.screens.main;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.airbnb.lottie.LottieAnimationView;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.Purchase;
import com.appodeal.ads.Appodeal;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ultimateguides.dota.BuildConfig;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.LocaleHelper;
import com.ultimateguides.dota.PreferenceField;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.Utils;
import com.ultimateguides.dota.screens.main.basiclists.BasicsFragment;
import com.ultimateguides.dota.screens.main.guides.GuidesFragment;
import com.ultimateguides.dota.screens.main.home.HomeFragment;
import com.ultimateguides.dota.screens.main.home.ShowNeedUpdateEvent;
import com.ultimateguides.dota.screens.main.ranks.myrank.MyRankFragment;
import com.ultimateguides.dota.screens.main.teams.TeamsFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    final HomeFragment homeFragment = new HomeFragment();
    final MyRankFragment rankFragment = new MyRankFragment();
    final TeamsFragment teamsFragment = new TeamsFragment();
    final BasicsFragment basicsFragment = new BasicsFragment();
    final GuidesFragment guideFragment = new GuidesFragment();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = homeFragment;
    private AlertDialog updatingDatabaseDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navView = findViewById(R.id.nav_view_bottom);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
        fm.beginTransaction().add(R.id.nav_host_fragment, guideFragment, "5").hide(guideFragment).commit();
        fm.beginTransaction().add(R.id.nav_host_fragment, basicsFragment, "4").hide(basicsFragment).commit();
        fm.beginTransaction().add(R.id.nav_host_fragment, teamsFragment, "3").hide(teamsFragment).commit();
        fm.beginTransaction().add(R.id.nav_host_fragment, rankFragment, "2").hide(rankFragment).commit();
        fm.beginTransaction().add(R.id.nav_host_fragment, homeFragment, "1").commit();

        showLoading();
        requestRemoteConfig();

        GuideApplication.addADS(this, R.id.ads_banner);
    }
//
//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(LocaleHelper.setLocale(base));
//    }

    private void showLoading() {
        ViewGroup rootDialog = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_loading, null);
        LottieAnimationView lottie = rootDialog.findViewById(R.id.lottie_animation);
        lottie.enableMergePathsForKitKatAndAbove(true);

        try {
            lottie.setAnimation(R.raw.loading);
        } catch (Exception e) {

        }
        AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.AppTheme_BottomSheetDialog);
        dialog.setView(rootDialog);
        dialog.setCancelable(false);
        updatingDatabaseDialog = dialog.show();
    }

    private void setupBillingLibrary() {
        BillingClient billingClient = BillingClient.newBuilder(this)
                .enablePendingPurchases()
                .setListener((billingResult, purchases) -> {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        consumePurchases(null, purchases);
                    }
                })
                .build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {

                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    List<Purchase> purchasesList = queryPurchases(billingClient);
                    consumePurchases(billingClient, purchasesList);
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
            }
        });
    }

    private void consumePurchases(BillingClient billingClient,
                                  List<Purchase> purchasesList) {
        if (purchasesList != null) {
            if (purchasesList.size() > 0) {

                GuideApplication.setDonate(true);
                GuideApplication.getPrefs().edit()
                        .putBoolean(PreferenceField.DID_DONATION, true)
                        .putLong(PreferenceField.TIME_LAST_REWARDED_VIDEO, Long.MAX_VALUE)
                        .apply();
            }

            for (int i = 0; i < purchasesList.size(); i++) {
                String purchaseToken = purchasesList.get(i).getPurchaseToken();
                ConsumeParams.Builder params = ConsumeParams.newBuilder();
                params.setPurchaseToken(purchaseToken);
                if (billingClient != null) {
                    billingClient.consumeAsync(params.build(), (billingResult2, purchaseToken1) -> {
                    });
                }
            }
        }
    }

    private List<Purchase> queryPurchases(BillingClient billingClient) {
        Purchase.PurchasesResult purchasesResult = billingClient.queryPurchases(BillingClient.SkuType.INAPP);
        return purchasesResult.getPurchasesList();
    }

    private void requestRemoteConfig() {
        GuideApplication.getFirebaseRemoteConfig()
                .fetchAndActivate()
                .addOnSuccessListener(this, aBoolean -> {
                    updatingDatabaseDialog.dismiss();

                    long localFirestoreVersion = GuideApplication.getPrefs().getLong(PreferenceField.FIRESTORE_DB_VERSION, 0);
                    long firestoreVersion = GuideApplication.getFirebaseRemoteConfig().getLong("firestore_version");
                    if (firestoreVersion != localFirestoreVersion) {
                        updatingDatabaseDialog.dismiss();
                        GuideApplication.downloadDatabase(firestoreVersion);
                        GuideApplication.getPrefs().edit().putBoolean(PreferenceField.HAS_APP_NEWS, true).apply();
                    }

                    String appVersionStore = GuideApplication.getFirebaseRemoteConfig().getString("app_version_store");
                    if (!appVersionStore.equals(BuildConfig.VERSION_NAME)) {
                        EventBus.getDefault().post(new ShowNeedUpdateEvent(appVersionStore));
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        System.out.println();
                    }
                })
                .addOnCompleteListener(this, new OnCompleteListener<Boolean>() {
                    @Override
                    public void onComplete(@NonNull Task<Boolean> task) {
                        System.out.println();
                    }
                })
        ;

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                fm.beginTransaction().hide(active).show(homeFragment).commit();
                active = homeFragment;
                return true;

            case R.id.navigation_rank:
                fm.beginTransaction().hide(active).show(rankFragment).commit();
                active = rankFragment;
                return true;

            case R.id.navigation_teams:
                fm.beginTransaction().hide(active).show(teamsFragment).commit();
                active = teamsFragment;
                return true;
            case R.id.navigation_basic_lists:
                fm.beginTransaction().hide(active).show(basicsFragment).commit();
                active = basicsFragment;
                return true;
            case R.id.navigation_guide:
                fm.beginTransaction().hide(active).show(guideFragment).commit();
                active = guideFragment;
                return true;
        }
        return false;
    };

    @Subscribe
    public void onEvent(HideUpdateDialogEvent e) {
        if (updatingDatabaseDialog != null) {
            updatingDatabaseDialog.dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        LocaleHelper.onAttach(this);
        setupBillingLibrary();
        EventBus.getDefault().register(this);
        if (GuideApplication.isADSBlocked()) {
            Appodeal.hide(this, Appodeal.BANNER_VIEW);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        LocaleHelper.onAttach(newBase);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private static long hour = 3600000;
    private static long minute = 60000;

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if (!GuideApplication.isADSBlocked()) {
            GuideApplication.addADS(this, R.id.ads_banner);
        }
    }

    //    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


//    FirebaseFirestore db = FirebaseFirestore.getInstance();


    public void onClickUpdates(View v) {
        Utils.openAppMarket(v.getContext());
    }

    public void onClickPatchNotes(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("04/07/2019");
        builder.setCancelable(true);
//        builder.setMessage(R.string.patch_notes);
        builder.setNeutralButton(R.string.btn_hide, (dialogInterface, i) -> dialogInterface.dismiss());
        builder.show();
    }


    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}