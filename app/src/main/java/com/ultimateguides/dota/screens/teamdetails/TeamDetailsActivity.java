package com.ultimateguides.dota.screens.teamdetails;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Source;
import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.FirestoreFactory;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.PreferenceField;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.AlliancesImages;
import com.ultimateguides.dota.binding.HeroImages;
import com.ultimateguides.dota.customviews.AllianceView;
import com.ultimateguides.dota.model.AllianceModel;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.model.firebase.AllianceData;
import com.ultimateguides.dota.model.firebase.HeroData;
import com.ultimateguides.dota.model.firebase.HeroMainData;
import com.ultimateguides.dota.model.firebase.HeroSkillsData;
import com.ultimateguides.dota.model.firebase.TeamModel;
import com.ultimateguides.dota.screens.main.basiclists.alliances.AlliancesListPresenter;
import com.ultimateguides.dota.screens.herodetails.HeroDetailsActivity;
import com.ultimateguides.dota.screens.main.basiclists.heroes.HeroesListPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TeamDetailsActivity extends AppCompatActivity {

    private View waiting;
    private TeamModel model;
    private HeroImages images = new HeroImages();
    private AlliancesImages images2 = new AlliancesImages();

    HeroesListPresenter presenterHeroes = new HeroesListPresenter();
    AlliancesListPresenter presenterAlliances = new AlliancesListPresenter("Classes!A2:F24");
    //    private List<HeroModelOLD> heroList = new ArrayList<>();
    private List<HeroModel> list = new ArrayList<>();
    List<AllianceModel> allianceList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initExtras();
        getSupportActionBar().setTitle(model.name);
        initViews(model);

        waiting = findViewById(R.id.waiting);
        waiting.setVisibility(View.VISIBLE);

        Task<QuerySnapshot> alliancesTask = FirestoreFactory.getAlliances()
                .get(Source.CACHE);

        Task<QuerySnapshot> heroesMainTask = FirestoreFactory.getHeroes()
                .get(Source.CACHE);

        Tasks.whenAllComplete(heroesMainTask)
                .addOnCompleteListener(task -> {
                    HeroImages avatars = new HeroImages();
                    waiting.setVisibility(View.GONE);
                    QuerySnapshot heroesMain = heroesMainTask.getResult();
//                    QuerySnapshot heroesStats = heroesStatsTask.getResult();
                    list.clear();
                    for (QueryDocumentSnapshot document : heroesMain) {
                        HeroData data = document.toObject(HeroData.class);
                        HeroModel model = new HeroModel(data.type, avatars.get(data.type));
                        model.update(data);
//                model.updateMain(data);
//                model.avatarRes = avatars.get(data.type);
//                for (QueryDocumentSnapshot docStats : heroesStats) {
//                    if (docStats.get("type").equals(data.type)) {
//                        HeroSkillsData stats = docStats.toObject(HeroSkillsData.class);
//                        model.updateStats(stats);
//                    }
//                }
                        list.add(model);
                    }

                    if (alliancesTask.isSuccessful()) {
                        allianceList.clear();
                        for (QueryDocumentSnapshot document : alliancesTask.getResult()) {
                            AllianceData data = document.toObject(AllianceData.class);
                            AllianceModel model = new AllianceModel(data);
                            allianceList.add(model);
                        }
                    } else {
                        Log.d("Firestore", "Update fails");
                    }
                    calculateBonuses();

                });

        GuideApplication.addADS(this, R.id.ads_banner);
    }

    private void initExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            model = (TeamModel) extras.getSerializable("model");
        } else {
            model = new TeamModel();
        }
    }

    private void initViews(TeamModel model) {
        TextView describeView = findViewById(R.id.tv_describe);
        ImageView hero1View1 = findViewById(R.id.iv_hero_1);
        ImageView hero1View2 = findViewById(R.id.iv_hero_2);
        ImageView hero1View3 = findViewById(R.id.iv_hero_3);
        ImageView hero1View4 = findViewById(R.id.iv_hero_4);
        ImageView hero1View5 = findViewById(R.id.iv_hero_5);
        ImageView hero1View6 = findViewById(R.id.iv_hero_6);
        ImageView hero1View7 = findViewById(R.id.iv_hero_7);
        ImageView hero1View8 = findViewById(R.id.iv_hero_8);
        ImageView hero1View9 = findViewById(R.id.iv_hero_9);
        ImageView hero1View10 = findViewById(R.id.iv_hero_10);

        describeView.setText(String.valueOf(model.longDescription));
        if (model.heroes.size() >= 1)
            hero1View1.setImageResource(images.get(model.heroes.get(0)));
        if (model.heroes.size() >= 2)
            hero1View2.setImageResource(images.get(model.heroes.get(1)));
        if (model.heroes.size() >= 3)
            hero1View3.setImageResource(images.get(model.heroes.get(2)));
        if (model.heroes.size() >= 4)
            hero1View4.setImageResource(images.get(model.heroes.get(3)));
        if (model.heroes.size() >= 5)
            hero1View5.setImageResource(images.get(model.heroes.get(4)));
        if (model.heroes.size() >= 6)
            hero1View6.setImageResource(images.get(model.heroes.get(5)));
        if (model.heroes.size() >= 7)
            hero1View7.setImageResource(images.get(model.heroes.get(6)));
        if (model.heroes.size() >= 8)
            hero1View8.setImageResource(images.get(model.heroes.get(7)));
        if (model.heroes.size() >= 9)
            hero1View9.setImageResource(images.get(model.heroes.get(8)));
        if (model.heroes.size() >= 10)
            hero1View10.setImageResource(images.get(model.heroes.get(9)));

        hero1View1.setOnClickListener(view -> onClickHero(model.heroes.get(0)));
        hero1View2.setOnClickListener(view -> onClickHero(model.heroes.get(1)));
        hero1View3.setOnClickListener(view -> onClickHero(model.heroes.get(2)));
        hero1View4.setOnClickListener(view -> onClickHero(model.heroes.get(3)));
        hero1View5.setOnClickListener(view -> onClickHero(model.heroes.get(4)));
        hero1View6.setOnClickListener(view -> onClickHero(model.heroes.get(5)));
        hero1View7.setOnClickListener(view -> onClickHero(model.heroes.get(6)));
        hero1View8.setOnClickListener(view -> onClickHero(model.heroes.get(7)));
        hero1View9.setOnClickListener(view -> onClickHero(model.heroes.get(8)));
        hero1View10.setOnClickListener(view -> onClickHero(model.heroes.get(9)));
    }

    private void calculateBonuses() {
        for (AllianceModel alliance : allianceList) {
            AllianceType allianceType = alliance.type;
            for (String heroType : model.heroes) {
                HeroModel hero = findHeroOfType(heroType);
                if (hero.alliance1 == allianceType
                        || hero.alliance2 == allianceType
                        || hero.alliance3 == allianceType) {
                    if (allianceBonuses.containsKey(allianceType)) {
                        int count = allianceBonuses.get(alliance.type);
                        allianceBonuses.put(allianceType, count + 1);
                    } else {
                        allianceBonuses.put(allianceType, 1);
                    }
                }
            }
        }

        //trim with zero effect
        for (AllianceModel alliance : allianceList) {
            AllianceType allianceType = alliance.type;
            int step = alliance.unitsPerStage;
            if (allianceBonuses.containsKey(allianceType)) {
                Integer bonusCount = allianceBonuses.get(allianceType);
                if (bonusCount < step) {
                    allianceBonuses.remove(allianceType);
                }
            }
        }

        LinearLayout placeholderBonuses = findViewById(R.id.placeholder_bonuses);
        for (AllianceType type : allianceBonuses.keySet()) {
            Integer countUnits = allianceBonuses.get(type);
            AllianceModel allianceItem = findAllianceOfType(type);
            int unitsPerStage = allianceItem.unitsPerStage;
            int completedStages = countUnits / unitsPerStage;
            if (completedStages > allianceItem.maxStages) {
                completedStages = allianceItem.maxStages;
            }

            AllianceView view = new AllianceView(TeamDetailsActivity.this);
            view.setAllianceIcon(images2.get(type));
            view.setStages(allianceItem.maxStages);
            view.setUnitsPerStage(allianceItem.unitsPerStage);
            view.setActiveCount(countUnits);
            view.setDescribe(completedStages == 0 ? "" : allianceItem.stages.get(completedStages - 1));
            view.updateView();
            placeholderBonuses.addView(view);
        }

        //------------------------
        int priceMin = 0;
        int priceMax = 0;
        for (String heroType : model.heroes) {
            long firestoreVersion = GuideApplication.getPrefs().getLong(PreferenceField.FIRESTORE_DB_VERSION, 0);
            Crashlytics.setInt("teamdetails_heroes_size", list.size());
            Crashlytics.setString("teamdetails_hero", heroType);
            Crashlytics.setLong("firestore_version", firestoreVersion);
            HeroModel hero = findHeroOfType(heroType);
            priceMin += hero.goldCost;
            priceMax += (hero.goldCost * 3);
        }

        TextView priceMinView = findViewById(R.id.tv_price_min);
        TextView priceMaxView = findViewById(R.id.tv_price_max);
        priceMinView.setText(String.valueOf(priceMin));
        priceMaxView.setText(String.valueOf(priceMax));
    }

    HashMap<AllianceType, Integer> allianceBonuses = new HashMap<>();

    private void openHeroDetails(HeroModel model) {
        Intent i = new Intent(this, HeroDetailsActivity.class);
        i.putExtra("model", model);
        startActivity(i);
    }

    private HeroModel findHeroOfType(String type) {
        if (list.size() > 0) {
            for (HeroModel model : list) {
                if (model.type.equals(type)) {
                    return model;
                }
            }
        }
        return null;
    }

    private AllianceModel findAllianceOfType(AllianceType type) {
        if (allianceList.size() > 0) {
            for (AllianceModel model : allianceList) {
                if (model.type == type) {
                    return model;
                }
            }
        }
        return null;
    }

    private void onClickHero(String type) {
        if (list.size() > 0) {
            for (HeroModel modelItem : list) {
                if (modelItem.type.equals(type)) {
                    openHeroDetails(modelItem);
                }
            }
        } else {
            Log.d("TAG", "Empty heroes allianceList");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
