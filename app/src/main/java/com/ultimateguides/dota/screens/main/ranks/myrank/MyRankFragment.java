package com.ultimateguides.dota.screens.main.ranks.myrank;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.transition.TransitionManager;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.transitionseverywhere.ChangeText;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.PreferenceField;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.steamApi.MMRModel;
import com.ultimateguides.dota.model.html.RankModel;
import com.ultimateguides.dota.model.steamApi.SeasonPassModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MyRankFragment extends Fragment {

    ViewGroup waiting;
    WebView webView;

    List<RankModel> data = new ArrayList<>();
    List<Entry> entries = new ArrayList<>();
    LineChart chart;
    private ViewGroup matchDetailsHolder;
    private ViewGroup rootView;
    private ProgressBar progressBar;

    SteamWebviewClient client;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_my_rank, container, false);

        rootView.findViewById(R.id.btn_info).setOnClickListener(this::onClickInfo);
        rootView.findViewById(R.id.btn_logout).setOnClickListener(this::onClickLogout);
        rootView.findViewById(R.id.update).setOnClickListener(this::onClickReload);
//        rootView.findViewById(R.id.btn_hide_loader).setOnClickListener(this::onClickHide);

        waiting = rootView.findViewById(R.id.waiting);
        webView = rootView.findViewById(R.id.webView);
        progressBar = rootView.findViewById(R.id.progressBar);
        matchDetailsHolder = rootView.findViewById(R.id.match_details);

        matchDetailsHolder.setVisibility(View.GONE);
        webView.setVisibility(View.INVISIBLE);
        waiting.setVisibility(View.VISIBLE);
        webView.addJavascriptInterface(new JSCallback(), "Android");
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);

        client = new SteamWebviewClient(progressBar, waiting);
        client.setActivity(getActivity());

        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.setWebViewClient(client);
        WebView.setWebContentsDebuggingEnabled(false);
        webView.loadUrl("https://steamcommunity.com/login");

        chart = rootView.findViewById(R.id.chart);
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                matchDetailsHolder.setVisibility(View.VISIBLE);
                RankModel model = data.get((int) e.getX());
                TextView rankView = rootView.findViewById(R.id.tv_rank);
                TextView dateView = rootView.findViewById(R.id.tv_date);
                TextView lengthView = rootView.findViewById(R.id.tv_length);
                TextView roundsSurvivedView = rootView.findViewById(R.id.tv_rounds_survived);
                TextView roundsTotalView = rootView.findViewById(R.id.tv_rounds_total);

                TransitionManager.beginDelayedTransition(matchDetailsHolder,
                        new ChangeText().setChangeBehavior(ChangeText.CHANGE_BEHAVIOR_IN));
                rankView.setText(String.valueOf(model.FinalRank));

                dateView.setText(String.valueOf(model.StartTime)
                        .replace(" GMT", "")
                        .replace(" ", "\n"));
                lengthView.setText(String.valueOf(model.MatchLength / 60));
                roundsSurvivedView.setText(String.valueOf(model.RoundsSurvived));
                roundsTotalView.setText(String.valueOf(model.MatchRounds));
            }

            @Override
            public void onNothingSelected() {
                matchDetailsHolder.setVisibility(View.VISIBLE);
            }
        });
        return rootView;
    }

    public void onClickHide(View v) {
        waiting.setVisibility(View.GONE);
    }

    public void onClickLogout(View v) {
        waiting.setVisibility(View.VISIBLE);
        CookieManager.getInstance().removeAllCookies(value ->
        {
            client.clearLogs();
            webView.loadUrl("https://steamcommunity.com/login");
        });
        CookieManager.getInstance().flush();
    }

    public void onClickReload(View v) {
        waiting.setVisibility(View.VISIBLE);
        webView.loadUrl("https://steamcommunity.com/login");
    }

    public void onClickCopyLogs(View v) {
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("logs", client.getLogs());
        clipboard.setPrimaryClip(clip);
    }

    public void onClickInfo(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("FAQ");
        builder.setCancelable(true);
        String content = getResources().getString(R.string.faq_ranking);
        builder.setMessage(HtmlCompat.fromHtml(content, HtmlCompat.FROM_HTML_MODE_LEGACY));
        builder.setNeutralButton(R.string.btn_hide, (dialogInterface, i) -> dialogInterface.dismiss());
        builder.show();
    }

    private LineData getData(int count, List<RankModel> models) {
        ArrayList<Entry> values = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            values.add(new Entry(i, models.get(i).FinalRank));
        }
//        Collections.reverse(values);
        LineDataSet set1 = new LineDataSet(values, "DataSet 1");
        // set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);
        set1.setLineWidth(1.25f);
        set1.setCircleRadius(5f);
        set1.setCircleHoleRadius(2.5f);
        set1.setColor(getResources().getColor(R.color.palette1));
        set1.setCircleColor(Color.WHITE);
        set1.setValueTextColor(getResources().getColor(R.color.tier_1));
//        set1.setTypeface(textView.getTypeface(), Typeface.BOLD_ITALIC)
//        set1.setHighLightColor(getResources().getColor(R.color.tier_1));
        set1.setDrawValues(true);
        set1.setValueTextSize(18f);
        return new LineData(set1);
    }

    private void setupChart(LineChart chart, LineData data, int color) {
        ((LineDataSet) data.getDataSetByIndex(0)).setCircleHoleColor(color);
        chart.setData(data);
        chart.setBackgroundColor(color);
        chart.animateX(1500);
    }

    private void updateOverall() {
        TextView overallGamesView = rootView.findViewById(R.id.tv_games_played);
        TextView averageRatingView = rootView.findViewById(R.id.tv_avarage_rating);
        TextView top3View = rootView.findViewById(R.id.tv_top_3);
        TextView winCountView = rootView.findViewById(R.id.tv_win_1);

        int overallGames = data.size();
        int overallRate = 0;
        int top3Games = 0;
        int winGames = 0;
        overallGamesView.setText(String.valueOf(overallGames));
        for (RankModel model : data) {
            overallRate += model.FinalRank;
            if (model.FinalRank <= 3) {
                top3Games++;
            }
            if (model.FinalRank == 1) {
                winGames++;
            }
        }

        float averageRate = overallRate / (overallGames * 1f);
        averageRatingView.setText(String.format(Locale.US, "%.2f", averageRate));

        float top3Rate = top3Games / (overallGames * 1f);
        top3Rate *= 100;
        top3View.setText(String.format(Locale.US, "%.1f", top3Rate) + "%");
        winCountView.setText(String.valueOf(winGames));
    }

    private void updateMMR(MMRModel model) {
        GuideApplication.getPrefs().edit()
                .putString(PreferenceField.USERNAME, model.accountId)
                .apply();

        TextView userView = rootView.findViewById(R.id.tv_user);
        TextView mmrView = rootView.findViewById(R.id.tv_mmr);
        userView.setText(model.accountId);
        mmrView.setText(String.valueOf(model.mmr));

    }

    private void updateSeasonPass(ArrayList<SeasonPassModel> array) {
        boolean isEmpty = array == null || array.size() == 0;
        TextView levelView = rootView.findViewById(R.id.tv_protopass_level);

        int level = isEmpty ? 1 : array.size() + 1;
        levelView.setText(String.valueOf(level));
    }

    public class JSCallback {

        @JavascriptInterface
        public void onData(String value) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Type listType = new TypeToken<ArrayList<RankModel>>() {
            }.getType();
            ArrayList<RankModel> array = gson.fromJson(value, listType);
            data.clear();
            data.addAll(array);

            for (int i = 0; i < array.size(); i++) {
                entries.add(new Entry(i, array.get(i).FinalRank));
            }

            getActivity().runOnUiThread(() -> {
                //btnShare.setEnabled(true);
//                waiting.setVisibility(View.GONE);
                LineData data = getData(15, array);
                data.setValueFormatter(new IntegerFormatter());
                setupChart(chart, data, getResources().getColor(R.color.palette3));
                chart.animateY(500);
                chart.highlightValue(0, 0);
                updateOverall();

                String url = "https://steamcommunity.com/profiles/PROFILE_ID/gcpd/1046930?category=Account&tab=GameAccountClient";
                String steamId = GuideApplication.getPrefs().getString(PreferenceField.STEAM_ID, "");
                webView.loadUrl(url.replace("PROFILE_ID", steamId));
            });
        }

        @JavascriptInterface
        public void onMMR(String value) {
            Gson gson = new GsonBuilder().create();
            MMRModel model = gson.fromJson(value, MMRModel.class);

            getActivity().runOnUiThread(() -> {
                updateMMR(model);
// //TODO                webView.setVisibility(View.GONE);
//                waiting.setVisibility(View.GONE);

                String url = "https://steamcommunity.com/profiles/PROFILE_ID/gcpd/1046930?category=Event&tab=EventClaim";
                String steamId = GuideApplication.getPrefs().getString(PreferenceField.STEAM_ID, "");
                webView.loadUrl(url.replace("PROFILE_ID", steamId));
            });
        }

        @JavascriptInterface
        public void onSeasonPass(String value) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Type listType = new TypeToken<ArrayList<SeasonPassModel>>() {
            }.getType();
            ArrayList<SeasonPassModel> array = gson.fromJson(value, listType);

            getActivity().runOnUiThread(() -> {
                updateSeasonPass(array);
                webView.setVisibility(View.INVISIBLE);
                waiting.setVisibility(View.GONE);
            });
        }

    }
}