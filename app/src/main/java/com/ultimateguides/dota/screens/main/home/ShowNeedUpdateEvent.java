package com.ultimateguides.dota.screens.main.home;

public class ShowNeedUpdateEvent {
    public final String appVersionStore;

    public ShowNeedUpdateEvent(String appVersionStore) {
        this.appVersionStore = appVersionStore;
    }
}
