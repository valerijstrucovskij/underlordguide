package com.ultimateguides.dota.screens.main.ranks.leaderboard;

import android.content.res.Resources;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;

import java.io.InputStream;

public class LeaderboardWebviewClient extends WebViewClient {
//    @Override
//    public boolean shouldOverrideUrlLoading(WebView view, String url) {
//        return super.shouldOverrideUrlLoading(view, url);
//    }
//
////TODO pre lolli
//
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    @Override
//    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
//        if (request.getUrl().toString().endsWith("leaderboard")) {
//            view.setVisibility(View.GONE);
//
//
//            //view.loadUrl("https://steamcommunity.com/profiles/76561198043931277/gcpd/1046930?category=Account&tab=GameAccountClient");
////            view.loadUrl("https://steamcommunity.com/profiles/76561198043931277/gcpd/1046930?category=Games&tab=Matches");
//        }
//        return super.shouldOverrideUrlLoading(view, request);
//    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (url.toLowerCase().endsWith("/leaderboard")) {
            executeJScript(view, loadRasInject(R.raw.inject_leaderboard));
        }
    }

    private String loadRasInject(int resId) {
        String data = null;
        try {
            Resources res = GuideApplication.getApp().getResources();
            InputStream in_s = res.openRawResource(resId);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            data = new String(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    private void executeJScript(WebView webView, String script) {
        webView.evaluateJavascript(script, s -> Log.d("TAG", "onReceiveValue: " + s));
    }
}
