package com.ultimateguides.dota.screens.main.ranks.leaderboard;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.steamApi.LeaderboardModel;

import java.util.List;

public class LeaderboardRecycleViewAdapter extends RecyclerView.Adapter<LeaderboardRecycleViewAdapter.ViewHolderItem> {

    List<LeaderboardModel> data;

    public LeaderboardRecycleViewAdapter(List<LeaderboardModel> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_leaderboard, parent, false);
        return new LeaderboardRecycleViewAdapter.ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderItem holder, int position) {
        LeaderboardModel dataItem = getItem(position);
        holder.rankView.setText(String.valueOf(dataItem.rank));
        holder.nameView.setText(String.valueOf(dataItem.name));

        Resources resources = holder.rankView.getContext().getResources();
        switch (position) {
            case 0:
                holder.nameView.setTextColor(resources.getColor(R.color.tier_5));
                break;
            case 1:
                holder.nameView.setTextColor(resources.getColor(R.color.tier_4));
                break;
            case 2:
                holder.nameView.setTextColor(resources.getColor(R.color.tier_3));
                break;
            case 3:
                holder.nameView.setTextColor(resources.getColor(R.color.tier_2));
                break;
            case 4:
                holder.nameView.setTextColor(resources.getColor(R.color.tier_1));
                break;
            default:
                holder.nameView.setTextColor(resources.getColor(android.R.color.white));
        }
    }

    private LeaderboardModel getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView rankView;
        TextView nameView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            rankView = itemView.findViewById(R.id.tv_rank);
            nameView = itemView.findViewById(R.id.tv_name);
        }
    }
}
