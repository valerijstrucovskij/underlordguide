package com.ultimateguides.dota.screens.main.ranks.myrank;

import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.text.DecimalFormat; 

public class IntegerFormatter extends ValueFormatter implements IValueFormatter {
    private DecimalFormat mFormat;

    public IntegerFormatter() {
        mFormat = new DecimalFormat("###,###,##0"); // use one decimal
    }

    @Override
    public String getFormattedValue(float value) {
        return mFormat.format(value);
    }
}