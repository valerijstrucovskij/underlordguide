package com.ultimateguides.dota.screens;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.gson.Gson;
import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.Config;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.model.AllianceModel;
import com.ultimateguides.dota.model.spreadsheet.ResponseAddionsModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public abstract class ParentPresenter<T> {

    protected final String TABLE_RANGE;
    protected final Sheets sheetsService;

    public ParentPresenter(String tableRange) {
        sheetsService = GuideApplication.getSheetService();
        TABLE_RANGE = tableRange;
    }

    public Single<ArrayList<T>> requestDataAsync() {
        return Single.fromCallable(() -> requestData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    protected abstract ArrayList<T> requestData();
}
