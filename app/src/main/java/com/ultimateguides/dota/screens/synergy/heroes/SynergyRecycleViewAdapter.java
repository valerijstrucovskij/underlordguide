package com.ultimateguides.dota.screens.synergy.heroes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.AlliancesImages;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.others.Action1;

import java.util.List;

public class SynergyRecycleViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<HeroModel> data;
    private Action1<Integer> onItemClick;
    AlliancesImages images = new AlliancesImages();

    public SynergyRecycleViewAdapter(List<HeroModel> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        FrameLayout v = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_hero_full, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HeroModel dataItem = getItem(position);
        ((ViewHolderItem) holder).imageView.setImageResource(dataItem.avatarRes);
        ((ViewHolderItem) holder).tierView.setText("" + dataItem.goldCost);
        ((ViewHolderItem) holder).nameView.setText("" + dataItem.name);
        ((ViewHolderItem) holder).alliance1.setImageResource(images.get(dataItem.alliance1));
        ((ViewHolderItem) holder).alliance2.setImageResource(images.get(dataItem.alliance2));
        ((ViewHolderItem) holder).alliance3.setImageResource(images.get(dataItem.alliance3));
        ((ViewHolderItem) holder).root.setOnClickListener(view -> onItemClick.call(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private HeroModel getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<Integer> onItemClick) {
        this.onItemClick = onItemClick;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        RoundedImageView imageView;
        ImageView alliance1;
        ImageView alliance2;
        ImageView alliance3;
        TextView tierView;
        TextView nameView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            imageView = itemView.findViewById(R.id.imageView);
            tierView = itemView.findViewById(R.id.tier);
            nameView = itemView.findViewById(R.id.tv_name);
            alliance1 = itemView.findViewById(R.id.iv_alliance_1);
            alliance2 = itemView.findViewById(R.id.iv_alliance_2);
            alliance3 = itemView.findViewById(R.id.iv_alliance_3);
        }
    }
}