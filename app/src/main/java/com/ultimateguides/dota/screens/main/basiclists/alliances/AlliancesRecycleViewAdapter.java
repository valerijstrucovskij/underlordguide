package com.ultimateguides.dota.screens.main.basiclists.alliances;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.AlliancesImages;
import com.ultimateguides.dota.model.AllianceModel;
import com.ultimateguides.dota.others.Action1;

import java.util.ArrayList;
import java.util.List;

public class AlliancesRecycleViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements Filterable {
    List<AllianceModel> data;
    private List<AllianceModel> filteredData;
    private Action1<AllianceType> onItemClick;
    AlliancesImages images = new AlliancesImages();

    public AlliancesRecycleViewAdapter(List<AllianceModel> data) {
        this.data = data;
        this.filteredData = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_alliance, parent, false);
        return new AlliancesRecycleViewAdapter.ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AllianceModel dataItem = getItem(position);

        int unitPerStage = dataItem.unitsPerStage;
        ViewHolderItem itemView = (ViewHolderItem) holder;
        itemView.descriptionHolderView1.setVisibility(dataItem.maxStages >= 1 ? View.VISIBLE : View.GONE);
        itemView.descriptionHolderView2.setVisibility(dataItem.maxStages >= 2 ? View.VISIBLE : View.GONE);
        itemView.descriptionHolderView3.setVisibility(dataItem.maxStages >= 3 ? View.VISIBLE : View.GONE);
        itemView.countView1.setText("" + unitPerStage);
        itemView.countView2.setText("" + (unitPerStage << 1));
        itemView.countView3.setText("" + (unitPerStage * 3));

        List<String> descriptStages = dataItem.stages;
        if (descriptStages.size() > 0) {
            itemView.descriptionView1.setText("" + dataItem.stages.get(0));
        }
        if (descriptStages.size() > 1) {
            itemView.descriptionView2.setText("" + dataItem.stages.get(1));
        }
        if (descriptStages.size() > 2) {
            itemView.descriptionView3.setText("" + dataItem.stages.get(2));
        }
        itemView.titleView.setText("" + dataItem.name);
        itemView.iconView.setImageResource(images.get(dataItem.type));
        itemView.root.setOnClickListener(view -> onItemClick.call(dataItem.type));
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    private AllianceModel getItem(int position) {
        return filteredData.get(position);
    }

    public void setOnItemClick(Action1<AllianceType> onItemClick) {
        this.onItemClick = onItemClick;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredData = data;
                } else {
                    List<AllianceModel> filteredList = new ArrayList<>();
                    for (AllianceModel row : data) {
                        if (row.type.toString().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filteredData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredData = (ArrayList<AllianceModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView titleView;
        ImageView iconView;
        ViewGroup descriptionHolderView1;
        ViewGroup descriptionHolderView2;
        ViewGroup descriptionHolderView3;
        TextView descriptionView1;
        TextView descriptionView2;
        TextView descriptionView3;
        TextView countView1;
        TextView countView2;
        TextView countView3;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            titleView = itemView.findViewById(R.id.tv_title);
            iconView = itemView.findViewById(R.id.iv_icon);
            descriptionHolderView1 = itemView.findViewById(R.id.description_1);
            descriptionHolderView2 = itemView.findViewById(R.id.description_2);
            descriptionHolderView3 = itemView.findViewById(R.id.description_3);
            descriptionView1 = itemView.findViewById(R.id.tv_description1);
            descriptionView2 = itemView.findViewById(R.id.tv_description2);
            descriptionView3 = itemView.findViewById(R.id.tv_description3);
            countView1 = itemView.findViewById(R.id.tv_count_1);
            countView2 = itemView.findViewById(R.id.tv_count_2);
            countView3 = itemView.findViewById(R.id.tv_count_3);
        }
    }
}
