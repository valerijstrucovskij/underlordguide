package com.ultimateguides.dota.screens.main.guides;

public class GuideModel {

    public String category;
    public String name;

    public GuideModel(String category, String name) {
        this.category = category;
        this.name = name;
    }

    public boolean isHeader() {
        return false;
    }
}
