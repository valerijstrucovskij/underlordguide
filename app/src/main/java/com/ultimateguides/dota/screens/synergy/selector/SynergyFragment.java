package com.ultimateguides.dota.screens.synergy.selector;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.screens.synergy.events.SynergyClearEvent;
import com.ultimateguides.dota.screens.synergy.events.SynergySelectedEvent;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.HashSet;

public class SynergyFragment extends Fragment {

    private ExpandableLayout expandableAlliances;
    private HashSet<String> filter;
    HashSet<Chip> chips = new HashSet<>();

    public SynergyFragment(HashSet<String> filter) {
        this.filter = filter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_synergy, container, false);

        ChipGroup chipGroup = rootView.findViewById(R.id.chipGroup);

        LinearLayout waiting = rootView.findViewById(R.id.waiting);
        rootView.findViewById(R.id.btn_clear_all).setOnClickListener(this::onClickClearAll);
        return rootView;
    }

    public void onClickChipAlliance(View v) {
        String text = ((Chip) v).getText().toString();
        if (filter.contains(text)) {
            filter.remove(text);
        } else {
            filter.add(text);
            chips.add((Chip) v);
        }
        EventBus.getDefault().post(new SynergySelectedEvent(v));
    }

    private void onClickClearAll(View v) {
        for (Chip next : chips) {
            next.setChecked(false);
        }
        filter.clear();
        EventBus.getDefault().post(new SynergyClearEvent(v));
    }

    private void toggleAllianceLayout(View v) {
        expandableAlliances.toggle();
    }
}