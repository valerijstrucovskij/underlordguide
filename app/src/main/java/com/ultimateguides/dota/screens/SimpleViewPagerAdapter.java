package com.ultimateguides.dota.screens;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class SimpleViewPagerAdapter extends FragmentStatePagerAdapter {

    private final Context context;
    private final int[] titles;
    private final ArrayList<Fragment> fragments;

    public SimpleViewPagerAdapter(FragmentManager fm, Context context, int[] titles, ArrayList<Fragment> fragments) {
        super(fm);
        this.context = context;
        this.titles = titles;
        this.fragments = fragments;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return context.getResources().getString(titles[position]);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return titles.length;
    }
}