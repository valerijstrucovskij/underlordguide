package com.ultimateguides.dota.screens.donation;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.google.android.material.button.MaterialButton;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.PreferenceField;
import com.ultimateguides.dota.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DonationActivity extends Activity {
    private BillingClient billingClient;

    private Map<String, SkuDetails> mSkuDetailsMap = new HashMap<>();
    public static final String SkuSmallId = "small_coffee";
    public static final String SkuMediumId = "medium_coffee";
    public static final String SkuLargeId = "large_coffee";
    private View placeholderButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_buy_coffee);
        createView();
        createBilling();
    }

    private void createView() {
        MaterialButton btnClose = findViewById(R.id.btn_not_now);
        placeholderButton = findViewById(R.id.placeholder_price);
        placeholderButton.setVisibility(View.GONE);
        btnClose.setOnClickListener(view -> finish());

        findViewById(R.id.btn_buy_1).setOnClickListener(this::onClickPay);
        findViewById(R.id.btn_buy_2).setOnClickListener(this::onClickPay);
        findViewById(R.id.btn_buy_3).setOnClickListener(this::onClickPay);

        TextView usernameView = findViewById(R.id.tv_username);
        String username = GuideApplication.getPrefs().getString(PreferenceField.USERNAME, "");
        usernameView.setText("" + username);
    }


    private void onClickPay(View v) {
        if (v.getId() == R.id.btn_buy_1) {
            launchBilling(SkuSmallId);
        }
        if (v.getId() == R.id.btn_buy_2) {
            launchBilling(SkuMediumId);
        }
        if (v.getId() == R.id.btn_buy_3) {
            launchBilling(SkuLargeId);
        }
    }

    private void payComplete(String purchaseToken) {
        GuideApplication.setDonate(true);
        GuideApplication.getPrefs().edit()
                .putBoolean(PreferenceField.DID_DONATION, true)
                .putLong(PreferenceField.TIME_LAST_REWARDED_VIDEO, Long.MAX_VALUE)
                .apply();

        ConsumeParams.Builder params = ConsumeParams.newBuilder();
        params.setPurchaseToken(purchaseToken);
        billingClient.consumeAsync(params.build(), (billingResult, purchaseToken1) -> {
            System.out.println();
        });
    }


    private void createBilling() {
        billingClient = BillingClient.newBuilder(this)
                .enablePendingPurchases()
                .setListener((billingResult, purchases) -> {
                    if (purchases != null && purchases.size() > 0)
                        payComplete(purchases.get(0).getPurchaseToken());
                })
                .build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    querySkuDetails();

                    //restore purchases
                    List<Purchase> purchasesList = queryPurchases();
                    for (int i = 0; i < purchasesList.size(); i++) {
                        String purchaseId = purchasesList.get(i).getSku();
                        String purchaseToken = purchasesList.get(i).getPurchaseToken();
                        payComplete(purchaseToken);
                    }
                } else {
                    FancyToast.makeText(DonationActivity.this,
                            "Sorry, something went wrong. Try later",
                            Toast.LENGTH_SHORT,
                            FancyToast.ERROR,
                            false
                    ).show();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                FancyToast.makeText(DonationActivity.this,
                        "Sorry, something went wrong. Try later",
                        Toast.LENGTH_SHORT,
                        FancyToast.ERROR, false
                ).show();
            }
        });
    }

    private List<Purchase> queryPurchases() {
        Purchase.PurchasesResult purchasesResult = billingClient.queryPurchases(BillingClient.SkuType.INAPP);
        return purchasesResult.getPurchasesList();
    }

    private void querySkuDetails() {
        SkuDetailsParams.Builder skuDetailsParamsBuilder = SkuDetailsParams.newBuilder();
        List<String> skuList = new ArrayList<>();
        skuList.add(SkuSmallId);
        skuList.add(SkuMediumId);
        skuList.add(SkuLargeId);
        skuDetailsParamsBuilder.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        billingClient.querySkuDetailsAsync(skuDetailsParamsBuilder.build(), (billingResult, skuDetailsList) -> {
            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                for (SkuDetails skuDetails : skuDetailsList) {
                    mSkuDetailsMap.put(skuDetails.getSku(), skuDetails);

                }
                placeholderButton.setVisibility(View.VISIBLE);
            }
        });
    }

    public void launchBilling(String skuId) {
        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(mSkuDetailsMap.get(skuId))
                .build();
        billingClient.launchBillingFlow(this, billingFlowParams);
    }
}
