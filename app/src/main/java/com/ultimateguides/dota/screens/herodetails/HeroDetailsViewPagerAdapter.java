package com.ultimateguides.dota.screens.herodetails;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.viewpager.widget.PagerAdapter;

import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.Source;
import com.ultimateguides.dota.FirestoreFactory;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.binding.ItemsImages;
import com.ultimateguides.dota.model.HeroModel;
import com.ultimateguides.dota.model.ItemModel;
import com.ultimateguides.dota.model.firebase.ItemsData;
import com.ultimateguides.dota.screens.main.basiclists.items.ItemsRecycleViewAdapter;

import java.util.ArrayList;
import java.util.List;

import static androidx.recyclerview.widget.StaggeredGridLayoutManager.VERTICAL;

public class HeroDetailsViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private List<String> titles;
    private final HeroModel model;

    public HeroDetailsViewPagerAdapter(Context context, List<String> titles, HeroModel model) {
        mContext = context;
        this.titles = titles;
        this.model = model;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        switch (position) {
            case 0:
                return instantiateStats(container);
            case 1:
                return instantiateSkills(container);
            case 2:
                return instantiateItems(container);
        }
        return null;
    }

    private View instantiateStats(ViewGroup container) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.content_hero_details_stats, container, false);
        TextView healthView = view.findViewById(R.id.tv_health);
        TextView manaView = view.findViewById(R.id.tv_mana);
        TextView dpsView = view.findViewById(R.id.tv_dps);
        TextView damageView = view.findViewById(R.id.tv_damage);
        TextView attackSpeedView = view.findViewById(R.id.tv_attack_speed);
        TextView movementSpeedView = view.findViewById(R.id.tv_movement_speed);
        TextView attackRangeView = view.findViewById(R.id.tv_attack_range);
        TextView magicResistView = view.findViewById(R.id.tv_magic_resist);
        TextView armorView = view.findViewById(R.id.tv_armor);

        //        healthView.setText(String.format("%s/%s/%s", model.health., st2.health, st3.health));
        healthView.setText(String.format("%s", model.health));
        manaView.setText(String.format("%s", model.maxMana));
//        dpsView.setText(String.format("%s/%s/%s", st1.dps, st2.dps, st3.dps));
        dpsView.setText(String.format("%s", model.dps));
        damageView.setText(String.format("%s-%s/%s-%s/%s-%s",
                model.damageMin.get(0), model.damageMax.get(0),
                model.damageMin.get(1), model.damageMax.get(1),
                model.damageMin.get(2), model.damageMax.get(2)));
//        attackSpeedView.setText(String.format("%s/%s/%s", st1.attackSpeed, st2.attackSpeed, st3.attackSpeed));
        attackSpeedView.setText(String.format("%s", model.attackRate));
        movementSpeedView.setText(String.format("%s", model.movespeed));
        attackRangeView.setText(String.format("%s", model.attackRange));
        magicResistView.setText(String.format("%s", model.magicResist));
        armorView.setText(String.format("%s", model.armor));

        container.addView(view);
        return view;
    }

    private View instantiateSkills(ViewGroup container) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.content_hero_details_skills, container, false);

        TextView skillName1 = view.findViewById(R.id.tv_skill_name_1);
        TextView skillDescription1 = view.findViewById(R.id.tv_skill_1);
        TextView skillCooldown1 = view.findViewById(R.id.tv_skill_cooldown_1);
        TextView skillName2 = view.findViewById(R.id.tv_skill_name_2);
        TextView skillDescription2 = view.findViewById(R.id.tv_skill_2);
        TextView skillCooldown2 = view.findViewById(R.id.tv_skill_cooldown_2);
        ImageView skillIcon2 = view.findViewById(R.id.iv_skill_2);

        if (TextUtils.isEmpty(model.abilityName2)) {
            skillName2.setVisibility(View.GONE);
            skillDescription2.setVisibility(View.GONE);
            skillCooldown2.setVisibility(View.GONE);
            skillIcon2.setVisibility(View.GONE);
        }

        TextView aceLabel = view.findViewById(R.id.tv_ace_effect_label);
        TextView aceSkill = view.findViewById(R.id.tv_ace_effect);

        if (TextUtils.isEmpty(model.aceEffect)) {
            aceLabel.setVisibility(View.GONE);
            aceSkill.setVisibility(View.GONE);
        } else {
            aceSkill.setText(model.aceEffect);
        }

        skillName1.setText(model.abilityName1);
        skillDescription1.setText(model.abilityDesctiption1);
        skillCooldown1.setText(model.cooldown1);
        skillName2.setText(model.abilityName2);
        skillDescription2.setText(model.abilityDesctiption2);
        skillCooldown2.setText(model.cooldown2);

        container.addView(view);
        return view;
    }

    private List<ItemModel> list = new ArrayList<>();
    private ItemsRecycleViewAdapter adapter = new ItemsRecycleViewAdapter(list);

    private View instantiateItems(ViewGroup container) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.content_hero_details_items, container, false);
        RecyclerView recycleView = view.findViewById(R.id.recycleView);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1, VERTICAL);
        recycleView.setLayoutManager(layoutManager);
        recycleView.setAdapter(adapter);

        FirestoreFactory.getItems()
                .orderBy("tier", Query.Direction.ASCENDING)
                .get(Source.CACHE)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        list.clear();
                        List<ItemModel> fullList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            ItemsData data = document.toObject(ItemsData.class);
                            if (data.id == 10563 //barricade
                                    || data.id == 10558 //TODO next update; assasins
                                    || data.id == 10548 //TODO next update: beasts
                                    || data.id == 10525 //extra damage
                                    || data.id == 10526 //gold on defeat
                                    || data.id == 10527 //free first roll
                                    || data.id == 10565 //target buddy
                                    || data.id == 10562 //healing ward
                                    || data.id == 10560 //recruiter
                                    || data.id == 10564 //tombstone
                                    || data.id == 10008 //outdated
                                    || data.id == 10531 //TODO next update; bonus_damage_low_life
                                    || data.id == 10548) continue;
//                            if (data.tier != tier) {
//                                list.add(new ItemModel(data.tier));
//                                tier = data.tier;
//
//                            }
                            ItemModel model = new ItemModel(data);
                            fullList.add(model);
                        }

                        if (model.items != null) {
                            for (String itemId : model.items) {
                                for (ItemModel itemModel : fullList) {
                                    if (itemId.equals(itemModel.icon)) {
                                        list.add(itemModel);
                                    }
                                }
                            }
                        }

                        adapter.notifyDataSetChanged();

                    } else {
                        Log.d("Firestore", "Update fails");
                    }
                });

        container.addView(view);
        return view;
    }
}