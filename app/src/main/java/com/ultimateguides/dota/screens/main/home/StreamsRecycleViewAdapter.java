package com.ultimateguides.dota.screens.main.home;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.twitch.TwitchVideoModel;
import com.ultimateguides.dota.others.Action1;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.ultimateguides.dota.model.steamApi.NewsSteamModel.AppnewsBean.NewsitemsBean;

public class StreamsRecycleViewAdapter extends RecyclerView.Adapter<StreamsRecycleViewAdapter.ViewHolderItem> {

    List<TwitchVideoModel> data;
    private Action1<String> onItemClick;

    public StreamsRecycleViewAdapter(List<TwitchVideoModel> data) {
        this.data = data;
    }

    @Override
    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_twitch_stream, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderItem holder, int position) {
        TwitchVideoModel dataItem = getItem(position);
        holder.userView.setText(dataItem.userName + " [" + dataItem.viewerCount + "]");
        holder.viewersView.setText("" + dataItem.viewerCount);

        String url = dataItem.thumbnailUrl.replace("{width}", "150");
        url = url.replace("{height}", "100");
        Glide.with(holder.userView.getContext())
                .load(url)
                .into(holder.previewView);
        holder.root.setOnClickListener(view -> onItemClick.call("https://m.twitch.tv/" + dataItem.userName));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private TwitchVideoModel getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<String> onItemClick) {
        this.onItemClick = onItemClick;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView userView;
        ImageView previewView;
        TextView viewersView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            userView = itemView.findViewById(R.id.tv_user_name);
            viewersView = itemView.findViewById(R.id.tv_viewers_count);
            previewView = itemView.findViewById(R.id.iv_preview);
        }
    }
}
