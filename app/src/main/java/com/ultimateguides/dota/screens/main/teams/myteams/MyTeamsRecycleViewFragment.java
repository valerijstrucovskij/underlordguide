package com.ultimateguides.dota.screens.main.teams.myteams;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.customviews.LoadingView;
import com.ultimateguides.dota.database.AppDatabase;
import com.ultimateguides.dota.database.TeamsDao;
import com.ultimateguides.dota.database.TeamEntity;
import com.ultimateguides.dota.screens.builder.TeamBuilderActivity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MyTeamsRecycleViewFragment extends Fragment {

    private List<TeamEntity> dataList = new ArrayList<>();
    private MyTeamsViewAdapter adapter = new MyTeamsViewAdapter(dataList);
    ViewGroup emptyList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_teams_my, container, false);

        //initViews
        LoadingView waiting = rootView.findViewById(R.id.waiting);
        emptyList = rootView.findViewById(R.id.empty_list);
        RecyclerView recyclerView = rootView.findViewById(R.id.rv_teams);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClick(param -> {
            Intent i = new Intent(getContext(), TeamBuilderActivity.class);
            i.putExtra("teamEntity", dataList.get(param));
            startActivity(i);
        });

        adapter.setOnDeleteTeam(param -> {
            removeFromLocalDB(param)
                    .subscribe(() -> {
                        dataList.remove(param);
                        adapter.notifyDataSetChanged();
                        updateEmptyList();
                    }, e -> System.out.println("RoomWithRx: " + e.getMessage()));
        });

        //subscribe to changes listener
        AppDatabase.getInstance().teamsDao()
                .getAllMyTeams2()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(entites -> {
                    dataList.clear();
                    dataList.addAll(entites);
                    adapter.notifyDataSetChanged();
                    updateEmptyList();
                    System.out.println("get all RoomWithRx: " + entites.size());
                });

        rootView.findViewById(R.id.fab_add_team).setOnClickListener(view -> {
            Intent i = new Intent(getContext(), TeamBuilderActivity.class);
            startActivity(i);
        });
        return rootView;
    }

    private void updateEmptyList() {
        if (dataList.size() == 0) {
            emptyList.setVisibility(View.VISIBLE);
        } else {
            emptyList.setVisibility(View.GONE);
        }
    }

    public Completable removeFromLocalDB(TeamEntity e) {
        TeamsDao dao = AppDatabase.getInstance().teamsDao();
        return Completable.fromCallable(() -> dao.deleteTeam(e))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void onResume() {
        super.onResume();
        GuideApplication.getAnalytics().setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());
    }
}