package com.ultimateguides.dota.screens.synergy;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.screens.SimpleViewPagerAdapter;
import com.ultimateguides.dota.screens.synergy.heroes.SynergyHeroesFragment;
import com.ultimateguides.dota.screens.synergy.selector.SynergyFragment;

import java.util.ArrayList;
import java.util.HashSet;

public class SynergyActivity extends AppCompatActivity {

    String[] titles = new String[]{"select", "heroes"};
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private SynergyFragment synergyFragment;
    private SynergyHeroesFragment synergy2Fragment;
    private HashSet<String> filter = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_synergy);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ViewPager viewPager = findViewById(R.id.viewPager);
        synergyFragment = new SynergyFragment(filter);
        synergy2Fragment = new SynergyHeroesFragment(filter);
        fragments.add(synergyFragment);
        fragments.add(synergy2Fragment);
//        SimpleViewPagerAdapter pagerAdapter = new SimpleViewPagerAdapter(getSupportFragmentManager(), titles, fragments);
//        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_synergy);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_synergy_2);

        GuideApplication.addADS(this, R.id.ads_banner);
    }

    public void onClickChipAlliance(View v) {
        synergyFragment.onClickChipAlliance(v);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
