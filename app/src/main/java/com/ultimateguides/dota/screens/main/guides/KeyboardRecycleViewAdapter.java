package com.ultimateguides.dota.screens.main.guides;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ultimateguides.dota.R;
import com.ultimateguides.dota.others.Action1;

import java.util.List;

public class KeyboardRecycleViewAdapter extends RecyclerView.Adapter<KeyboardRecycleViewAdapter.ViewHolderItem> {

    List<GuideModel> data;
    private Action1<Integer> onItemClick;

    public KeyboardRecycleViewAdapter(List<GuideModel> data) {
        this.data = data;
    }

    @Override
    public KeyboardRecycleViewAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_guide_keyboard, parent, false);
        return new ViewHolderItem(v);
    }

    @Override
    public void onBindViewHolder(KeyboardRecycleViewAdapter.ViewHolderItem holder, int position) {
        GuideModel dataItem = getItem(position);
        holder.nameView.setText(dataItem.name);
        holder.categoryView.setText(dataItem.category);
//        holder.root.setOnClickListener(view -> onItemClick.call(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private GuideModel getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClick(Action1<Integer> onItemClick) {
        this.onItemClick = onItemClick;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ViewGroup root;
        TextView categoryView;
        TextView nameView;

        public ViewHolderItem(View itemView) {
            super(itemView);
            root = (ViewGroup) itemView;
            categoryView = itemView.findViewById(R.id.tv_category);
            nameView = itemView.findViewById(R.id.tv_name);
        }
    }
}
