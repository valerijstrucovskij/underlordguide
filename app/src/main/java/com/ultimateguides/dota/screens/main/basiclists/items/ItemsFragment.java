package com.ultimateguides.dota.screens.main.basiclists.items;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.Source;
import com.ultimateguides.dota.FirestoreFactory;
import com.ultimateguides.dota.GuideApplication;
import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.ItemModel;
import com.ultimateguides.dota.model.firebase.ItemsData;
import com.ultimateguides.dota.screens.ParentListActivity;
import com.ultimateguides.dota.screens.main.basiclists.BasicsFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static androidx.recyclerview.widget.StaggeredGridLayoutManager.VERTICAL;

public class ItemsFragment extends Fragment {

    //    ItemsListPresenter presenter = new ItemsListPresenter("Items!A2:C69");
//    ItemsListPresenter presenter = new ItemsListPresenter();
    private List<ItemModel> list = new ArrayList<>();
    private ItemsRecycleViewAdapter adapter = new ItemsRecycleViewAdapter(list);
    private RecyclerView recycleView;
    private RecyclerView.SmoothScroller smoothScroller;
    private StaggeredGridLayoutManager layoutManager;
    private HashMap<Integer, Integer> headersPositions = new HashMap<>();
    private ViewGroup tierButtons;
    private ViewGroup rootView;

    public ItemsFragment() {
    }

    public static ItemsFragment newInstance() {
        ItemsFragment fragment = new ItemsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.activity_items, container, false);
        LinearLayout waiting = rootView.findViewById(R.id.waiting);
        layoutManager = new StaggeredGridLayoutManager(2, VERTICAL);
        recycleView = rootView.findViewById(R.id.recycleView);
        recycleView.setLayoutManager(layoutManager);
        recycleView.setAdapter(adapter);

        smoothScroller = new LinearSmoothScroller(getActivity()) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        SearchView searchView = rootView.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String text = newText;
                adapter.getFilter().filter(text);
                return false;
            }
        });

        tierButtons = rootView.findViewById(R.id.group);

        tierButtons.setVisibility(View.GONE);
        waiting.setVisibility(View.VISIBLE);
        FirestoreFactory.getItems()
                .orderBy("tier", Query.Direction.ASCENDING)
                .get(Source.CACHE)
                .addOnCompleteListener(task -> {
                    waiting.setVisibility(View.GONE);
                    if (task.isSuccessful()) {
                        list.clear();
                        int tier = 0;
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            ItemsData data = document.toObject(ItemsData.class);
                            if(data.id == 10563 //barricade
                                    || data.id == 10558 //TODO next update; assasins
                                    || data.id == 10548 //TODO next update: beasts
                                    || data.id == 10525 //extra damage
                                    || data.id == 10526 //gold on defeat
                                    || data.id == 10527 //free first roll
                                    || data.id == 10565 //target buddy
                                    || data.id == 10562 //healing ward
                                    || data.id == 10560 //recruiter
                                    || data.id == 10564 //tombstone
                                    || data.id == 10008 //outdated
                                    || data.id == 10531 //TODO next update; bonus_damage_low_life
                                    || data.id == 10548) continue;
                            if (data.tier != tier) {
                                list.add(new ItemModel(data.tier));
                                tier = data.tier;

                            }
                            ItemModel model = new ItemModel(data);
                            list.add(model);
                        }
                        adapter.notifyDataSetChanged();

                        int visibilityTierButtons = list.size() == 0 ? View.GONE : View.VISIBLE;
                        tierButtons.setVisibility(visibilityTierButtons);

                        int headerCount = 1;
                        for (int position = 0; position < list.size(); position++) {
                            if (list.get(position).isHeader) {
                                headersPositions.put(headerCount++, position);
                            }
                        }
                    } else {
                        Log.d("Firestore", "Update fails");
                    }
                });

        rootView.findViewById(R.id.btn_tier_1).setOnClickListener(this::onClickScrollToTier);
        rootView.findViewById(R.id.btn_tier_2).setOnClickListener(this::onClickScrollToTier);
        rootView.findViewById(R.id.btn_tier_3).setOnClickListener(this::onClickScrollToTier);
        rootView.findViewById(R.id.btn_tier_4).setOnClickListener(this::onClickScrollToTier);
        rootView.findViewById(R.id.btn_tier_5).setOnClickListener(this::onClickScrollToTier);
//        rootView.findViewById(R.id.btn_tier_allience).setOnClickListener(this::onClickScrollToTier);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        GuideApplication.getAnalytics().setCurrentScreen(getActivity(), this.getClass().getSimpleName(), this.getClass().getSimpleName());
    }

    public void onClickScrollToTier(View v) {
        int id = v.getId();
        int position = 0;
        switch (id) {
            case R.id.btn_tier_1: {
                position = headersPositions.get(1);
                break;
            }
            case R.id.btn_tier_2: {
                position = headersPositions.get(2);
                break;
            }
            case R.id.btn_tier_3: {
                position = headersPositions.get(3);
                break;
            }
            case R.id.btn_tier_4: {
                position = headersPositions.get(4);
                break;
            }
            case R.id.btn_tier_5: {
                position = headersPositions.get(5);
                break;
            }
//            case R.id.btn_tier_allience: {
//                position = headersPositions.get(6);
//                break;
//            }
            default:
                throw new IllegalStateException("Unexpected value: " + id);
        }
        smoothScroller.setTargetPosition(position);
        layoutManager.startSmoothScroll(smoothScroller);
    }
}