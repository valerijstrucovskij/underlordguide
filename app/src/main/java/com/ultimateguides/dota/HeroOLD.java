package com.ultimateguides.dota;

import androidx.annotation.Keep;
import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class HeroOLD {
    @Retention(RetentionPolicy.RUNTIME)
    @StringDef({
            IHeroType.Abaddon,
            IHeroType.Alchemist,
            IHeroType.AntiMage,
            IHeroType.ArcWarden,
            IHeroType.Axe,
            IHeroType.Batrider,
            IHeroType.Beastmaster,
            IHeroType.Bloodseeker,
            IHeroType.BountyHunter,
            IHeroType.ChaosKnight,
            IHeroType.Clockwerk,
            IHeroType.CrystalMaiden,
            IHeroType.Disruptor,
            IHeroType.Doom,
            IHeroType.DragonKnight,
            IHeroType.DrowRanger,
            IHeroType.Enchantress,
            IHeroType.Enigma,
            IHeroType.Gryocopter,
            IHeroType.Juggernaut,
            IHeroType.KeeperoftheLight,
            IHeroType.Kunkka,
            IHeroType.Lich,
            IHeroType.Lina,
            IHeroType.LoneDruid,
            IHeroType.Luna,
            IHeroType.Lycan,
            IHeroType.Medusa,
            IHeroType.Mirana,
            IHeroType.Morphling,
            IHeroType.NaturesProphet,
            IHeroType.Necrophos,
            IHeroType.OgreMagi,
            IHeroType.Omniknight,
            IHeroType.PhantomAssassin,
            IHeroType.Puck,
            IHeroType.Pudge,
            IHeroType.QueenofPain,
            IHeroType.Razor,
            IHeroType.SandKing,
            IHeroType.Shadowfiend,
            IHeroType.ShadowShaman,
            IHeroType.Slardar,
            IHeroType.Slark,
            IHeroType.Sniper,
            IHeroType.Techies,
            IHeroType.TemplarAssassin,
            IHeroType.Terrorblade,
            IHeroType.Tidehunter,
            IHeroType.Timbersaw,
            IHeroType.Tinker,
            IHeroType.Tiny,
            IHeroType.TreantProtector,
            IHeroType.TrollWarlord,
            IHeroType.Tusk,
            IHeroType.Venomancer,
            IHeroType.Viper,
            IHeroType.Warlock,
            IHeroType.Windranger,
            IHeroType.WitchDoctor})

            
    public @interface IHeroType {
        String Abaddon = "Abaddon",
                Alchemist = "Alchemist",
                AntiMage = "Anti mage",
                ArcWarden = "Arc Warden",
                Axe = "Axe",
                Batrider = "Batrider",
                Beastmaster = "Beastmaster",
                Bloodseeker = "Bloodseeker",
                BountyHunter = "Bounty hunter",
                ChaosKnight = "Chaos knight",
                Clockwerk = "Clockwerk",
                CrystalMaiden = "Crystal Maiden",
                Disruptor = "Disruptor",
                Doom = "Doom",
                DragonKnight = "dragon knight",
                DrowRanger = "Drow Ranger",
                Enchantress = "Enchantress",
                Enigma = "Enigma",
                Gryocopter = "Gryocopter",
                Juggernaut = "Juggernaut",
                KeeperoftheLight = "Keeper of the Light",
                Kunkka = "Kunkka",
                Lich = "Lich",
                Lina = "Lina",
                LoneDruid = "Lone druid",
                Luna = "Luna",
                Lycan = "Lycan",
                Medusa = "Medusa",
                Mirana = "Mirana",
                Morphling = "Morphling",
                NaturesProphet = "Nature's Prophet",
                Necrophos = "Necrophos",
                OgreMagi = "ogre Magi",
                Omniknight = "Omniknight",
                PhantomAssassin = "Phantom assassin",
                Puck = "Puck",
                Pudge = "Pudge",
                QueenofPain = "Queen of Pain",
                Razor = "Razor",
                SandKing = "Sand King",
                Shadowfiend = "Shadowfiend",
                ShadowShaman = "Shadow shaman",
                Slardar = "Slardar",
                Slark = "Slark",
                Sniper = "Sniper",
                Techies = "Techies",
                TemplarAssassin = "Templar assassin",
                Terrorblade = "Terrorblade",
                Tidehunter = "Tidehunter",
                Timbersaw = "Timbersaw",
                Tinker = "Tinker",
                Tiny = "Tiny",
                TreantProtector = "Treant Protector",
                TrollWarlord = "troll Warlord",
                Tusk = "Tusk",
                Venomancer = "Venomancer",
                Viper = "Viper",
                Warlock = "warlock",
                Windranger = "Windranger",
                WitchDoctor = "Witch Doctor";
    }

    @IHeroType
    private String type;

    @IHeroType
    public String getType() {
        return type;
    }

    public HeroOLD(@IHeroType String newType) {
        this.type = newType;
    }
}
