package com.ultimateguides.dota.model.firebase;

import com.google.firebase.database.PropertyName;
import com.ultimateguides.dota.AllianceType;

import java.util.List;

public class ItemsData {

    @PropertyName("name")
    public String name;

    @PropertyName("description")
    public String description;

    @PropertyName("tier")
    public int tier;

    @PropertyName("id")
    public int id;

    @PropertyName("icon")
    public String icon;
}
