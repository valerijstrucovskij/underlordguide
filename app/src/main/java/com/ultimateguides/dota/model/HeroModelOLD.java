package com.ultimateguides.dota.model;

import android.text.TextUtils;

import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.HeroTypeOLD;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

public class HeroModelOLD implements Serializable {
    public final HeroTypeOLD type;
    public final Integer avatarRes;
    public int price;
    public String description;
    public String cooldown;
    public AllianceType alliance1;
    public AllianceType alliance2;
    public AllianceType alliance3 = AllianceType.None;

    public HeroStats stats;

    public HeroModelOLD(HeroTypeOLD type, Integer avatarRes) {
        this.type = type;
        this.avatarRes = avatarRes;
    }

    public void addAddions(List<String> model) {
        NumberFormat format = NumberFormat.getNumberInstance(Locale.US);
        try {
            price = format.parse(model.get(1)).intValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        alliance1 = AllianceType.valueOf(model.get(2));
        alliance2 = AllianceType.valueOf(model.get(3));
        if (model.size() > 4 && !TextUtils.isEmpty(model.get(4))){
            alliance3 = AllianceType.valueOf(model.get(4));
        }
        cooldown = model.get(5);
        description = model.get(6);
    }
}
