package com.ultimateguides.dota.model;

public class TierModel {
    public String level;
    public String tier1;
    public String tier2;
    public String tier3;
    public String tier4;
    public String tier5;

    
    public TierModel(String level, String tier1, String tier2, String tier3, String tier4, String tier5) {
        this.level = level;
        this.tier1 = tier1;
        this.tier2 = tier2;
        this.tier3 = tier3;
        this.tier4 = tier4;
        this.tier5 = tier5;
    }
}
