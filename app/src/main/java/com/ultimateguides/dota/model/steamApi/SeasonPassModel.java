package com.ultimateguides.dota.model.steamApi;

import com.google.gson.annotations.SerializedName;

public class SeasonPassModel {

    @SerializedName("EventID")
    public int eventID;

    @SerializedName("AccountID")
    public String accountId;

    @SerializedName("ClaimID")
    public int claimID;

    @SerializedName("TimesClaimed")
    public int timesClaimed;
    
}
