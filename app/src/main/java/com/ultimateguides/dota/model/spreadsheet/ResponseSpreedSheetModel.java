package com.ultimateguides.dota.model.spreadsheet;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseSpreedSheetModel {
    @SerializedName("majorDimension")
    public String majorDimension;
    @SerializedName("range")
    public String range;
    @SerializedName("values")
    public List<List<String>> values;
    
}
