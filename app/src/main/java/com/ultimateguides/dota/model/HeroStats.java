package com.ultimateguides.dota.model;

import com.ultimateguides.dota.model.spreadsheet.ResponseHeroesModel;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

public class HeroStats implements Serializable {

    public String name;
    public int mana;
    public int moveSpeed;
    public int attackRange;
    public SkillStage stage1;
    public SkillStage stage2;
    public SkillStage stage3;

    public HeroStats(List<String> statsRaw) {
        stage1 = new SkillStage();
        stage2 = new SkillStage();
        stage3 = new SkillStage();

        NumberFormat format = NumberFormat.getNumberInstance(Locale.US);

        name = statsRaw.get(0);
        try {
            stage1.health = format.parse(statsRaw.get(1)).intValue();
            stage2.health = format.parse(statsRaw.get(2)).intValue();
            stage3.health = format.parse(statsRaw.get(3)).intValue();
            if (!statsRaw.get(4).equals("-"))
                mana = format.parse(statsRaw.get(4)).intValue();
            stage1.dps = format.parse(statsRaw.get(5)).intValue();
            stage2.dps = format.parse(statsRaw.get(6)).intValue();
            stage3.dps = format.parse(statsRaw.get(7)).intValue();
            stage1.attackDamageMin = format.parse(statsRaw.get(8)).intValue();
            stage1.attackDamageMax = format.parse(statsRaw.get(9)).intValue();
            stage2.attackDamageMin = format.parse(statsRaw.get(10)).intValue();
            stage2.attackDamageMax = format.parse(statsRaw.get(11)).intValue();
            stage3.attackDamageMin = format.parse(statsRaw.get(12)).intValue();
            stage3.attackDamageMax = format.parse(statsRaw.get(13)).intValue();
            stage1.attackSpeed = format.parse(statsRaw.get(14)).floatValue();
            stage2.attackSpeed = format.parse(statsRaw.get(15)).floatValue();
            stage3.attackSpeed = format.parse(statsRaw.get(16)).floatValue();
            moveSpeed = format.parse(statsRaw.get(17)).intValue();
            attackRange = format.parse(statsRaw.get(18)).intValue();
            stage1.magicResist = format.parse(statsRaw.get(19)).intValue();
            stage2.magicResist = format.parse(statsRaw.get(20)).intValue();
            stage3.magicResist = format.parse(statsRaw.get(21)).intValue();
            stage1.armor = format.parse(statsRaw.get(22)).intValue();
            stage2.armor = format.parse(statsRaw.get(23)).intValue();
            stage3.armor = format.parse(statsRaw.get(24)).intValue();
//            stage1.healthRegen = format.parse(statsRaw.get(25)).intValue();
//            stage2.healthRegen = format.parse(statsRaw.get(26)).intValue();
//            stage3.healthRegen = format.parse(statsRaw.get(27)).intValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
