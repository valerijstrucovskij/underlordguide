package com.ultimateguides.dota.model.twitch;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TwitchVideoModel {

    @SerializedName("id")
    public String id;
    @SerializedName("user_id")
    public String userId;
    @SerializedName("user_name")
    public String userName;
    @SerializedName("game_id")
    public String gameId;
    @SerializedName("type")
    public String type;
    @SerializedName("title")
    public String title;
    @SerializedName("viewer_count")
    public int viewerCount;
    @SerializedName("started_at")
    public String startedAt;
    @SerializedName("language")
    public String language;
    @SerializedName("thumbnail_url")
    public String thumbnailUrl;
    @SerializedName("tag_ids")
    public List<String> tagIds;
}
