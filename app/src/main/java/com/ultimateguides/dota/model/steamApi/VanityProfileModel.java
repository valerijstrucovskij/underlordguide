package com.ultimateguides.dota.model.steamApi;

import com.google.gson.annotations.SerializedName;

public class VanityProfileModel {


    /**
     * response : {"steamid":"76561198043931277","success":1}
     */

    @SerializedName("response")
    public ResponseBean response;

    public static class ResponseBean {

        @SerializedName("steamid")
        public String steamid;
        @SerializedName("success")
        public int success;
    }
}
