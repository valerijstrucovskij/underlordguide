package com.ultimateguides.dota.model.firebase;

import android.text.TextUtils;

import com.google.firebase.database.PropertyName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CommunityTeamData implements Serializable {

    @PropertyName("username")
    public String username;

    @PropertyName("name")
    public String name;

    @PropertyName("notes")
    public String notes;

    @PropertyName("date")
    public String dateModified;

    @PropertyName("lord")
    public String lordId;

    @PropertyName("rating")
    public String rating = "0";

    //    @PropertyName("heroes")
    public List<String> heroes;
    public String key;

//    public int userRating;

    @PropertyName("heroes")
    public void setHeroesList(String heroesString) {
        String[] list = heroesString.split(";");
        heroes = new ArrayList<String>();

        for (int i = 0; i < list.length; i++) {
            String heroId = list[i].trim();
            if (!TextUtils.isEmpty(heroId)) {
                heroes.add(heroId);
            }
        }

    }
}
