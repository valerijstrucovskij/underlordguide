package com.ultimateguides.dota.model;

import android.text.TextUtils;

import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.model.firebase.AllianceData;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class AllianceModel {

    public String name;
    public AllianceType type;
    public int unitsPerStage;
    public int maxStages;
    public List<String> stages;

    public AllianceModel(List<String> alliance) {
        type = AllianceType.valueOf(alliance.get(0));
        NumberFormat format = NumberFormat.getNumberInstance(Locale.US);
        try {
            unitsPerStage = format.parse(alliance.get(1)).intValue();
            maxStages = format.parse(alliance.get(2)).intValue();
            stages = new ArrayList<>(maxStages);
            if (alliance.size() >= 4) {
                stages.add(alliance.get(3));
            }
            if (alliance.size() >= 5) {
                stages.add(alliance.get(4));
            }
            if (alliance.size() == 6 && !TextUtils.isEmpty(alliance.get(5))) {
                stages.add(alliance.get(5));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public AllianceModel(AllianceData data) {
        name = data.name;
        type = data.type;
        unitsPerStage = data.unitsPerStage;
        maxStages = data.maxStages;
        stages = new ArrayList<>();
        stages.addAll(data.stages);
    }
}
