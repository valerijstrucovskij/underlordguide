package com.ultimateguides.dota.model.spreadsheet;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ResponseAlliancesModel implements Serializable {

    /**
     * majorDimension : ROWS
     * range : Classes!A2:F24
     * values : [["assassin","3","3","Assassins gain a 10% chance to critical hit for 300% damage","Assassins gain a 20% chance to critical hit for 400% damage","Assassins gain a 30% chance to critical hit for 500% damage"],["Elusive","3","3","Elusive units gain +20% Evasion","Elusive units gain +45% Evasion","Elusive units gain +75% Evasion"],["demon","1","1","demon units gain +50% pure damage. Active when you have only one type of demon unit on the board"],["demonhunter","2","3","Invalidate your opponent's demon AllianceType bonus","Invalidate your opponent's demon AllianceType bonus. demon units gain +50% pure damage"],["human","2","3","human Units gain a 20% Chance to silence target for 4 seconds when attacking.","human Units gain a 44% Chance to silence target for 4 seconds when attacking.","human Units gain a 66% Chance to silence target for 4 seconds when attacking."],["savage","2","3","Allies gain +10% Attack Damage","Allies gain +25% Attack Damage","Allies gain +45% Attack Damage"],["Scaled","2","2","Allies gain +30% Magic Resistance","Allies gain +50% Magic Resistance"],["troll","2","2","troll units gain +35 Attack Speed","troll units gain +65 Attack Speed and other allies gain +30 Attack Speed"],["warlock","2","3","Allies gain +15% Lifesteal","Allies gain +30% Lifesteal","Allies gain +50% Lifesteal"],["brawny","2","2","All brawny units have their maximum HP increased by 200","All brawny units have their maximum HP increased by 500"],["Deadeye","2","1","Deadeye units focus their attacks on the lowest-health enemy"],["druid","2","2","The lowest star ally druid is upgraded a level","The 2 lowest star ally druid is upgraded a level each"],["Heartless","2","3","Enemies suffer -5 Armor","Enemies suffer -10 Armor","Enemies suffer -20 Armor"],["hunter","3","2","Hunters have a 15% chance of quickly performing 3 attacks.","Hunters have a 25% chance of quickly performing 3 attacks."],["knight","2","3","knight units take 25% less physical and magic damage when standing 1 cell away from another knight","knight units take 35% less physical and magic damage when standing 1 cell away from another knight","knight units take 50% less physical and magic damage when standing 1 cell away from another knight"],["mage","3","2","Enemies suffer -40% Magic Resistance","Enemies suffer -100% Magic Resistance"],["dragon","3","1","dragon units start combat with full mana"],["inventor","2","2","Inventors gain +15 HP Regeneration","Inventors gain +40 HP Regeneration"],["primordial","2","2","When hit, primordial units have a 30% chance to disarm melee attackers for 4 seconds","When hit, allies units have a 30% chance to disarm melee attackers for 4 seconds"],["shaman","2","1","A random Enemy is turned into a Frog for 6 seconds at the start of combat"],["bloodbound","2","1","When a Blood-Bound unit dies, all other blood-bound units +100% Attack Damage for the rest of the battle"],["warrior","3","3","Warriors gain +10 Armor","Warriors gain +15 Armor","Warriors gain +25 Armor"],["Scrappy","3","2","A Random ally is granted +15 Armor and +10 HP Regeneration","Allies gain +15 Armor and +10 HP Regeneration"]]
     */

     
    @SerializedName("majorDimension")
    public String majorDimension;
    @SerializedName("range")
    public String range;
    @SerializedName("values")
    public List<List<String>> values;
}
