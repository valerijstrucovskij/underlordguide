package com.ultimateguides.dota.model;

import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.HeroTypeOLD;
import com.ultimateguides.dota.model.firebase.HeroData;
import com.ultimateguides.dota.model.firebase.HeroMainData;
import com.ultimateguides.dota.model.firebase.HeroSkillsData;

import java.io.Serializable;
import java.util.List;

public class HeroModel implements Serializable {
    public String name;
    public String type;
    public Integer avatarRes;
    public String abilityName1;
    public String abilityDesctiption1;
    public String cooldown1;
    public String abilityName2;
    public String abilityDesctiption2;
    public String cooldown2;
    public String aceEffect;
    public AllianceType alliance1;
    public AllianceType alliance2;
    public AllianceType alliance3 = AllianceType.None;
    public List<String> items;
    //stats
    public int dps;
    public String armor;
    public int attackRange;
    public String attackRate;
    public String displayName;
    public int draftTier;
    public int goldCost;
    public int id;
    public String magicResist;
    public int maxMana;
    public String texturename;
    public int movespeed;
    public List<Integer> damageMin;
    public List<Integer> damageMax;
    public List<Integer> health;

    public HeroModel(String type, Integer avatarRes) {
        this.type = type;
        this.avatarRes = avatarRes;
    }

    public void update(HeroData data) {
        name = data.displayName;
        goldCost = data.goldCost;
        draftTier = data.draftTier;
        HeroData.HeroAbility ability1 = data.abilities.get(0);
        abilityName1 = ability1.name;
        abilityDesctiption1 = ability1.description;
        cooldown1 = ability1.cooldown;
        items = data.items;

        if (data.abilities.size() >= 2) {
            HeroData.HeroAbility ability2 = data.abilities.get(1);
            abilityName2 = ability2.name;
            abilityDesctiption2 = ability2.description;
            cooldown2 = ability2.cooldown;
        }

        alliance1 = data.alliances.get(0);
        alliance2 = data.alliances.get(1);
        if (data.alliances.size() >= 3) {
            alliance3 = data.alliances.get(2);
        }

        health = data.health;
        armor = data.armor + "";
        attackRange = data.attackRange;
        attackRate = data.attackRate + "";
        displayName = data.displayName;
        id = data.id;
        magicResist = data.magicResist + "";
        maxMana = data.maxmana;
        texturename = data.texturename;
        movespeed = data.movespeed;
        damageMin = data.damageMin;
        damageMax = data.damageMax;

        updateDps();

    }

    private void updateDps() {
        float attackRateInt = 1;
        if (attackRate.contains(",")) {
            String array = attackRate.split(",")[0];
            attackRateInt = Float.parseFloat(array);
        } else {
            attackRateInt = Float.parseFloat(attackRate);
        }
        dps = (int) (damageMin.get(0) * attackRateInt);
    }
}
