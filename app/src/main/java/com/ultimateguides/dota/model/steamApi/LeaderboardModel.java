package com.ultimateguides.dota.model.steamApi;

import com.google.gson.annotations.SerializedName;

public class LeaderboardModel {

    @SerializedName("rank")
    public int rank;

    @SerializedName("name")
    public String name;
    
}
