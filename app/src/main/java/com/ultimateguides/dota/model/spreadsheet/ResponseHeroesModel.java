package com.ultimateguides.dota.model.spreadsheet;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseHeroesModel {

    /**
     * spreadsheetId : 1b4WGrJ1oMGRV4qBnvUpzPxW6dXySZ_zb9ZUsPBxWDMw
     * valueRanges : [{"majorDimension":"ROWS","range":"'All Units'!A6:AB6","values":[["Abaddon","750","1,500","3,000","100","38","77","154","45","55","90","110","180","220","1.3","1.3","1.3","325","1","0","0","0","10","10","10","0","0","0"]]},{"majorDimension":"ROWS","range":"'All Units'!A7:AB7","values":[["Alchemist","1,000","2,000","4,000","100","60","120","240","50","70","100","140","200","280","1.0","1.0","1.0","305","1","0","0","0","5","5","5","1","2","4"]]}]
     */

    @SerializedName("majorDimension")
    public String majorDimension;
    @SerializedName("range")
    public String range;
    @SerializedName("values")
    public List<List<String>> stats;
    

//    public static class HeroRaw {
//        /**
//         * majorDimension : ROWS
//         * range : 'All Units'!A6:AB6
//         * values : [["Abaddon","750","1,500","3,000","100","38","77","154","45","55","90","110","180","220","1.3","1.3","1.3","325","1","0","0","0","10","10","10","0","0","0"]]
//         */
//
//        @SerializedName("majorDimension")
//        public String majorDimension;
//        @SerializedName("range")
//        public String range;
//        @SerializedName("values")
//        public List<List<String>> stats;
//    }
}
