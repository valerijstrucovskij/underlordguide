package com.ultimateguides.dota.model.firebase;

import com.google.firebase.database.PropertyName;
import com.ultimateguides.dota.HeroOLD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class TinderlordsData implements Serializable {

    public String key;

    @PropertyName("date")
    public String date;

    @PropertyName("discordUser")
    public String discordUser;

    @PropertyName("steamUser")
    public String steamUser;

    @PropertyName("lang")
    public String lang;

    @PropertyName("message")
    public String message;

    @PropertyName("ownerId")
    public String ownerId;

}
