package com.ultimateguides.dota.model.steamApi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsSteamModel {

    /**
     * appnews : {"appid":1046930,"newsitems":[{"gid":"2436926440564623897","title":"Have a Good knight","url":"https://steamstore-a.akamaihd.net/news/externalpost/steam_community_announcements/2436926440564623897","is_external_url":true,"author":"Finol","contents":"Based on community feedback, we've adjusted opponent selection weight to be even more biased against folks you've played recently. Adjusted AI pathfinding: Units will only try to jump into a beneficial aura's range if it's only 1 cell away (down from 2). Fixed bug that prevented Assassins from jumpi...","feedlabel":"Community Announcements","date":1561586635,"feedname":"steam_community_announcements","feed_type":1,"appid":1046930,"tags":["patchnotes"]},{"gid":"2436926440564425836","title":"Open Beta Release Day Update","url":"https://steamstore-a.akamaihd.net/news/externalpost/steam_community_announcements/2436926440564425836","is_external_url":true,"author":"Daniel Jennings","contents":"Improvements to Android performance. Fixed combined hero and item acquire notifications not rendering. Added Rank to the Dashboard. Adjusted Teleport FX. Added language selector to settings. More work has been done on the Season Info UI. We have solved the quit button disappearance. Someone took it ...","feedlabel":"Community Announcements","date":1561581899,"feedname":"steam_community_announcements","feed_type":1,"appid":1046930,"tags":["patchnotes"]},{"gid":"2436926440564425309","title":"Mid-Week Update","url":"https://steamstore-a.akamaihd.net/news/externalpost/steam_community_announcements/2436926440564425309","is_external_url":true,"author":"Daniel Jennings","contents":"Enabled Tutorial. More improvements to PC UI and styling. Tweaked default camera positions and angles. Also added sliders to the Game Settings UI to tweak these two further. Tweaked the time bodies stay on the board before sinking. More work on DPS meter design. Victorious units now celebrate for lo...","feedlabel":"Community Announcements","date":1561581887,"feedname":"steam_community_announcements","feed_type":1,"appid":1046930,"tags":["patchnotes"]}],"count":12}
     */

    @SerializedName("appnews")
    public AppnewsBean appnews;

    public static class AppnewsBean {
        /**
         * appid : 1046930
         * newsitems : [{"gid":"2436926440564623897","title":"Have a Good knight","url":"https://steamstore-a.akamaihd.net/news/externalpost/steam_community_announcements/2436926440564623897","is_external_url":true,"author":"Finol","contents":"Based on community feedback, we've adjusted opponent selection weight to be even more biased against folks you've played recently. Adjusted AI pathfinding: Units will only try to jump into a beneficial aura's range if it's only 1 cell away (down from 2). Fixed bug that prevented Assassins from jumpi...","feedlabel":"Community Announcements","date":1561586635,"feedname":"steam_community_announcements","feed_type":1,"appid":1046930,"tags":["patchnotes"]},{"gid":"2436926440564425836","title":"Open Beta Release Day Update","url":"https://steamstore-a.akamaihd.net/news/externalpost/steam_community_announcements/2436926440564425836","is_external_url":true,"author":"Daniel Jennings","contents":"Improvements to Android performance. Fixed combined hero and item acquire notifications not rendering. Added Rank to the Dashboard. Adjusted Teleport FX. Added language selector to settings. More work has been done on the Season Info UI. We have solved the quit button disappearance. Someone took it ...","feedlabel":"Community Announcements","date":1561581899,"feedname":"steam_community_announcements","feed_type":1,"appid":1046930,"tags":["patchnotes"]},{"gid":"2436926440564425309","title":"Mid-Week Update","url":"https://steamstore-a.akamaihd.net/news/externalpost/steam_community_announcements/2436926440564425309","is_external_url":true,"author":"Daniel Jennings","contents":"Enabled Tutorial. More improvements to PC UI and styling. Tweaked default camera positions and angles. Also added sliders to the Game Settings UI to tweak these two further. Tweaked the time bodies stay on the board before sinking. More work on DPS meter design. Victorious units now celebrate for lo...","feedlabel":"Community Announcements","date":1561581887,"feedname":"steam_community_announcements","feed_type":1,"appid":1046930,"tags":["patchnotes"]}]
         * count : 12
         */

        @SerializedName("appid")
        public int appid;
        @SerializedName("count")
        public int count;
        @SerializedName("newsitems")
        public List<NewsitemsBean> newsitems;

        public static class NewsitemsBean {
            /**
             * gid : 2436926440564623897
             * title : Have a Good knight
             * url : https://steamstore-a.akamaihd.net/news/externalpost/steam_community_announcements/2436926440564623897
             * is_external_url : true
             * author : Finol
             * contents : Based on community feedback, we've adjusted opponent selection weight to be even more biased against folks you've played recently. Adjusted AI pathfinding: Units will only try to jump into a beneficial aura's range if it's only 1 cell away (down from 2). Fixed bug that prevented Assassins from jumpi...
             * feedlabel : Community Announcements
             * date : 1561586635
             * feedname : steam_community_announcements
             * feed_type : 1
             * appid : 1046930
             * tags : ["patchnotes"]
             */

            @SerializedName("gid")
            public String gid;
            @SerializedName("title")
            public String title;
            @SerializedName("url")
            public String url;
            @SerializedName("is_external_url")
            public boolean isExternalUrl;
            @SerializedName("author")
            public String author;
            @SerializedName("contents")
            public String contents;
            @SerializedName("feedlabel")
            public String feedlabel;
            @SerializedName("date")
            public int date;
            @SerializedName("feedname")
            public String feedname;
            @SerializedName("feed_type")
            public int feedType;
            @SerializedName("appid")
            public int appid;
            @SerializedName("tags")
            public List<String> tags;
        }
    }
}
