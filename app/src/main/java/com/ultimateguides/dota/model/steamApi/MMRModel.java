package com.ultimateguides.dota.model.steamApi;

import com.google.gson.annotations.SerializedName;

public class MMRModel {

    @SerializedName("AccountID")
    public String accountId;

    @SerializedName("MMRLevel")
    public int mmr;
    
}
