package com.ultimateguides.dota.model.firebase;

import com.google.firebase.database.PropertyName;

import java.io.Serializable;

public class TeamRatingData implements Serializable {

    
    public String name;

    @PropertyName("userRating")
    public int userRating;
}
