package com.ultimateguides.dota.model.html;

import com.google.gson.annotations.SerializedName;

public class RankModel {
    /**
     * MatchID : 9762985
     * Mode : 1
     * MatchLength : 2601
     * StartTime : 2019-07-18 18:26:03 GMT
     * ServerVersion : 201
     * ClusterID : 138
     * MatchRounds : 37
     * PlayerSlot : 3
     * Team : 2
     * Flags : 0
     * PlayerState : 1
     * FinalRank : 7
     * SurvivalTime : 2000
     * Party : 0
     * RoundsSurvived : 28
     * Platform : 1
     */

    @SerializedName("MatchID")
    public int MatchID;
    @SerializedName("Mode")
    public int Mode;
    @SerializedName("MatchLength")
    public int MatchLength;
    @SerializedName("StartTime")
    public String StartTime;
    @SerializedName("ServerVersion")
    public int ServerVersion;
    @SerializedName("ClusterID")
    public int ClusterID;
    @SerializedName("MatchRounds")
    public int MatchRounds;
    @SerializedName("PlayerSlot")
    public int PlayerSlot;
    @SerializedName("Team")
    public int Team;
    @SerializedName("Flags")
    public int Flags;
    @SerializedName("PlayerState")
    public int PlayerState;
    @SerializedName("FinalRank")
    public int FinalRank;
    @SerializedName("SurvivalTime")
    public int SurvivalTime;
    @SerializedName("Party")
    public int Party;
    @SerializedName("RoundsSurvived")
    public int RoundsSurvived;
    @SerializedName("Platform")
    public int Platform;
}
