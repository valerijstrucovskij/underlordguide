package com.ultimateguides.dota.model.twitch;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TwitchDataModel<T> {

    @SerializedName("data")
    public List<T> data;
}
