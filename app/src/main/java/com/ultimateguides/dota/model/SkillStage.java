package com.ultimateguides.dota.model;

import java.io.Serializable;

public class SkillStage implements Serializable {
    public int health;
    public int dps;
    public int attackDamageMin;
    public int attackDamageMax;
    public float attackSpeed;
    public int magicResist;
    public int armor;
    public int moveSpeed;
    public int attackRange;
    public int mana;
}
