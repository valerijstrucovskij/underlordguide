package com.ultimateguides.dota.model.firebase;

import com.google.firebase.database.PropertyName;
import com.ultimateguides.dota.AllianceType;

import java.util.List;

public class AllianceData {

    @PropertyName("name")
    public String name;
    
    @PropertyName("type")
    public AllianceType type;

    @PropertyName("maxStages")
    public int maxStages;

    @PropertyName("unitsPerStage")
    public int unitsPerStage;

    @PropertyName("stages")
    public List<String> stages;
}
