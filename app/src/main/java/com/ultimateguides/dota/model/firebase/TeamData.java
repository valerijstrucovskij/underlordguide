package com.ultimateguides.dota.model.firebase;

import com.google.firebase.database.PropertyName;
import com.ultimateguides.dota.HeroOLD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class TeamData implements Serializable {

//    public String name;
    @PropertyName("description")
    public String describe;

    @PropertyName("heroes")
    public List<HeroOLD> heroes;

    public int userRating;

    @PropertyName("heroes")
    public void setHeroesList(List<String> list) {
        heroes = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            heroes.add(new HeroOLD(list.get(i)));
        }

    }
}
