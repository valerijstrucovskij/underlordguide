package com.ultimateguides.dota.model.firebase;

import com.google.firebase.database.PropertyName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TeamModel implements Serializable {

    
    @PropertyName("name")
    public String name = "";

    @PropertyName("badge")
    public String badge;

    @PropertyName("editor_rate")
    public String editorRate;

    @PropertyName("items")
    public String items;

    @PropertyName("long_description")
    public String longDescription;

    public int userRating;

    @PropertyName("modified_string")
    public String modified;

    @PropertyName("heroes")
    public List<String> heroes;

    @PropertyName("lordId")
    public String lordId;

//    public TeamModel() {
//        heroes = new ArrayList<>();
//    }
}
