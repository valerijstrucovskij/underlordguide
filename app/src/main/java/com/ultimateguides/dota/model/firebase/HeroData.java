package com.ultimateguides.dota.model.firebase;

import com.google.firebase.firestore.PropertyName;
import com.ultimateguides.dota.AllianceType;

import java.util.List;

public class HeroData {
//    @PropertyName("name")
//    public String name;

    @PropertyName("type")
    public String type;

    @PropertyName("alliances")
    public List<AllianceType> alliances;

    @PropertyName("abilities")
    public List<HeroAbility> abilities;
    @PropertyName("items")
    public List<String> items;

    //
    @PropertyName("armor")
    public Object armor;
    @PropertyName("attackRange")
    public int attackRange;
    @PropertyName("attackRate")
    public Object attackRate;
    @PropertyName("displayName")
    public String displayName;
    @PropertyName("draftTier")
    public int draftTier;
    @PropertyName("goldCost")
    public int goldCost;
    @PropertyName("id")
    public int id;
    @PropertyName("magicResist")
    public Object magicResist;
    @PropertyName("maxMana")
    public int maxmana;
    @PropertyName("texturename")
    public String texturename;
    @PropertyName("movespeed")
    public int movespeed;
    @PropertyName("damageMin")
    public List<Integer> damageMin;
    @PropertyName("damageMax")
    public List<Integer> damageMax;
    @PropertyName("health")
    public List<Integer> health;

    public static class HeroAbility {

//        public HeroAbility(){}

        @PropertyName("name")
        public String name;

        @PropertyName("description")
        public String description;

        @PropertyName("cooldown")
        public String cooldown;
    }
}
