package com.ultimateguides.dota.model;

import com.ultimateguides.dota.R;
import com.ultimateguides.dota.model.firebase.ItemsData;

import java.util.List;

public class ItemModel {
    public String name;
    public int tier;
    public String description;
    public boolean isHeader = false;
    public int headerColor = R.color.white;
    public String icon;

    public ItemModel(String itemTier) {
        name = "Tier " + itemTier;
        isHeader = true;
        switch (itemTier) {
            case "1": {
                headerColor = R.color.tier_1;
                break;
            }
            case "2": {
                headerColor = R.color.tier_2;
                break;
            }
            case "3": {
                headerColor = R.color.tier_3;
                break;
            }
            case "4": {
                headerColor = R.color.tier_4;
                break;
            }
            case "5": {
                headerColor = R.color.tier_5;
                break;
            }
            case "Alliances": {
                name = "" + itemTier;
                headerColor = R.color.tier_alliance;
                break;
            }
        }
    }

    public ItemModel(int itemTier) {
        name = "Tier " + itemTier;
        isHeader = true;
        switch (itemTier) {
            case 1: {
                headerColor = R.color.tier_1;
                break;
            }
            case 2: {
                headerColor = R.color.tier_2;
                break;
            }
            case 3: {
                headerColor = R.color.tier_3;
                break;
            }
            case 4: {
                headerColor = R.color.tier_4;
                break;
            }
            case 5: {
                headerColor = R.color.tier_5;
                break;
            }
        }
    }


    public ItemModel(ItemsData data) {
        this.name = data.name;
        this.tier = data.tier;
        this.description = data.description;
        this.icon = data.icon;
    }
}
