package com.ultimateguides.dota.model.firebase;

import android.text.TextUtils;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.PropertyName;
import com.ultimateguides.dota.AllianceType;

public class HeroMainData {
    @PropertyName("name")
    public String name;

    @PropertyName("type")
    public String type;

    @PropertyName("race1")
    public AllianceType race1;

    @PropertyName("race2")
    public AllianceType race2;

    @PropertyName("race3")
    public AllianceType race3;

    @PropertyName("abilityName1")
    public String abilityName1;

    @PropertyName("abilityDescription1")
    public String abilityDescription1;

    @PropertyName("ability1_cooldown")
    public String cooldown1;

    @PropertyName("abilityName2")
    public String abilityName2;

    @PropertyName("abilityDescription2")
    public String abilityDescription2;

    @PropertyName("ability2_cooldown")
    public String cooldown2;

    @PropertyName("ace_effect")
    public String aceEffect;
}
