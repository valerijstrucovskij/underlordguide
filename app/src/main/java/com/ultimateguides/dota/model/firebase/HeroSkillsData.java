package com.ultimateguides.dota.model.firebase;

import com.google.firebase.database.PropertyName;

import java.util.List;

public class HeroSkillsData {
    @PropertyName("armor")
    public Object armor;
    @PropertyName("attackRange")
    public int attackRange;
    @PropertyName("attackRate")
    public Object attackRate;
    @PropertyName("displayName")
    public String displayName;
    @PropertyName("draftTier")
    public int draftTier;
    @PropertyName("goldCost")
    public int goldCost;
    @PropertyName("id")
    public int id;
    @PropertyName("magicResist")
    public Object magicResist;
    @PropertyName("maxMana")
    public int maxmana;
    @PropertyName("texturename")
    public String texturename;
    @PropertyName("movespeed")
    public int movespeed;
    @PropertyName("abilities")
    public List<String> abilities;
    @PropertyName("damageMin")
    public List<Integer> damageMin;
    @PropertyName("damageMax")
    public List<Integer> damageMax;
    @PropertyName("health")
    public List<Integer> health;
//        @PropertyName("neutral_creep")
//        public boolean neutralCreep;

}
