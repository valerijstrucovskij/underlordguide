package com.ultimateguides.dota;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.content.IntentCompat;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.AppodealNetworks;
import com.bugfender.sdk.Bugfender;
import com.facebook.stetho.Stetho;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.ultimateguides.dota.retrofit.SteamAPI;
import com.ultimateguides.dota.screens.main.HideUpdateDialogEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.UUID;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GuideApplication extends Application {

    public static GuideApplication app;
    private static SteamAPI steamApi;
    private static Sheets sheetsService;
    private static String modelName;
    private static FirebaseDatabase firedatabase;
    private static FirebaseFirestore firestore;
    private static SharedPreferences prefs;

    public static FirebaseRemoteConfig getFirebaseRemoteConfig() {
        return mFirebaseRemoteConfig;
    }

    private static FirebaseRemoteConfig mFirebaseRemoteConfig;
    private static boolean isDonated = false;
    private Retrofit retrofit;

    public static GuideApplication getApp() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        modelName = Build.MODEL;
        if (!modelName.equals("Android SDK built for x86")) {
            Bugfender.init(this, Config.bugfenderId, BuildConfig.DEBUG);
            Bugfender.enableCrashReporting();
            Bugfender.enableUIEventLogging(this);
        } else {
            Stetho.initializeWithDefaults(this);
        }
        FirebaseApp.initializeApp(this);
        //----------------------------------------------
        String uuid = getPrefs().getString(PreferenceField.UUID, null);
        if (TextUtils.isEmpty(uuid)) {
            uuid = UUID.randomUUID().toString();
            getPrefs().edit().putString(PreferenceField.UUID, uuid).apply();
        }

        isDonated = getPrefs().getBoolean(PreferenceField.DID_DONATION, false);
        setDonate(isDonated);

        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.steampowered.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        steamApi = retrofit.create(SteamAPI.class);

        //------------------------------------------------------------------------
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        httpClient.interceptors().add(interceptor);
        //---------------------------------------------------------------------------
        //TODO REMOVE
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory factory = JacksonFactory.getDefaultInstance();
        sheetsService = new Sheets.Builder(transport, factory, null)
                .setApplicationName("UnderlordsGuide")
                .build();
        //-------------------------------------------------------------------------
//        if (!FirebaseApp.getApps(this).isEmpty()) {
//        }
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_values);
        //---------------------------------------------------------------------------
        firedatabase = FirebaseDatabase.getInstance();
        firedatabase.setPersistenceEnabled(false);
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        firestore = FirebaseFirestore.getInstance();
        firestore.setFirestoreSettings(settings);

        //Init cloud messaging
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("GuideApplication", "getInstanceId failed", task.getException());
                        return;
                    }
                    String token = task.getResult().getToken();
                    System.out.println();
                    // Log and toast
//                        String msg = getString(R.string.msg_token_fmt, token);
//                        Log.d(TAG, msg);
//                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                });
    }

    public static FirebaseAnalytics getAnalytics() {
        return FirebaseAnalytics.getInstance(app);
    }

//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(LocaleHelper.setLocale(base));
//    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleHelper.setLocale(this);
    }

    public static void downloadDatabase(long firestoreVersion) {
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        firestore = FirebaseFirestore.getInstance();
        firestore.setFirestoreSettings(settings);

        Task<QuerySnapshot> alliancesTask = FirestoreFactory.getAlliances().get();
        Task<QuerySnapshot> itemsTask = FirestoreFactory.getItems().get();
        Task<QuerySnapshot> heroesTask = FirestoreFactory.getHeroes().get();
        Task<QuerySnapshot> editorsTeamsTask = FirestoreFactory.getTeamsEditors().get();
        Task<Void> allTasks = Tasks.whenAll(alliancesTask, itemsTask, heroesTask, editorsTeamsTask);
        allTasks.addOnCompleteListener(task -> {
            if (alliancesTask.isSuccessful() && itemsTask.isSuccessful()) {
                GuideApplication.getPrefs()
                        .edit()
                        .putLong(PreferenceField.FIRESTORE_DB_VERSION, firestoreVersion)
                        .apply();
                EventBus.getDefault().post(new HideUpdateDialogEvent());
            }
        });
    }

    public static SharedPreferences getPrefs() {
        if (prefs == null)
            prefs = getApp().getSharedPreferences("default", MODE_PRIVATE);
        return prefs;
    }

    public static void addADS(Activity activity, int bannerId) {
        if (BuildConfig.DEBUG) return;
        isDonated = getPrefs().getBoolean(PreferenceField.DID_DONATION, false);
        if (!isADSBlocked()) {
            Appodeal.setTesting(BuildConfig.DEBUG);
            Appodeal.setBannerViewId(bannerId);
            Appodeal.muteVideosIfCallsMuted(true);
            if (!Appodeal.isInitialized(Appodeal.BANNER_VIEW))
                Appodeal.initialize(activity, Config.appodealId, Appodeal.BANNER_VIEW);
            Appodeal.show(activity, Appodeal.BANNER_VIEW);
        }
    }


    private static long hour = 3600000;

    public static boolean isADSBlocked() {
        long lastWatchedTime = prefs.getLong(PreferenceField.TIME_LAST_REWARDED_VIDEO, 0);
        long currentTime = System.currentTimeMillis();
        long diff = currentTime - lastWatchedTime;
        if (diff >= hour) {
            return false;
        }
        return true;
    }

//    public static void addADS(Activity activity) {
//        if (!isDonated) {
//            Appodeal.disableNetwork(activity, AppodealNetworks.INMOBI);
//            Appodeal.setTesting(BuildConfig.DEBUG);
//            Appodeal.initialize(activity, Config.appodealId, Appodeal.BANNER_BOTTOM);
//            Appodeal.show(activity, Appodeal.BANNER_BOTTOM);
//        }
//    }

    public static Sheets getSheetService() {
        return sheetsService;
    }

    public static SteamAPI getApi() {
        return steamApi;
    }

    public static FirebaseDatabase getFireDatabase() {
        return firedatabase;
    }

    public static FirebaseFirestore getFirestore() {
        return firestore;
    }

    public static boolean isDonated() {
        return isDonated;
    }

    public static void setDonate(boolean isDonated) {
        GuideApplication.isDonated = isDonated;
    }
}
