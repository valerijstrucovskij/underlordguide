package com.ultimateguides.dota;

public interface FirebaseAnalyticsParams {
    String FAVORITE_TEAM = "favorite_team";
    String FAVORITE_COMMUNITY_TEAM = "favorite_community_team";
    String FAVORITE_HERO = "favorite_hero";
}
