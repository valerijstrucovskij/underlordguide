package com.ultimateguides.dota.binding;

import com.ultimateguides.dota.R;

import java.util.HashMap;

public class ItemsImages extends HashMap<String, Integer> {
    {
        put("", R.color.transparent);
        put("blight_stone", R.drawable.item_t1_blight_stone);
        put("chainmail", R.drawable.item_t1_chainmail);
        put("claymore", R.drawable.item_t1_claymore);
        put("cloak", R.drawable.item_t1_cloak);
        put("extra_items", R.drawable.item_t1_embarrassment_of_riches);
        put("gloves_of_haste", R.drawable.item_t1_gloves_of_haste);
        put("hood", R.drawable.item_t3_hood_of_defiance);
        put("mana_sponge", R.drawable.item_t1_brooch_of_the_martyr);
        put("tranquil_boots", R.drawable.item_t1_tranquil_boots);
        put("vitality_booster", R.drawable.item_t1_vitality_booster);
        put("arcane_boots", R.drawable.item_t2_arcane_boots);
        put("basher", R.drawable.item_t3_skull_basher);
        put("battle_fury", R.drawable.item_t4_battle_fury);
        put("add_unit_cap", R.drawable.item_t5_expanded_roster);
        put("better_items", R.drawable.item_t2_smuggler);
        put("blade_mail", R.drawable.item_t2_blade_mail);
        put("blink_dagger", R.drawable.item_t2_blink_dagger);
        put("big_time_contract", R.drawable.item_t2_big_timecontract);
        put("force_staff", R.drawable.item2_force_staff);
        put("humans_undead", R.drawable.item_t2_fall_from_grace);
        put("prevent_next_death", R.drawable.item_t2_aegis_of_the_immortal);
        put("summoning_stone", R.drawable.item_t2_summoning_stone);
        put("void_stone", R.drawable.item_t2_brooch_of_the_aggressor);
//        put("Soul Sucking Syphon", R.drawable.item_t1_soul_sucking_syphon);
//        put("Strange Bedfellows", R.drawable.item_t1_strange_bedfellows);
//        put("Unstable Reactor", R.drawable.item_t1_unstable_reactor);

//        put("Age of Chivalry", R.drawable.item_t2_age_of_chivalry);



//        put("Completing the Cycle", R.drawable.item_t2_completing_the_cycle);
//        put("Coordinated Assault", R.drawable.item_t2_coordinated_assault);

//        put("Forged in Battle", R.drawable.item_t2_forged_in_battle);
//        put("Indomitable Will", R.drawable.item_t2_indomitable_will);
        put("mask_of_madness", R.drawable.item_t2_mask_of_madness);
        put("octarine_fragment", R.drawable.item_t2_octarine_essence);
//        put("Silver Lining", R.drawable.item_t2_silver_lining);


//        put("Tooth and Claw", R.drawable.item_t2_tooth_and_claw);
//        put("Unstoppable", R.drawable.item_t2_unstoppable);
        put("better_units", R.drawable.item_t3_higher_class_of_criminal);
//        put("Check the Bodies", R.drawable.item_t3_check_the_bodies);
        put("dragons_hoard", R.drawable.item_t3_dragons_hoard);
//        put("Elusive Targets", R.drawable.item_t3_elusive_targets);
        put("skadi", R.drawable.item_t3_eye_of_skadi);
//        put("Font of Creation", R.drawable.item_t3_font_of_creation);

//        put("hunter's Focus", R.drawable.item_t3_hunters_focus);
        put("mekansm", R.drawable.item_t3_mekansm);
//        put("Pocket Sand", R.drawable.item_t3_pocket_sand);
//        put("Retaliate", R.drawable.item_t3_retaliate);
        put("sacred_relic", R.drawable.item_t3_sacred_relic);
//        put("shaman Pluck", R.drawable.item_t3_shaman_pluck);

        put("vanguard", R.drawable.item_t3_vanguard);
//        put("Wicked Intent", R.drawable.item_t3_wicked_intent);

        put("black_king_bar", R.drawable.item_t4_black_king_bar);
        put("daedalus", R.drawable.item_t4_daedalus);
        put("dagon", R.drawable.item_t4_dagon);
        put("cheaper_units", R.drawable.item_t4_friends_and_family);
        put("maelstrom", R.drawable.item_t4_maelstrom);
        put("moon_shard", R.drawable.item_t4_moon_shard);
        put("pipe_of_insight", R.drawable.item_t4_pipe_of_insight);
        put("radiance", R.drawable.item_t4_radiance);
        put("refresher_orb", R.drawable.item_t4_refresher_orb);
        put("assault_cuirass", R.drawable.item_t5_assault_cuirass);
        put("bloodthorn", R.drawable.item_t5_bloodthorn);
//        put("Desperate Measures", R.drawable.item_t5_desperate_measures);
        put("divine_rapier", R.drawable.item_t5_divine_rapier);

        put("heart", R.drawable.item_t5_heart_of_tarrasque);
        put("Recruiter", R.drawable.item_t5_recruiter);
        put("shivas_guard", R.drawable.item_t5_shivas_guard);
        put("scythe_of_vyse", R.drawable.item_t4_scythe_of_vyse);
        put("helm_of_undying", R.drawable.item_helm_of_underdying);
        put("desperation_bracer", R.drawable.item_bracers_desperation);
        put("dawning_of_ristul", R.drawable.items_dawning_ristul);
        put("poaching_knife", R.drawable.item_poaching_knight);
        put("hype_breaker", R.drawable.item_assassin_veil);
        put("hype_harvester", R.drawable.item_murder_gong);

    }
}
