package com.ultimateguides.dota.binding;

import com.ultimateguides.dota.AllianceType;
import com.ultimateguides.dota.R;

import java.util.HashMap;

public class AlliancesImages extends HashMap<AllianceType, Integer> {
    {
        put(AllianceType.None, R.color.transparent);
        put(AllianceType.assassin, R.drawable.race_assassin);
        put(AllianceType.ogre, R.drawable.race_bloodbound);
        put(AllianceType.bloodbound, R.drawable.race_bloodbound);
        put(AllianceType.brawny, R.drawable.race_brawny);
        put(AllianceType.dwarf, R.drawable.race_deadeye);
        put(AllianceType.demon, R.drawable.race_demon);
        put(AllianceType.demonhunter, R.drawable.race_demonhunter);
        put(AllianceType.dragon, R.drawable.race_dragon);
        put(AllianceType.druid, R.drawable.race_druid);
        put(AllianceType.elf, R.drawable.race_elusive);
        put(AllianceType.undead, R.drawable.race_heartless);
        put(AllianceType.human, R.drawable.race_human);
        put(AllianceType.hunter, R.drawable.race_hunter);
        put(AllianceType.mech, R.drawable.race_inventor);
        put(AllianceType.inventor, R.drawable.race_inventor);
        put(AllianceType.knight, R.drawable.race_knight);
        put(AllianceType.mage, R.drawable.race_mage);
        put(AllianceType.primordial, R.drawable.race_primordial);
        put(AllianceType.savage, R.drawable.race_savage);
        put(AllianceType.naga, R.drawable.race_scaled);
        put(AllianceType.goblin, R.drawable.race_scrappy);
        put(AllianceType.shaman, R.drawable.race_shaman);
        put(AllianceType.troll, R.drawable.race_troll);
        put(AllianceType.warlock, R.drawable.race_warlock);
        put(AllianceType.warrior, R.drawable.race_warrior);
        put(AllianceType.champion, R.drawable.race_champion);
        put(AllianceType.brutal, R.drawable.race_brute);
        put(AllianceType.healer, R.drawable.race_healer);
        put(AllianceType.insect, R.drawable.race_insect);
        put(AllianceType.spiritbrother, R.drawable.race_spirit);
    }
}
