package com.ultimateguides.dota.binding;

import com.ultimateguides.dota.HeroTypeDescriptor;
import com.ultimateguides.dota.R;

import java.util.HashMap;

public class HeroImages extends HashMap<String, Integer> {
    {
        put(HeroTypeDescriptor.Abaddon, R.drawable.avatar_abaddon);
        put(HeroTypeDescriptor.Alchemist, R.drawable.avatar_alchemist);
        put(HeroTypeDescriptor.AntiMage, R.drawable.avatar_antimage);
        put(HeroTypeDescriptor.ArcWarden, R.drawable.avatar_arcwarden);
        put(HeroTypeDescriptor.Axe, R.drawable.avatar_axe);
        put(HeroTypeDescriptor.Batrider, R.drawable.avatar_batrider);
        put(HeroTypeDescriptor.Beastmaster, R.drawable.avatar_beastmaster);
        put(HeroTypeDescriptor.Bloodseeker, R.drawable.avatar_bloodseeker);
        put(HeroTypeDescriptor.BountyHunter, R.drawable.avatar_bountyhunter);
        put(HeroTypeDescriptor.ChaosKnight, R.drawable.avatar_chaosknight);
        put(HeroTypeDescriptor.Clockwerk, R.drawable.avatar_clockwerk);
        put(HeroTypeDescriptor.CrystalMaiden, R.drawable.avatar_crystalmaiden);
        put(HeroTypeDescriptor.Disruptor, R.drawable.avatar_disruptor);
        put(HeroTypeDescriptor.Doom, R.drawable.avatar_doom);
        put(HeroTypeDescriptor.DragonKnight, R.drawable.avatar_dragonknight);
        put(HeroTypeDescriptor.DrowRanger, R.drawable.avatar_drowranger);
        put(HeroTypeDescriptor.Enchantress, R.drawable.avatar_enchantress);
        put(HeroTypeDescriptor.Enigma, R.drawable.avatar_enigma);
        put(HeroTypeDescriptor.Gryocopter, R.drawable.avatar_gyrocopter);
        put(HeroTypeDescriptor.Juggernaut, R.drawable.avatar_juggernaut);
        put(HeroTypeDescriptor.KeeperoftheLight, R.drawable.avatar_keeperofthelight);
        put(HeroTypeDescriptor.Kunkka, R.drawable.avatar_kunkka);
        put(HeroTypeDescriptor.Lich, R.drawable.avatar_lich);
        put(HeroTypeDescriptor.Lina, R.drawable.avatar_lina);
        put(HeroTypeDescriptor.LoneDruid, R.drawable.avatar_lonedruid);
        put(HeroTypeDescriptor.Luna, R.drawable.avatar_luna);
        put(HeroTypeDescriptor.Lycan, R.drawable.avatar_lycan);
        put(HeroTypeDescriptor.Mirana, R.drawable.avatar_mirana);
        put(HeroTypeDescriptor.Medusa, R.drawable.avatar_meduza);
        put(HeroTypeDescriptor.Morphling, R.drawable.avatar_morphling);
        put(HeroTypeDescriptor.NaturesProphet, R.drawable.avatar_naturesprophet);
        put(HeroTypeDescriptor.Necrophos, R.drawable.avatar_necrophos);
        put(HeroTypeDescriptor.OgreMagi, R.drawable.avatar_ogremagi);
        put(HeroTypeDescriptor.Omniknight, R.drawable.avatar_omniknight);
        put(HeroTypeDescriptor.PhantomAssassin, R.drawable.avatar_phantomassassin);
        put(HeroTypeDescriptor.Puck, R.drawable.avatar_puck);
        put(HeroTypeDescriptor.Pudge, R.drawable.avatar_pudge);
        put(HeroTypeDescriptor.QueenofPain, R.drawable.avatar_queenofpain);
        put(HeroTypeDescriptor.Razor, R.drawable.avatar_razor);
        put(HeroTypeDescriptor.SandKing, R.drawable.avatar_sandking);
        put(HeroTypeDescriptor.Shadowfiend, R.drawable.avatar_shadowfiend);
        put(HeroTypeDescriptor.ShadowShaman, R.drawable.avatar_shadowshaman);
        put(HeroTypeDescriptor.Slardar, R.drawable.avatar_slardar);
        put(HeroTypeDescriptor.Slark, R.drawable.avatar_slark);
        put(HeroTypeDescriptor.Sniper, R.drawable.avatar_sniper);
        put(HeroTypeDescriptor.Techies, R.drawable.avatar_techies);
        put(HeroTypeDescriptor.TemplarAssassin, R.drawable.avatar_templarassassin);
        put(HeroTypeDescriptor.Terrorblade, R.drawable.avatar_terrorblade);
        put(HeroTypeDescriptor.Tidehunter, R.drawable.avatar_tidehunter);
        put(HeroTypeDescriptor.Timbersaw, R.drawable.avatar_timbersaw);
        put(HeroTypeDescriptor.Tinker, R.drawable.avatar_tinker);
        put(HeroTypeDescriptor.Tiny, R.drawable.avatar_tiny);
        put(HeroTypeDescriptor.TreantProtector, R.drawable.avatar_treantprotector);
        put(HeroTypeDescriptor.TrollWarlord, R.drawable.avatar_trollwarlord);
        put(HeroTypeDescriptor.Tusk, R.drawable.avatar_tusk);
        put(HeroTypeDescriptor.Venomancer, R.drawable.avatar_venomancer);
        put(HeroTypeDescriptor.Viper, R.drawable.avatar_viper);
        put(HeroTypeDescriptor.Warlock, R.drawable.avatar_warlock);
        put(HeroTypeDescriptor.Windranger, R.drawable.avatar_windranger);
        put(HeroTypeDescriptor.WitchDoctor, R.drawable.avatar_witchdoctor);

        put(HeroTypeDescriptor.Dazzle, R.drawable.avatar_dazzle);
        put(HeroTypeDescriptor.ShadowDemon, R.drawable.avatar_shadow_demon);
        put(HeroTypeDescriptor.Sven, R.drawable.avatar_sven);
        put(HeroTypeDescriptor.Weaver, R.drawable.avatar_weaver);
        put(HeroTypeDescriptor.WispIo, R.drawable.avatar_wisp);
        put(HeroTypeDescriptor.Bristleback, R.drawable.avatar_bristleback);
        put(HeroTypeDescriptor.Broodmother, R.drawable.avatar_broodmother);
        put(HeroTypeDescriptor.Void, R.drawable.avatar_void);
        put(HeroTypeDescriptor.LegionCommander, R.drawable.avatar_legion_commander);
        put(HeroTypeDescriptor.LifeStealer, R.drawable.avatar_lifestealer);
        put(HeroTypeDescriptor.Magnus, R.drawable.avatar_magnus);
        put(HeroTypeDescriptor.NyxAssassin, R.drawable.avatar_nyx);

        put(HeroTypeDescriptor.SpiritEarth, R.drawable.avatar_spirit_earth);
        put(HeroTypeDescriptor.SpiritEmber, R.drawable.avatar_spirit_ember);
        put(HeroTypeDescriptor.SpiritStorm, R.drawable.avatar_spirit_strom);
        put(HeroTypeDescriptor.SpiritVoid, R.drawable.avatar_spirit_void);
        put(HeroTypeDescriptor.Snapfire, R.drawable.avatar_snapfire);
    }
}
