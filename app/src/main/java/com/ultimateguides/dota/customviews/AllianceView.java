package com.ultimateguides.dota.customviews;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.palette.graphics.Palette;

import com.ultimateguides.dota.R;
import java.util.ArrayList;
import java.util.List;

public class AllianceView extends LinearLayout {
    private int stages;
    private int unitsPerStage;
    private int color;
    private boolean isHighloghtLast;
    private int activeCount;
    private int allianceIcon;
    ViewGroup placeholder = null;
    private View root;
    private String describe;
    private boolean isVisibleDescription;

    public AllianceView(Context context) {
        this(context, null);
    }

    public AllianceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AllianceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readAttr(context, attrs);
    }

    private void initViews(Context context) {
        root = inflate(context, R.layout.view_alliance_level, this);
        updateView();
    }

    private void readAttr(Context context, AttributeSet attrs) {
        TypedArray typedArray;
        typedArray = context.obtainStyledAttributes(attrs, R.styleable.AllianceView);
        stages = typedArray.getInteger(R.styleable.AllianceView_stagesSummary, 0);
        unitsPerStage = typedArray.getInteger(R.styleable.AllianceView_unitsPerStage, 0);
        activeCount = typedArray.getInteger(R.styleable.AllianceView_activateCount, 0);
        isHighloghtLast = typedArray.getBoolean(R.styleable.AllianceView_highlightLast, false);
        isVisibleDescription = typedArray.getBoolean(R.styleable.AllianceView_isVisibleDescription, false);
        allianceIcon = typedArray.getResourceId(R.styleable.AllianceView_allianceIcon, R.drawable.race_assassin);
        describe = typedArray.getString(R.styleable.AllianceView_description);
        color = typedArray.getColor(R.styleable.AllianceView_color, Color.WHITE);
        typedArray.recycle();
        initViews(context);
    }

    public void updateView() {
        ImageView iconView = root.findViewById(R.id.iv_alliance);
        TextView describeView = root.findViewById(R.id.tv_description);
        ViewGroup placehodler1 = root.findViewById(R.id.placeholder_1);
        ViewGroup placehodler2 = root.findViewById(R.id.placeholder_2);
        ViewGroup placehodler3 = root.findViewById(R.id.placeholder_3);
        placehodler1.setVisibility(GONE);
        placehodler2.setVisibility(GONE);
        placehodler3.setVisibility(GONE);

        switch (unitsPerStage) {
            case 1:
                placeholder = placehodler1;
                placehodler1.setVisibility(VISIBLE);
                break;
            case 2:
                placeholder = placehodler2;
                placehodler2.setVisibility(VISIBLE);
                break;
            case 3:
                placeholder = placehodler3;
                placehodler3.setVisibility(VISIBLE);
                break;
            default:
                placeholder = placehodler1;
                placehodler1.setVisibility(VISIBLE);
        }
        //-------------------------------------------------------------------------------
        describeView.setText(describe);
        describeView.setVisibility(isVisibleDescription || !TextUtils.isEmpty(describe) ? VISIBLE : GONE);

        iconView.setImageResource(allianceIcon);
        iconView.invalidate();
        BitmapDrawable drawable = (BitmapDrawable) iconView.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        Palette p = Palette.from(bitmap).generate();
        int summaryAll = stages * unitsPerStage;
        List<AppCompatImageView> units = new ArrayList<>(summaryAll);

        for (int i = 0; i < placeholder.getChildCount(); i++) {
            AppCompatImageView v = (AppCompatImageView) placeholder.getChildAt(i);
            units.add(v);
            //trim
            if (i >= summaryAll) {
                v.setVisibility(GONE);
            } else {
                v.setVisibility(VISIBLE);
            }

            //enable to count
            if (i < activeCount) {
                v.setSupportBackgroundTintMode(PorterDuff.Mode.SRC);
            } else {
                v.setSupportBackgroundTintMode(PorterDuff.Mode.MULTIPLY);
            }

            int[][] states = new int[][]{
                    new int[]{android.R.attr.state_enabled}
            };
            int[] colors = new int[]{
                    p.getDominantColor(getResources().getColor(android.R.color.white))
            };
            v.setSupportBackgroundTintList(new ColorStateList(states, colors));
        }
//        if (unitsPerStage > 0 && activeCount % unitsPerStage == 0 && isHighloghtLast && activeCount != 0) {
        if (activeCount == unitsPerStage && isHighloghtLast) {
            setBackgroundResource(R.color.palette1);
        } else {
            setBackgroundResource(R.color.transparent);
        }
    }
//    public void setH

    public void setStages(int stages) {
        this.stages = stages;
    }

    public void setUnitsPerStage(int unitsPerStage) {
        this.unitsPerStage = unitsPerStage;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setHighlightLast(boolean highloghtLast) {
        isHighloghtLast = highloghtLast;
    }

    public void setActiveCount(int activeCount) {
        this.activeCount = activeCount;
    }

    public void setAllianceIcon(int allianceIcon) {
        this.allianceIcon = allianceIcon;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }
}
