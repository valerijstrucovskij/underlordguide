package com.ultimateguides.dota.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultimateguides.dota.R;

public class LoadingView extends LinearLayout {
    private String title;
    private String subTitle;

//    public LoadingView(Context context) {
//        this(context, null);
//        initViews(context);
//    }


    public LoadingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    //    readAttr(context, attrs);
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readAttr(context, attrs);
    }

    private void initViews(Context context) {
        inflate(context, R.layout.view_loading, this);
        TextView titleView = findViewById(R.id.tv_title);
        TextView subtitleView = findViewById(R.id.tv_subtitle);

        titleView.setText(title);
        subtitleView.setText(subTitle);
    }

    private void readAttr(Context context, AttributeSet attrs) {
        TypedArray typedArray;
        typedArray = context
                .obtainStyledAttributes(attrs, R.styleable.LoadingView);
        title = typedArray.getString(R.styleable.LoadingView_title);
        subTitle = typedArray.getString(R.styleable.LoadingView_subtitle);
        typedArray.recycle();

        initViews(context);
    }
}
