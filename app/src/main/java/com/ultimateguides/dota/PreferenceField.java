package com.ultimateguides.dota;

public class PreferenceField {
    public static String LANGUAGE = "current_language";
    public static String UUID = "uuid";
    public static String FIRESTORE_DB_VERSION = "firestore_db_version";
    public static String USERNAME = "username_steam";
    public static String STEAM_ID = "username_steam_id";
    public static String DID_DONATION = "is_donated";
    public static String HAS_APP_NEWS = "has_app_news";
    public static String DONATION_SIZE = "donation_size";
    public static String TIME_LAST_REWARDED_VIDEO = "when_watched_rewarded_video";
    public static String IS_SAW_ONCE_REWARDED_VIDEO = "is_saw_once_rewarded_video";
}
