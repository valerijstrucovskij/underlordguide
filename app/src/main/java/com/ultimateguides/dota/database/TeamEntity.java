package com.ultimateguides.dota.database;

import android.graphics.Color;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.ultimateguides.dota.HeroTypeOLD;
import com.ultimateguides.dota.model.firebase.TeamModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

@Entity(tableName = "my_teams")
public class TeamEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "backgroundColor")
    public int backgroundColor;

    @ColumnInfo(name = "lordName")
    public String lordName;

    @ColumnInfo(name = "modified")
    public long modified;

    @ColumnInfo(name = "heroes")
    public ArrayList<String> heroes;


    public TeamEntity(int id, String name, long modified, ArrayList<String> heroes, String lordName) {
        this.id = id;
        this.name = name;
        this.modified = modified;
        this.heroes = heroes;
        this.lordName = lordName;
    }

    public TeamEntity(TeamModel model) {
        heroes = new ArrayList<>();
        if (model == null) return;
        name = model.name;
        lordName = model.lordId;
        modified = new Date().getTime();
        heroes.addAll(model.heroes);
    }
}
