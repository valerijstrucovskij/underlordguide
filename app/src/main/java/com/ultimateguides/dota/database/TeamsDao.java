package com.ultimateguides.dota.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public interface TeamsDao { 

    @Query("Select * from my_teams")
    Maybe<List<TeamEntity>> getAllMyTeams();

    @Query("Select * from my_teams")
    Flowable<List<TeamEntity>> getAllMyTeams2();

    @Insert
    long addTeam(TeamEntity data);

    @Update
    int updateTeam(TeamEntity data);

    @Delete
    int deleteTeam(TeamEntity data);

}
