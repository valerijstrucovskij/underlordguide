package com.ultimateguides.dota;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Locale;

public class LocaleHelper {

    public static final String DEFAULT_LANGUAGE = "en";

    static HashMap<String, String> pairLang;

    static {
        pairLang = new HashMap<>();
        pairLang.put("en", "english");
        pairLang.put("ru", "russian");
        pairLang.put("th", "thai");
    }

    public static Context onAttach(Context context) {
        String lang = getPersistedData(Locale.getDefault().getLanguage());
        return setLocale(context, lang);
    }

    public static Context onAttach(Context context, String defaultLanguage) {
        String lang = getPersistedData(defaultLanguage);
        return setLocale(context, lang);
    }

    public static String getLanguage() {
        return getPersistedData(Locale.getDefault().getLanguage());
    }

    public static String getLanguageDisplay() {
        String shortLang = getPersistedData(Locale.getDefault().getLanguage());
        String lang = pairLang.get(shortLang);
        if (TextUtils.isEmpty(lang)) {
            lang = pairLang.get("en");
        }
        return lang;
    }

    static Context setLocale(Context context) {
        String lang = getPersistedData(Locale.getDefault().getLanguage());
        persist(lang);
        return updateResources(context, lang);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//        }
//
//        return updateResourcesLegacy(context, language);
    }

    public static Context setLocale(Context context, String language) {
        persist(language);

        return updateResources(context, language);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//        }
//
//        return updateResourcesLegacy(context, language);
    }

    private static String getPersistedData(String defaultLanguage) {
        return GuideApplication.getPrefs().getString(PreferenceField.LANGUAGE, defaultLanguage);
    }

    private static void persist(String language) {
        SharedPreferences.Editor editor = GuideApplication.getPrefs().edit();
        editor.putString(PreferenceField.LANGUAGE, language);
        editor.commit();
    }

    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
//        if (Build.VERSION.SDK_INT >= 17) {
//            config.setLocale(locale);
//            context = context.createConfigurationContext(config);
//        } else {
//            config.locale = locale;
//            res.updateConfiguration(config, res.getDisplayMetrics());
//        }
        config.setLocale(locale);
        res.updateConfiguration(config, res.getDisplayMetrics());
        return context;
//        return context.createConfigurationContext(configuration);
    }

//    @SuppressWarnings("deprecation")
//    private static Context updateResourcesLegacy(Context context, String language) {
//        Locale locale = new Locale(language);
//        Locale.setDefault(locale);
//
//        Resources resources = context.getResources();
//
//        Configuration configuration = resources.getConfiguration();
//        configuration.locale = locale;
//
//        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
//
//        return context;
//    }
}
