const fetchNext = token =>
  fetch(`${location.origin}${location.pathname}?ajax=1&tab=Matches&sessionid=${g_sessionID}&continue_token=${token}`)
    .then(res => res.json())

const fetchAllData = async () => {
  const tables = [document.querySelector('.generic_kv_table')]
  let continueToken = g_sGcContinueToken
  while (continueToken) {
      const resp = await fetchNext(continueToken)
		if(resp.html != ''){
          tables.push(tableFromResponse(resp))

        }
		continueToken = resp.continue_token
  }
  return tables.map(parseTable).flat()
}

const tableFromResponse = response => {
  const node = document.createElement('div')
  node.innerHTML = response.html
  return node.querySelector('table')
}

const parseTable = table => {
console.log(table == null)
  const rows = [...table.rows]

  const columns = [...rows[0].cells].map(c => c.textContent)
  console.log(columns)
  return rows.slice(1)
    .map(row => [...row.cells].map((cell) => cell.textContent))
    .map(row => {
      var json = {}
      json[columns[0]] = row[0].trim();
      json[columns[1]] = row[1].trim();
      json[columns[2]] = row[2].trim();
      json[columns[3]] = row[3].trim();
      return json;
    }
    )
}

fetchAllData()
  .then(data => {
//    console.log(JSON.stringify(data))
    Android.onSeasonPass(JSON.stringify(data))
    return data
  })