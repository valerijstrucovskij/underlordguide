const fetchNext = token =>
  fetch(`${location.origin}${location.pathname}?ajax=1&tab=Matches&sessionid=${g_sessionID}&continue_token=${token}`)
    .then(res => res.json())

const fetchAllData = async () => {
  const tables = [document.querySelector('.generic_kv_table')]
  let continueToken = g_sGcContinueToken
  while (continueToken) {
    const resp = await fetchNext(continueToken)
    tables.push(tableFromResponse(resp))
    continueToken = resp.continue_token
  }
  return tables.map(parseTable).flat()
}

const tableFromResponse = response => {
  const node = document.createElement('div')
  node.innerHTML = response.html
  return node.querySelector('table')
}

const parseTable = table => {
  const rows = [...table.rows]
  const columns = [...rows[0].cells].map(c => c.textContent)
  return rows.slice(1)
    .map(row => [...row.cells].map((cell) => cell.textContent ))
    .map(row => columns.reduce((acc, col, i) =>
      ({ ...acc, [col]: col !== 'StartTime' ? parseInt(row[i]) : row[i] }),
      {}
    ))
}
fetchAllData()
  .then(data => {
//    console.log("NEW DATA" + data)
    Android.onData(JSON.stringify(data))
    return data
  })