const fetchAllData = async () => {
  const tables = [document.querySelector('.generic_kv_table')]
  return tables.map(parseTable).flat()
}

const parseTable = table => {
  const rows = [...table.rows]
  const columns = [...rows[0].cells].map(c => c.textContent)
  return rows.slice(1)
    .map(row => [...row.cells].map((cell) => cell.textContent ))
    .map(row => columns.reduce((acc, col, i) =>

          ({ ...acc, [col]:row[i].trim()}),
        {}
      )
    )
}
 fetchAllData()
      .then(data => {
// 			console.log(JSON.stringify(data[0]))
        	Android.onMMR(JSON.stringify(data[0]))
        	return data
      }
      )
