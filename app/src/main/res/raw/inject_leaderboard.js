const fetchAllData = async () => {
    const tables = [document.querySelectorAll('.row .player')][0]
    let players = [];
    tables.forEach((i, elem) => {
        var item = {}
        item.rank = elem+1;
        item.name = i.textContent;
        players.push(item);
    });
    return players
}


fetchAllData()
    .then(data => {
        Android.onLeaderboard(JSON.stringify(data))
        return data
    }
    )