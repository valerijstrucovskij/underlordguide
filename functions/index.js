'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// Take the text parameter passed to this HTTP endpoint and insert it into the
// Realtime Database under the path /messages/:pushId/original
exports.addMessage = functions.https.onRequest(async (req, res) => {
    // Grab the text parameter.
    const original = req.query.text;
    // Push the new message into the Realtime Database using the Firebase Admin SDK.
    const snapshot = await admin.database().ref('/messages').push({ original: original });
    // Redirect with 303 SEE OTHER to the URL of the pushed object in the Firebase console.
    res.redirect(303, snapshot.ref.toString());
});


//------------------------------------------------------------------------------------------------
// [END functionsimport]
// [START additionalimports]
// Moments library to format dates.
const moment = require('moment');
// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({
    origin: true,
});
// [END additionalimports]

// [START all]
/**
 * Returns the server's date. You must provide a `format` URL query parameter or `format` value in
 * the request body with which we'll try to format the date.
 *
 * Format must follow the Node moment library. See: http://momentjs.com/
 *
 * Example format: "MMMM Do YYYY, h:mm:ss a".
 * Example request using URL query parameters:
 *   https://us-central1-<project-id>.cloudfunctions.net/date?format=MMMM%20Do%20YYYY%2C%20h%3Amm%3Ass%20a
 * Example request using request body with cURL:
 *   curl -H 'Content-Type: application/json' /
 *        -d '{"format": "MMMM Do YYYY, h:mm:ss a"}' /
 *        https://us-central1-<project-id>.cloudfunctions.net/date
 *
 * This endpoint supports CORS.
 */
// [START trigger]
exports.date = functions.https.onRequest(async (req, res) => {
    // [END trigger]
    // [START sendError]
    // Forbidding PUT requests.
    if (req.method === 'PUT') {
        return res.status(403).send('Forbidden!');
    }
    // [END sendError]

    // [START usingMiddleware]
    // Enable CORS using the `cors` express middleware.
    return cors(req, res, () => {
        // [END usingMiddleware]
        // Reading date format from URL query parameter.
        // [START readQueryParam]
        let format = req.query.format;
        // [END readQueryParam]
        // Reading date format from request body query parameter
        if (!format) {
            // [START readBodyParam]
            format = req.body.format;
            // [END readBodyParam]
        }
        // [START sendResponse]
        const formattedDate = moment().format(format);
        console.log('Sending Formatted date:', formattedDate);
        res.status(200).send(formattedDate);
        // [END sendResponse]
    });
});
// [END all]


//---------------------------------------------------------

exports.leaderboard = functions.https.onRequest(async (req, res) => {

    const fetchAllData = async () => {
        const tables = [document.querySelectorAll('.row .player')][0]
        let players = [];
        tables.forEach((i, elem) => {
            var item = {}
            item.rank = elem + 1;
            item.name = i.textContent;
            players.push(item);
        });
        return players
    }


    return cors(req, res, () => {
        // function getData(callback) {
        //         let url = 'https://underlords.com/leaderboard';
        //         let players = [];
        //         request(url, (error, response, html) => {
        //             if (error) {
        //                 return callback(players);
        //             }
        //             var $ = cheerio.load(html);
        //             $('.row .player').each((i, elem) => {
        //                 players.push($(elem).filter('.player').text());
        //             });
        //             callback(players);
        //         });
        //     }

        var d2 = fetchAllData();

        var d1 = { name: "Test", category: "s1" }
        res.status(200).json(d2);
    });
});
//------------------------------------------
// const functions = require('firebase-functions');
// const admin = require('firebase-admin');
// admin.initializeApp()



const firestore = admin.firestore();
const data = require("./users.json");

exports.updateHeroes = functions.https.onRequest(async (req, res) => {
    // firestore.collection('heroes').re
    // const revision = req.query.rev;
    // firestore.collection("heroes_2").doc(revision).set(data);


    if (data && (typeof data === "object")) {
        Object.keys(data).forEach(docKey => {
            firestore.collection('heroes').doc(docKey).set(data[docKey]);
            
            // .then((res) => {
            //     console.log("Document " + docKey + " successfully written!");
            // }).catch((error) => {
            //     console.error("Error writing document: ", error);
            // });
            // firestore.collection('heroes').doc().li .add(data);
            // firestore.collection('heroes').set(data);    
           
            // // Grab the text parameter.
            // const original = req.query.text;
            // // Push the new message into the Realtime Database using the Firebase Admin SDK.
            // const snapshot = await admin.database().ref('/messages').push({ original: original });
            // // Redirect with 303 SEE OTHER to the URL of the pushed object in the Firebase console.
            // res.redirect(303, snapshot.ref.toString());
        });
    }
    // res.status(200).json(data);
    res.status(200).json(data[Object.keys(data)[0]]);
});
// const spawn = require('child-process-promise').spawn;
// const path = require('path');
// const os = require('os');
// const fs = require('fs');





    // if (req.method === 'PUT') {
    //     return res.status(403).send('Forbidden!');
    // }
    //let url = 'https://underlords.com/leaderboard'

    // function getData(callback) {
    //     let url = 'https://underlords.com/leaderboard';
    //     let players = [];
    //     request(url, (error, response, html) => {
    //         if (error) {
    //             return callback(players);
    //         }
    //         var $ = cheerio.load(html);
    //         $('.row .player').each((i, elem) => {
    //             players.push($(elem).filter('.player').text());
    //         });
    //         callback(players);
    //     });
    // }
    //res.setHeader('Content-Type', 'application/json');//return header is json data

    // getData(function (data) {

    // });

    //-------------
    // return cors(req, res, () => {

    // return fetch(url)
    //     .then(response => {
    //         // console.log(dta.text.toString())
    //         const tables = [response.querySelectorAll('.row .player')][0]
    //         let players = [];
    //         tables.forEach((i, elem) => {
    //             players.push($(elem).filter('.player').text());
    //         });
    //         console.log(players)
    //         return players;
    //     })
    //     .then(players => JSON.stringify(players))
    //     .then(json => {
    //         return res.set('Content-Type', 'application/json')
    //             .status(200)
    //             .json(json);
    //     });


    // });



//-------------------------
// const firebase = require("firebase");
// // Required for side-effects
// require("firebase/firestore");

// // Initialize Cloud Firestore through Firebase
// firebase.initializeApp({
//     apiKey: "AIzaSyBO4iJCOqLjnhsafVwu61PCjXVAGdoEivs",
//     authDomain: "underlords-5f247",
//     projectId: "underlords-5f247"
//   });

// var db = firebase.firestore();

// var menu =[  
//     {  
//        "id":1,
//        "name":"Focaccia al rosmarino",
//        "description":"Wood fired rosemary and garlic focaccia",
//        "price":8.50,
//        "type":"Appetizers"
//     },
//     {  
//        "id":2,
//        "name":"Burratta con speck",
//        "description":"Burratta cheese, imported smoked prok belly prosciutto, pached balsamic pear",
//        "price":13.50,
//        "type":"Appetizers"
//     }
//  ]

// menu.forEach(function(obj) {
//     db.collection("menu").add({
//         id: obj.id,
//         name: obj.name,
//         description: obj.description,
//         price: obj.price,
//         type: obj.type
//     }).then(function(docRef) {
//         console.log("Document written with ID: ", docRef.id);
//     })
//     .catch(function(error) {
//         console.error("Error adding document: ", error);
//     });
// })
